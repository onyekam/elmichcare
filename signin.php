<?php 
	session_start();
	$userMail = "";
	if(isset($_SESSION['userMail']))
		header("./");
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
		<meta name="title" content="Care Recruitment">
		<meta name="keywords" content="care recruitment"/>
		<meta name="description" content="Care People" />

		<meta name="author" content="">
		<meta name="format-detection" content="telephone=no">
		<title>ElMich Care</title>

		<link rel="stylesheet" type="text/css" href="files/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/style.css"/>
		<link rel="stylesheet" type="text/css" href="files/responsive.css"/>
		<link rel="stylesheet" type="text/css" href="files/AdminLTE.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/jquery-ui.css"/>
		<link rel="shortcut icon" href="files/fav.png"/>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="files/blue.css">  
	 
		<script type="text/javascript" src="files/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="files/bootstrap.min.js"></script>
		<script type="text/javascript" src="files/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="files/jquery-ui.js"></script>
		<script type="text/javascript" src="files/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="files/datedifferences.js"></script>
	   
	</head> 
	<body>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container"> 
					<div class="navbar-header page-scroll">
						<button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button"> 
							<span class="sr-only">Toggle navigation</span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand logo">
							<img src="files/logodaryel.png" alt="logo"/>
						</a>      
					</div>
					<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">	  
						<ul class="login-register pull-right" style="width:auto;">
							<li class="page-scroll" style="margin-right: 20px"> 
								<h4 style="color: #006a77;">Call 02072724914</h4> 
							</li>     
							<li  class=""><a href="#" style="background:#006a77">Home</a></li>
							<li id="login1" class="login"><a href="signin.php">Sign In</a></li>
							<li id="login2" style="display:none;" class="login">
								<div class="btn-group">
									<a href="#" class="btn btn-default">My Account</a>							  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">						
										<li><a href="signout.php">Sign Out</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<section class="main-section"> 
			<link href="files/smart_wizard.css" rel="stylesheet" type="text/css">
			<section class="page-section main-content" >
				<div class="container">
					<div class="search_form row" style="background:#006a77">
						<div class="col-md-10">
							<h2 style="color:white">Login</h2>                    
						</div> 
					</div>
					<div class="row"><div class="col-sm-12"></div></div>
					<div class="row">
						<article>	 
							<div class="form_all login">
								<form action="#" class="form" id="AdminUserLoginForm" method="post">
									<div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
									<div class="form-group row gap_row">
										<div class="col-sm-12 form-field">
											<label>Email:*</label>
											<div class="input email required">
												<input class="form-control required" placeholder="Email" maxlength="200" type="email" id="email" required="required"/>
											</div>									
										</div>											  
									</div>
									<div class="form-group row gap_row">
										<div class="col-sm-12 form-field">
											<label>Password:*</label>
											<div class="input password required">
												<input class="form-control required" placeholder="Password" type="password" id="password" required="required"/>
											</div>
										</div>
									</div>
									<div class="form-group row ">
										<div class="col-sm-12 loginbutton">
											<input type="submit" class="btn btn-primary" id="checkSubmit" value="Login" style="background:#006a77"/>
										</div>
									</div>
									<div class="form-group row gap_row">
										<div class="col-sm-8">
											<a class="pull-left" href="#" title="Forgot Password">Forgot Password?</a> 
										</div>
									</div> 
								</form>						   
							</div>
						</article>
					</div>
				</div>
			</section>
			<div class="overlay"></div>  
			<footer class="footer">
				<div class="footer-secoandry" style="background:#006a77">
				  <div class="container">
					<div class="logo-fotter pull-right"> 
						<a href="#"><img src="files/logodaryel.png" alt="logo"/></a>        </div>
					<div class="pull-left copyright"> © 2017 
					   <a href="#">elmichcare.com</a> |
					   <a href="#">Privacy</a> | 
					   <a href="#">Terms </a> </div>
				  </div>
				</div>
			  </footer>     
		</section>
	</body>
</html>

<script>
	$(document).ready(function(){
		//$(".overlay").show();
		$("#email").blur(function(){
			var a = $("#email").val().trim();
			var atpos = a.indexOf("@");
			var dotpos = a.lastIndexOf(".");
			if(a == ""){
				$('#email').closest(".form-group").addClass("has-error");
			}else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=a.length){
				$('#email').closest(".form-group").addClass("has-error");
			}else{
				$.ajax({
					type: 'post',
					url: 'includes/signin/',
					data: "email="+a,
					success: function(response) {
						if(response=="OK")	{
							$('#email').closest(".form-group").removeClass("has-error");
						}else{
							$('#email').closest(".form-group").addClass("has-error");
						}
					}
				});
			}
		});
		$("#password").keyup(function(){
			var a = $("#email").val().trim();
			var b = $("#password").val().trim();
			
			if(a == ""){
				$('#email').closest(".form-group").addClass("has-error");
			}else if(b == ""){
				$('#password').closest(".form-group").addClass("has-error");
			}else{
				$.ajax({
					type: 'post',
					url: 'includes/signin/',
					data: "email="+a+"&password="+b,
					success: function(response) {
						if(response=="OK")	{
							$('#email').closest(".form-group").removeClass("has-error");
							$('#password').closest(".form-group").removeClass("has-error");
							$("#email").css("border-color", "green");
							$('#password').css("border-color", "green");
							alert("Login Successful!");
							setTimeout(function(){
								window.location = "./";
							}, 1000);
						}else{
							$('#email').closest(".form-group").addClass("has-error");
							$('#password').closest(".form-group").addClass("has-error");
						}
					}
				});
			}
		});
	});
</script>