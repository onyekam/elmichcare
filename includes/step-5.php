<?php

	include_once("db.php");
	
	$soc_serve = $servExp = $arrest = $arrestExp = $inve = $inveExp = $caut = $cautExp = $give_perm = $con_cor = "";

	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM a_step5 WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){
				$soc_serve 	= $rows['soc_serve'];
				$servExp 	= $rows['servExp'];
				$arrest 	= $rows['arrest'];
				$arrestExp 	= $rows['arrestExp'];
				$inve 		= $rows['inve'];
				$inveExp 	= $rows['inveExp'];
				// $crime 		= $rows['crime'];
				// $crimeExp 	= $rows['crimeExp'];
				// $phys 		= $rows['phys'];
				// $physExp	= $rows['physExp'];
				$caut 		= $rows['caut'];
				$cautExp 	= $rows['cautExp'];
				$give_perm 	= crschk($rows['give_perm']);
				$con_cor 	= crschk($rows['con_cor']);
			}
		}
	}	
?>
						<div id="step-5">

							<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Applicant Declaration</h3>
        <h5 class="text-muted">Protection of Children, Vulnerable Adults and criminal convictions.</h5>        
    </div><!-- /.box-header -->
    <div class="box-body">

        <div class="col-md-12">

            <!--<div class="my-ui-list">
                <ul >
                    <li class="my-li">United Kingdom legislation and guidance relating to the welfare of children and vulnerable adults has at its core, the principle that the welfare of vulnerable persons must be the paramount consideration.</li>
                    <li class="my-li"><strong>Our care Agency</strong> fully supports this principle and therefore, we require that everyone who may come into contact with children and vulnerable persons or have access to their personal details, complete and sign this declaration.</li>
                    <li class="my-li">This record is to ensure that the children and vulnerable person's welfare is safeguarded. It will be kept with the strictest confidence.</li>
                </ul>
            </div>-->
			<br/>
            <p>Has any Social Service Department or Police Service ever conducted an enquiry or investigation into any allegations or concerns that you may pose an actual or potential risk to children or vulnerable adults?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="soc_serve">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>
</select>
<div id="servBox">

</div>
</div><input type="hidden" name="data[UserQuestion][1][user_id]" value="" class="getUsr" id="UserQuestion1UserId"/><input type="hidden" name="data[UserQuestion][1][question_id]" value="8" id="UserQuestion1QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a1" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][1][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion1AnsExtenstion"/></div>                </div>
            </div>
				
			
            <p>Have you ever been convicted of any offence relating to children or vulnerable adults?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="arrest">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>

</select>
<div id="arrestBox">

</div>
</div><input type="hidden" name="data[UserQuestion][2][user_id]" value="" class="getUsr" id="UserQuestion2UserId"/><input type="hidden" name="data[UserQuestion][2][question_id]" value="9" id="UserQuestion2QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a2" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][2][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion2AnsExtenstion"/></div>                </div>
            </div>
				
			
            <p>Have you ever been subject to any safeguarding investigation, criminal investigation or any investigations by previous employer?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="inve">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>

</select>
<div id="inveBox">
	
</div>
</div><input type="hidden" name="data[UserQuestion][3][user_id]" value="" class="getUsr" id="UserQuestion3UserId"/><input type="hidden" name="data[UserQuestion][3][question_id]" value="10" id="UserQuestion3QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a3" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][3][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion3AnsExtenstion"/></div>                </div>
            </div>
				
			
            <!-- <p>Do you have any criminal convictions spent or unspent?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="crime">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>
</select></div><input type="hidden" name="data[UserQuestion][4][user_id]" value="" class="getUsr" id="UserQuestion4UserId"/><input type="hidden" name="data[UserQuestion][4][question_id]" value="11" id="UserQuestion4QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a4" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][4][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion4AnsExtenstion"/></div>                </div>
            </div> -->
				
			
            <!-- <p>Do you have any physical or mental health conditions which may hinder your ability to carry on or work for the purpose of care activities?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="phys">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>
</select></div><input type="hidden" name="data[UserQuestion][5][user_id]" value="" class="getUsr" id="UserQuestion5UserId"/><input type="hidden" name="data[UserQuestion][5][question_id]" value="12" id="UserQuestion5QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a5" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][5][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion5AnsExtenstion"/></div>                </div>
            </div> -->
				
			
            <p>Have you received cautions, reprimands or final warnings which are spent or unspent?<span class="red">*</span></p>
            <div class="row selectItems">
                <div class="col-sm-3 form-group">
                    <div class="input select"><select class="form-control selectVal" required="required" id="caut">
<option value="">Select</option>
<option value="Yes">Yes</option>
<option value="No">No</option>
</select>
<div id="cautBox">

</div>
</div><input type="hidden" name="data[UserQuestion][6][user_id]" value="" class="getUsr" id="UserQuestion6UserId"/><input type="hidden" name="data[UserQuestion][6][question_id]" value="13" id="UserQuestion6QuestionId"/>        
                </div>
                <div class="col-sm-9 pull-right form-group" id="a6" style="display: none;">						 
                   <div class="input text"><input name="data[UserQuestion][6][ans_extenstion]" placeholder="Details" class="form-control detailBox2" type="text" id="UserQuestion6AnsExtenstion"/></div>                </div>
            </div>
			        </div>
        <div class="col-sm-12 form-group">

            <p>I declare that to the best of my knowledge the above information, and that submitted in any accompanying documents, is correct, and</p>
        </div>           

        <div class="col-sm-12 form-group">
                        <label>
                <input type="checkbox" id="give_perm" class="req_check" required <?=$give_perm?>>
				<span class="red">*</span>
                I give permission for Elmich Care to use my information as provided in the application form for the purpose of making employment decisions and if employed, for the company to company to use the information for the  purpose and to share information in line with the laws.</label>
        </div>
        <div class="col-sm-12 form-group">

            <label>
                <input type="checkbox" id="con_cor" class="req_check" required <?=$con_cor?>>
				<span class="red">*</span>
                I give permission for Elmich Care to obtain DBS or check the update services.</label>
                <input type="hidden" name="data[UserProfile][id]" value=""  class="req_check">
                


		</div> 
		<div class="col-sm-12">
			By submitting this form you agree to Elmich Care's <a href="ElmichCareWebsiteTermsConditions.doc">Terms and Conditions</a> and <a href="ElmichCareWebsitePrivacyPolicy.doc">Privacy Policy</a>
		</div>
    </div><!-- /.box-body -->
    <div class="overlay"></div>   
</div> 
							
<script>
	$(function(){
		$("#soc_serve").val("<?=$soc_serve?>").change();
		$("#arrest").val("<?=$arrest?>").change();
		$("#inve").val("<?=$inve?>").change();
		$("#caut").val("<?=$caut?>").change();


		$("#soc_serve").change(function(){
			var soc_serveValue = $("#soc_serve").val().trim();
			console.log(soc_serveValue);
			if (soc_serveValue == "Yes") {
				$("#servBox").append('<input type="text" class="form-control" placeholder="If Yes, Please Explain" value="<?=$servExp?>" id="servExp">');
			} else {
				$("#servBox").empty();
			}
		});
		$("#arrest").change(function(){
			var arrestValue = $("#arrest").val().trim();
			console.log(arrestValue);
			if (arrestValue == "Yes") {
				$("#arrestBox").append('<input type="text" class="form-control" placeholder="If Yes, Please Explain" value="<?=$arrestExp?>" id="arrestExp">');
			} else {
				$("#arrestBox").empty();
			}
		});
		$("#inve").change(function(){
			var inveValue = $("#inve").val().trim();
			if (inveValue == "Yes") {
				$("#inveBox").append('<input type="text" class="form-control" placeholder="If Yes, Please Explain" value="<?=$inveExp?>" id="inveExp">');
			} else {
				$("#inveBox").empty();
			}
		});
		$("#caut").change(function(){
			var cautValue = $("#caut").val().trim();
			console.log(cautValue);
			if (cautValue == "Yes") {
				$("#cautBox").append('<input type="text" class="form-control" placeholder="If Yes, Please Explain" value="<?=$cautExp?>" id="cautExp">');
			} else {
				$("#cautBox").empty();
			}
		});


		$(".buttonFinish").click(function(e){
			var step = $("#PrevStep").val();
			e.preventDefault();
			if(step == "step-4"){
				var soc_serve = arrest = inve = caut = "";
				var give_perm = con_cor = 0;
				
				if($("#give_perm").prop("checked")){
					give_perm = 1;
				}if($("#con_cor").prop("checked")){
					con_cor = 1;
				}
				soc_serve 	= $("#soc_serve").val().trim();
				if (soc_serve == "Yes") {
					var servExp = $("#servExp").val().trim();
				} else {
					servExp = "";
				}
				arrest 		= $("#arrest").val().trim();
				if (arrest == "Yes") {
					var arrestExp 	= $("#arrestExp").val().trim();
				} else {
					arrestExp = "";
				}
				inve 		= $("#inve").val().trim();
				if (inve == "Yes") {
					var inveExp	= $("#inveExp").val().trim();
				} else {
					inveExp = "";
				}
				caut 		= $("#caut").val().trim();
				if (caut == "Yes") {
					var cautExp = $("#cautExp").val().trim();
				} else {
					cautExp = "";
				}
				
				
				if(soc_serve=="" || servExp=="" || arrest=="" || arrestExp=="" || inve=="" || inveExp=="" || caut=="" || cautExp=="" || give_perm==0 || con_cor==0){
					alert("Some fields are empty!");
				}else{
					$(".overlay").show();
					$(".ajax-spinner").show();
					$.ajax({
						type: "POST",
						url: "includes/step5/",
						data: "con_cor="+con_cor+"&give_perm="+give_perm+"&caut="+caut+"&cautExp="+cautExp+"&inve="+inve+"&inveExp="+inveExp+"&arrest="+arrest+"&arrestExp="+arrestExp+"&soc_serve="+soc_serve+"&servExp="+servExp+"&posted=step5",
						cache: false,
						success: function(datum){
							var result = datum.trim();
							if(result == 'correct'){
								$(".overlay").hide();
								$(".ajax-spinner").hide();
								$("#step-5").show();
								$('#innerStep5').addClass("done");
								$('#innerStep5').removeClass("selected");
								$('#step-form-2').hide();
								$('#step-form-3').show();
								window.location = "./#circleBtn1";
							}else{
								alert(datum);
							}
						}
					});
				}
			}
		});
	});
</script>
												
						</div>