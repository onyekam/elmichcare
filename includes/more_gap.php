
<div id="more_gap" class="hide">
	<div class="row gap_row">
		<div class="col-sm-4 form-group">
			<label>Date From</label>
			<div class="input text"><input name="data[GapsInEmployment][{@}][gapDateFrom]" placeholder="dd-mm-yyyy" id="GapsInEmploymentGapDateFrom{@}" class="form-control clonepicker newStart" value="" type="text"/></div>                                                                    
		</div>
		<div class="col-sm-4 form-group">
			<label>Date To</label>
			<div class="input text"><input name="data[GapsInEmployment][{@}][gapDateTo]" id="GapsInEmploymentGapDateTo{@}" placeholder="dd-mm-yyyy" class="form-control clonepicker" value="" type="text"/></div>		</div>
		<div class="col-sm-4 form-group">
			<label>Reason for gap</label>
			<img src="/img/close.png" alt="Remove Gap" class="gap_remove pull-right" style="height: 24px; width: 24px; cursor: pointer;"/>			<div class="input text"><input name="data[GapsInEmployment][{@}][gapReason]" placeholder="Reason for gap" class="form-control " value="" type="text" id="GapsInEmploymentGapReason"/></div>		</div>
	</div>
</div>