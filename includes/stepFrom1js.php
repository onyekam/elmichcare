
<script>
	$(function(){
		$(".overlay").hide();
		$(".ajax-spinner").hide();
		$(".ref3").hide();
		$("#title").blur(function(){
			var a = $("#title").val().trim();
			if(a == ""){
				$('#title').closest(".form-group").addClass("has-error");
			}else{
				$('#title').closest(".form-group").removeClass("has-error");
			}
		});
		$("#f_name").blur(function(){
			var a = $("#f_name").val().trim();
			if(a == ""){
				$('#f_name').closest(".form-group").addClass("has-error");
			}else{
				$('#f_name').closest(".form-group").removeClass("has-error");
			}
		});
		$("#l_name").blur(function(){
			var a = $("#l_name").val().trim();
			if(a == ""){
				$('#l_name').closest(".form-group").addClass("has-error");
			}else{
				$('#l_name').closest(".form-group").removeClass("has-error");
			}
		});
		$("#m_name").blur(function(){
			var a = $("#m_name").val().trim();
			if(a == ""){
				$('#m_name').closest(".form-group").addClass("has-error");
			}else{
				$('#m_name').closest(".form-group").removeClass("has-error");
			}
		});
		$("#email").blur(function(){
			var a = $("#email").val().trim();
			var atpos = a.indexOf("@");
			var dotpos = a.lastIndexOf(".");
			if(a == ""){
				$('#email').closest(".form-group").addClass("has-error");
			}else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=a.length){
				$('#email').closest(".form-group").addClass("has-error");
			}else{
				$.ajax({
					type: 'post',
					url: 'includes/checkdata/',
					data: "email="+a,
					success: function(response) {
						if(response=="OK")	{
							$('#email').closest(".form-group").removeClass("has-error");
						}else{
							$('#email').closest(".form-group").addClass("has-error");
						}
					}
				});
			}
		});
		$("#c_email").blur(function(){
			var a = $("#c_email").val().trim();
			if(a == ""){
				$('#c_email').closest(".form-group").addClass("has-error");
			}else if(a != $("#email").val().trim()){
				$('#c_email').closest(".form-group").addClass("has-error");
			}else{
				$('#c_email').closest(".form-group").removeClass("has-error");
			}
		});
		$("#mobile").blur(function(){
			var a = $("#mobile").val().trim();
			if(a == ""){
				$('#mobile').closest(".form-group").addClass("has-error");
			}else{
				$('#mobile').closest(".form-group").removeClass("has-error");
			}
		});
		$("#dob").blur(function(){
			var a = $("#dob").val().trim();
			if(a == ""){
				$('#dob').closest(".form-group").addClass("has-error");
			}else{
				$('#dob').closest(".form-group").removeClass("has-error");
			}
		});
		// $("#address1").focus(function(){
		// 	var a = $("#dob").val().trim();
		// 	if(a == ""){
		// 		$('#dob').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#dob').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		$("#address1").blur(function(){
			var a = $("#address1").val().trim();
			if(a == ""){
				$('#address1').closest(".form-group").addClass("has-error");
			}else{
				$('#address1').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address2").blur(function(){
			var a = $("#address2").val().trim();
			if(a == ""){
				$('#address2').closest(".form-group").addClass("has-error");
			}else{
				$('#address2').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address_his1").blur(function(){
			var a = $("#address_his1").val().trim();
			if(a == ""){
				$('#address_his1').closest(".form-group").addClass("has-error");
			}else{
				$('#address_his1').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address_his2").blur(function(){
			var a = $("#address_his2").val().trim();
			if(a == ""){
				$('#address_his2').closest(".form-group").addClass("has-error");
			}else{
				$('#address_his2').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address_his3").blur(function(){
			var a = $("#address_his3").val().trim();
			if(a == ""){
				$('#address_his3').closest(".form-group").addClass("has-error");
			}else{
				$('#address_his3').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address_his4").blur(function(){
			var a = $("#address_his4").val().trim();
			if(a == ""){
				$('#address_his4').closest(".form-group").addClass("has-error");
			}else{
				$('#address_his4').closest(".form-group").removeClass("has-error");
			}
		});
		$("#address_his5").blur(function(){
			var a = $("#address_his5").val().trim();
			if(a == ""){
				$('#address_his5').closest(".form-group").addClass("has-error");
			}else{
				$('#address_his5').closest(".form-group").removeClass("has-error");
			}
		});
		$("#town").blur(function(){
			var a = $("#town").val().trim();
			if(a == ""){
				$('#town').closest(".form-group").addClass("has-error");
			}else{
				$('#town').closest(".form-group").removeClass("has-error");
			}
		});
		$("#borough").blur(function(){
			var a = $("#borough").val().trim();
			if(a == ""){
				$('#borough').closest(".form-group").addClass("has-error");
			}else if(a == "10"){
				$('#borough').closest(".form-group").removeClass("has-error");
				$("#otherBorough").blur(function(){
			 		var b = $("#otherBorough").val().trim();
					if(b == "9999"){
		 				$('#otherBorough').closest(".form-group").addClass("has-error");
		 			}else{
		 				$('#otherBorough').closest(".form-group").removeClass("has-error");
		 			}
		 		});
			}
			
			else{
				$('#borough').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#postcode").blur(function(){
			var a = $("#postcode").val().trim();
			if(a == ""){
				$('#postcode').closest(".form-group").addClass("has-error");
			}else{
				$('#postcode').closest(".form-group").removeClass("has-error");
			}
		});
		$("#password").blur(function(){
			var a = $("#password").val().trim();
			if(a == ""){
				$('#password').closest(".form-group").addClass("has-error");
			}else{
				$('#password').closest(".form-group").removeClass("has-error");
			}
		});
		$("#c_password").blur(function(){
			var a = $("#c_password").val().trim();
			if(a == ""){
				$('#c_password').closest(".form-group").addClass("has-error");
			}else if(a != $("#password").val().trim()){
				$('#c_password').closest(".form-group").addClass("has-error");
			}else{
				$('#c_password').closest(".form-group").removeClass("has-error");
			}
		});
		$("#prof").blur(function(){
			var a = $("#prof").val().trim();
			if(a == ""){
				$('#prof').closest(".form-group").addClass("has-error");
			}else{
				$('#prof').closest(".form-group").removeClass("has-error");
			}
		});
		$("#lang").blur(function(){
			var a = $("#lang").val().trim();
			if(a == ""){
				$('#lang').closest(".form-group").addClass("has-error");
			}else{
				$('#lang').closest(".form-group").removeClass("has-error");
			}
		});
		$("#post").blur(function(){
			var a = $("#post").val().trim();
			if(a == ""){
				$('#post').closest(".form-group").addClass("has-error");
			}else if(a == "10"){
				$('#post').closest(".form-group").removeClass("has-error");
				$("#otherPosition").blur(function(){
		 			var b = $("#otherPositionValue").val().trim();
				 	if(b == ""){
				 		$('#otherPositionValue').closest(".form-group").addClass("has-error");
				 	}else{
				 		$('#otherPositionValue').closest(".form-group").removeClass("has-error");
				 	}
				 });
			}
			
			else{
				$('#post').closest(".form-group").removeClass("has-error");
			}
		});
		
		
		$("#expert").blur(function(){
			var a = $("#expert").val().trim();
			if(a == ""){
				$('#expert').closest(".form-group").addClass("has-error");
			}else{
				$('#expert').closest(".form-group").removeClass("has-error");
			}
		});
		$("#dbs").blur(function(){
			var a = $("#dbs").val().trim();
			if(a == ""){
				$('#dbs').closest(".form-group").addClass("has-error");
			}else{
				$('#dbs').closest(".form-group").removeClass("has-error");
			}
		});
		$("#car_license").blur(function(){
			var a = $("#car_license").val().trim();
			if(a == ""){
				$('#car_license').closest(".form-group").addClass("has-error");
			}else{
				$('#car_license').closest(".form-group").removeClass("has-error");
			}
		});
		$("#insure_no").blur(function(){
			var a = $("#insure_no").val().trim();
			if(a == ""){
				$('#insure_no').closest(".form-group").addClass("has-error");
			}else{
				$('#insure_no').closest(".form-group").removeClass("has-error");
			}
		});
		function sendPersInfo(){
			var title 		= $("#title").val().trim();
			var f_name 		= $("#f_name").val().trim();
			var l_name 		= $("#l_name").val().trim();
			var m_name 		= $("#m_name").val().trim();
			var email		= $("#email").val().trim();
			var c_email		= $("#c_email").val().trim();
			var mobile		= $("#mobile").val().trim();
			var dob			= $("#dob").val().trim();
			var address1	= $("#address1").val().trim();
			var address2	= $("#address2").val().trim();
			var address_his1	= $("#address_his1").val().trim();
			var address_his2	= $("#address_his2").val().trim();
			var address_his3	= $("#address_his3").val().trim();
			var address_his4	= $("#address_his4").val().trim();
			var address_his5	= $("#address_his5").val().trim();
			var town		= $("#town").val().trim();
			var borough		= $("#borough").val().trim();
			if (borough == "9999") {
				var otherBorough = $("#otherBorough").val().trim();
			}
			var postcode	= $("#postcode").val().trim();
			var password	= $("#password").val().trim();
			var c_password	= $("#c_password").val().trim();
			var prof		= $("#prof").val().trim();
			var lang		= $("#lang").val().trim();
			var post		= $("#post").val().trim();
			if (post == "10") {
				var otherPosition		= $("#otherPositionValue").val().trim();
			}
			var expert		= $("#expert").val().trim();
			var dbs			= $("#dbs").val().trim();
			var car_license	= $("#car_license").val().trim();
			var insure_no	= $("#insure_no").val().trim();
			
			if(title=="" || f_name=="" || l_name=="" || m_name=="" || email=="" || c_email=="" || mobile=="" || dob=="" || address1=="" || address2=="" || address_his1=="" || address_his2=="" || address_his3=="" || address_his4=="" || address_his5=="" || town=="" || borough=="" || otherBorough=="" || postcode=="" || password=="" || c_password=="" || prof=="" || lang=="" || post=="" || otherPosition=="" || expert=="" || dbs=="" || car_license=="" || insure_no==""){
				alert("Some fields were left empty!");
				$("#title").focus();
			}else{
				$(".overlay").show();
				$(".ajax-spinner").show();
				$.ajax({
					type: "POST",
					url: "includes/stepForm1/",
					data: "title="+title+"&f_name="+f_name+"&l_name="+l_name+"&m_name="+m_name+"&email="+email+"&c_email="+c_email+"&mobile="+mobile+"&dob="+dob+"&address1="+address1+"&address2="+address2+"&address_his1="+address_his1+"&address_his2="+address_his2+"&address_his3="+address_his3+"&address_his4="+address_his4+"&address_his5="+address_his5+"&town="+town+"&borough="+borough+"&otherBorough="+otherBorough+"&postcode="+postcode+"&password="+password+"&prof="+prof+"&lang="+lang+"&post="+post+"&otherPosition="+otherPosition+"&expert="+expert+"&dbs="+dbs+"&car_license="+car_license+"&insure_no="+insure_no+"&posted=stepForm1",
					cache: false,
					success: function(datum){
						
						var result = datum.trim();
						console.log(result);
						if(result == 'correct'){				
							$(".overlay").hide();
							$(".ajax-spinner").hide();
							$("#step-form-1").hide();
							$("#step-form-2").show();
							$("#step-form-3").hide();
							$("#step-2").hide();
							$("#step-3").hide();
							$("#step-4").hide();
							$("#step-5").hide();
							$(".search_form").html("<div class=\"col-md-10\"><h2>Application Form</h2></div><div class=\"col-md-2\"></div>");
							$('#circleBtn2').addClass("btn-warning");
							$('#circleBtn2').removeClass("btn-default");
							$('#circleBtn1').removeClass("btn-warning");
							$('#circleBtn1').addClass("btn-default");
							window.location = "./#circleBtn1";
						}else{
							alert(datum);
						}
					}
				});
			}
		}
		$("#applyBtn1").click(function(){
			sendPersInfo();
		});
		$("#applyBtn2").click(function(){
			sendPersInfo();
		});
		$("#nextBtn1").click(function(){
			sendPersInfo();
		});
		$("#nextBtn2").click(function(){
			sendPersInfo();
		});
	});
</script>
	