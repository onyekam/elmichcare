<?php	
	
	include_once("db.php");
	
	$submitApp = "";
	
	if($userMail != "NONE"){
		$resSF3 = mysqli_query($CONN, "SELECT * FROM p_info WHERE email='$userMail' AND complete='YES'");
		if(mysqli_num_rows($resSF3) > 0){
			$submitApp = "hidden";
		}
	}
?>
	<div id="step-form-3">
		<div class="col-md-12">

			<!-- general form elements -->
			<div class="box box-primary" style="margin-top:-230px">
				<div class="box-header with-border">
					<h3 class="box-title">Terms & Policy</h3>
					<h5 class="text-muted">Consent to the terms of the Data Protection.</h5>
				</div>
				<!-- /.box-header -->


				<div class="box-body signature">
					<div class="col-md-12">
						<p>The information provided will be used to make decisions regarding your selection and employment with Elmich Care and will be used in line with our data protection statement.
Your Application has been submitted successfully.</p>
					</div>


					<div class="col-md-12">                            

						<div class="col-sm-offset-0 col-md-12">
							<div class="checkbox">
								<label>
									<input type="checkbox"  id="sigAgree"> <strong>I declare that, to the best of my knowledge, all parts of this form have been completed and are accurate and apply for the position conditionally upon this declaration.</strong>
								</label>
								<spen class="input-error"></span>
							</div>
						</div>                                                        

					</div>
					<fieldset>
					<legend>Signature</legend>
					<div class="row">
						<div class="form-group col-sm-6">
							<div class="checkbox">	
								<label> Full Name</label>
								<div class="input text"><input placeholder="Full Name" class="form-control" maxlength="110" type="text" id="sigName"/></div>							
							</div>
						</div> 
						<div class="form-group col-sm-6">
							<div class="checkbox">
								<label>Date</label>
								<div class="input text"><input placeholder="Date" class="form-control" readonly="readonly" type="text" id="sigDate"/></div>							
							</div>
						</div> 
					</div>
					</fieldset>


				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button class="btn btn-default prevBtn pull-left" type="button">Previous</button>
					<button class="btn btn-success nextBtn pull-right <?=$submitApp?>" type="submit">Submit Application</button>
					<input type="hidden" value="submit-application" id="WizardCurrStep"/>
				</div>
			</div>
		</div>
	</div>
	
 </form>
 
 <script>
	$(function(){
		var today = new Date(); 
		var dd = today.getDate(); 
		var mm = today.getMonth()+1; //January is 0! 
		var yyyy = today.getFullYear(); 
		if(dd<10){ 
			dd='0'+dd; 
		} if(mm<10){ 
			mm='0'+mm; 
		} 
		var today = dd+'-'+mm+'-'+yyyy; 
		$('#sigDate').val(today);
		
		$(".nextBtn").click(function(e){
			e.preventDefault();
			var sig_agree = 0;
			var sig_name = sig_date = "";
			
			if($("#sigAgree").prop("checked")){
				sig_agree = 1;
			}
			
			sig_name	= $("#sigName").val().trim();
			sig_date	= $("#sigDate").val().trim();
			
			if(sig_name=="" || sig_date=="" || sig_agree==0){
				alert("Some fields are empty!");
			}else{
				$(".overlay").show();
				$(".ajax-spinner").show();
				$.ajax({
					type: "POST",
					url: "includes/finish/",
					data: "sig_name="+sig_name+"&sig_date="+sig_date+"&sig_agree="+sig_agree+"&posted=finish",
					cache: false,
					success: function(datum){
						var result = datum;
						if(result == 'correct'){
							window.location = "./userAcc.php";
						}else{
							$(".overlay").hide();
							$(".ajax-spinner").hide();
							$('#dialog-message').removeClass("hide");
							alert(datum);
						}
					}
				});
			}
		});
	});
 </script>