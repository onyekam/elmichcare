
		<div id="more_edu" class="hide">
			<div class="row edu_row">
				<hr/>
				<div class="col-sm-5 form-group">
					<label>Subjects or Title</label>
					<div class="input text"><input name="data[UserEducation][{@}][edu_subjects]" placeholder="Subjects or Title" class="form-control education_title" type="text" id="UserEducationEduSubjects"/></div>
					<input type="hidden" name="data[UserEducation][{@}][user_id]" class="getUsr" value="" id="UserEducationUserId"/>                                                            
				</div>
				<div class="col-sm-5 form-group">
					<label>Qualification Obtained</label>
					<div class="input text"><input name="data[UserEducation][{@}][institude]" placeholder="Qualification" class="form-control qualification_obt" maxlength="255" type="text" id="UserEducationInstitude"/></div>                                                            
				</div>
				<div class="col-sm-2 form-group">
					<label>Year Awarded</label> 
					<img src="https://daryelcare.com/images/close.png" style="height: 24px; width: 24px; cursor: pointer; margin-top: -15px;" class="edu_remove pull-right" title="Remove Qualification">
<div class="input select"><select name="data[UserEducation][{@}][year_obtained]" placeholder="Year - YYYY" id="UserEducationYearObtained{@}" class="form-control clonepicker year_awarded">
<option value="">select</option>
<option value="1987">1987</option>
<option value="1988">1988</option>
<option value="1989">1989</option>
<option value="1990">1990</option>
<option value="1991">1991</option>
<option value="1992">1992</option>
<option value="1993">1993</option>
<option value="1994">1994</option>
<option value="1995">1995</option>
<option value="1996">1996</option>
<option value="1997">1997</option>
<option value="1998">1998</option>
<option value="1999">1999</option>
<option value="2000">2000</option>
<option value="2001">2001</option>
<option value="2002">2002</option>
<option value="2003">2003</option>
<option value="2004">2004</option>
<option value="2005">2005</option>
<option value="2006">2006</option>
<option value="2007">2007</option>
<option value="2008">2008</option>
<option value="2009">2009</option>
<option value="2010">2010</option>
<option value="2011">2011</option>
<option value="2012">2012</option>
<option value="2013">2013</option>
<option value="2014">2014</option>
<option value="2015">2015</option>
<option value="2016">2016</option>
<option value="2017">2017</option>
</select></div>                                                            
				</div>
			</div>  

		</div> 