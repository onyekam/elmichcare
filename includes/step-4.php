<?php
	
	include_once("db.php");
	
	$a_g = $b_g = $c_g = $d_g = $e_g = $f_g = $g_g = $h_g = $i_g = $j_g = $k_g = $l_g = $m_g = $n_g = $o_g = $p_g = $q_g = $r_g = $s_g = $t_g = $u_g = $v_g = $w_g = $x_g = $a_b = $b_b = $c_b = $d_b = $e_b = $f_b = $g_b = $h_b = $i_b = $j_b = $k_b = $l_b = $m_b = $n_b = $o_b = $p_b = $q_b = $r_b = $s_b = $t_b = $u_b = $v_b = $w_b = $x_b = $a_n = $b_n = $c_n = $d_n = $e_n = $f_n = $g_n = $h_n = $i_n = $j_n = $k_n = $l_n = $m_n = $n_n = $o_n = $p_n = $q_n = $r_n = $s_n = $t_n = $u_n = $v_n = $w_n = $x_n = "";
	
	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM a_step4 WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){
				$a_g = crsrad($rows['a_g']);
				$b_g = crsrad($rows['b_g']);
				$c_g = crsrad($rows['c_g']);
				$d_g = crsrad($rows['d_g']);
				$e_g = crsrad($rows['e_g']);
				$f_g = crsrad($rows['f_g']);
				$g_g = crsrad($rows['g_g']);
				$h_g = crsrad($rows['h_g']);
				$i_g = crsrad($rows['i_g']);
				$j_g = crsrad($rows['j_g']);
				$k_g = crsrad($rows['k_g']);
				$l_g = crsrad($rows['l_g']);
				$m_g = crsrad($rows['m_g']);
				$n_g = crsrad($rows['n_g']);
				$o_g = crsrad($rows['o_g']);
				$p_g = crsrad($rows['p_g']);
				$q_g = crsrad($rows['q_g']);
				$r_g = crsrad($rows['r_g']);
				$s_g = crsrad($rows['s_g']);
				$t_g = crsrad($rows['t_g']);
				$u_g = crsrad($rows['u_g']);
				$v_g = crsrad($rows['v_g']);
				$w_g = crsrad($rows['w_g']);
				$x_g = crsrad($rows['x_g']);
				$a_b = crsrad($rows['a_b']);
				$b_b = crsrad($rows['b_b']);
				$c_b = crsrad($rows['c_b']);
				$d_b = crsrad($rows['d_b']);
				$e_b = crsrad($rows['e_b']);
				$f_b = crsrad($rows['f_b']);
				$g_b = crsrad($rows['g_b']);
				$h_b = crsrad($rows['h_b']);
				$i_b = crsrad($rows['i_b']);
				$j_b = crsrad($rows['j_b']);
				$k_b = crsrad($rows['k_b']);
				$l_b = crsrad($rows['l_b']);
				$m_b = crsrad($rows['m_b']);
				$n_b = crsrad($rows['n_b']);
				$o_b = crsrad($rows['o_b']);
				$p_b = crsrad($rows['p_b']);
				$q_b = crsrad($rows['q_b']);
				$r_b = crsrad($rows['r_b']);
				$s_b = crsrad($rows['s_b']);
				$t_b = crsrad($rows['t_b']);
				$u_b = crsrad($rows['u_b']);
				$v_b = crsrad($rows['v_b']);
				$w_b = crsrad($rows['w_b']);
				$x_b = crsrad($rows['x_b']);
				$a_n = crsrad($rows['a_n']);
				$b_n = crsrad($rows['b_n']);
				$c_n = crsrad($rows['c_n']);
				$d_n = crsrad($rows['d_n']);
				$e_n = crsrad($rows['e_n']);
				$f_n = crsrad($rows['f_n']);
				$g_n = crsrad($rows['g_n']);
				$h_n = crsrad($rows['h_n']);
				$i_n = crsrad($rows['i_n']);
				$j_n = crsrad($rows['j_n']);
				$k_n = crsrad($rows['k_n']);
				$l_n = crsrad($rows['l_n']);
				$m_n = crsrad($rows['m_n']);
				$n_n = crsrad($rows['n_n']);
				$o_n = crsrad($rows['o_n']);
				$p_n = crsrad($rows['p_n']);
				$q_n = crsrad($rows['q_n']);
				$r_n = crsrad($rows['r_n']);
				$s_n = crsrad($rows['s_n']);
				$t_n = crsrad($rows['t_n']);
				$u_n = crsrad($rows['u_n']);
				$v_n = crsrad($rows['v_n']);
				$w_n = crsrad($rows['w_n']);
				$x_n = crsrad($rows['x_n']);
			}
		}
	}

?>
						<div id="step-4">

							<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Skills & Experience</h3>
        <h5 class="text-muted">SKILLS: have you worked with any of the following service user groups?</h5>
        <h5 class="text-muted">Select your level of experience</h5>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
              
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>ADHD</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$a_g?>">
                                <input type="radio" value="Good" id="a_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid  <?=$a_b?>">
                                <input type="radio" value="Basic"  id="a_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid  <?=$a_n?>">
                                <input type="radio" value="None"  id="a_n">None</label>
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="2" name="data[UserSkill][0][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][0][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Administration of medicine</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid  <?=$b_g?>">
                                <input type="radio" value="Good" id="b_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$b_b?>">
                                <input type="radio" value="Basic" id="b_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$b_n?>">
                                <input type="radio" value="None"  id="b_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="28" name="data[UserSkill][1][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][1][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Alzheimers</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$c_g?>">
                                <input type="radio" value="Good" id="c_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$c_b?>">
                                <input type="radio" value="Basic" id="c_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$c_n?>">
                                <input type="radio" value="None"  id="c_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="25" name="data[UserSkill][2][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][2][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Assisting with immobility</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$d_g?>">
                                <input type="radio" value="Good" id="d_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$d_b?>">
                                <input type="radio" value="Basic" id="d_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$d_n?>">
                                <input type="radio" value="None"  id="d_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="14" name="data[UserSkill][3][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][3][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Autism</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$e_g?>">
                                <input type="radio" value="Good" id="e_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$e_b?>">
                                <input type="radio" value="Basic" id="e_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$e_n?>">
                                <input type="radio" value="None"  id="e_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="3" name="data[UserSkill][4][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][4][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Cancer care</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$f_g?>">
                                <input type="radio" value="Good" id="f_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$f_b?>">
                                <input type="radio" value="Basic" id="f_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$f_n?>">
                                <input type="radio" value="None"  id="f_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="23" name="data[UserSkill][5][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][5][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Catheter care</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$g_g?>">
                                <input type="radio" value="Good" id="g_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$g_b?>">
                                <input type="radio" value="Basic" id="g_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$g_n?>">
                                <input type="radio" value="None"  id="g_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="20" name="data[UserSkill][6][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][6][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Cerebral Palsy</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$h_g?>">
                                <input type="radio" value="Good" id="h_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$h_b?>">
                                <input type="radio" value="Basic" id="h_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$h_n?>">
                                <input type="radio" value="None"  id="h_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="8" name="data[UserSkill][7][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][7][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Challenging behaviour</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$i_g?>">
                                <input type="radio" value="Good" id="i_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$i_b?>">
                                <input type="radio" value="Basic" id="i_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$i_n?>">
                                <input type="radio" value="None"  id="i_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="26" name="data[UserSkill][8][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][8][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Dementia care</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$j_g?>">
                                <input type="radio" value="Good" id="j_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$j_b?>">
                                <input type="radio" value="Basic" id="j_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$j_n?>">
                                <input type="radio" value="None"  id="j_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="13" name="data[UserSkill][9][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][9][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Diabetes</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$k_g?>">
                                <input type="radio" value="Good" id="k_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$k_b?>">
                                <input type="radio" value="Basic" id="k_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$k_n?>">
                                <input type="radio" value="None"  id="k_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="22" name="data[UserSkill][10][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][10][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Down' syndrome</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$l_g?>">
                                <input type="radio" value="Good" id="l_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$l_b?>">
                                <input type="radio" value="Basic" id="l_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$l_n?>">
                                <input type="radio" value="None"  id="l_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="9" name="data[UserSkill][11][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][11][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Frail elderly</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$m_g?>">
                                <input type="radio" value="Good" id="m_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$m_b?>">
                                <input type="radio" value="Basic" id="m_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$m_n?>">
                                <input type="radio" value="None"  id="m_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="11" name="data[UserSkill][12][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][12][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Hoists</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$n_g?>">
                                <input type="radio" value="Good" id="n_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$n_b?>">
                                <input type="radio" value="Basic" id="n_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$n_n?>">
                                <input type="radio" value="None"  id="n_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="15" name="data[UserSkill][13][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][13][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Incontinence</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$o_g?>">
                                <input type="radio" value="Good" id="o_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$o_b?>">
                                <input type="radio" value="Basic" id="o_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$o_n?>">
                                <input type="radio" value="None"  id="o_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="24" name="data[UserSkill][14][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][14][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Learning disabilities</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$p_g?>">
                                <input type="radio" value="Good" id="p_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$p_b?>">
                                <input type="radio" value="Basic" id="p_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$p_n?>">
                                <input type="radio" value="None"  id="p_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="12" name="data[UserSkill][15][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][15][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Lewy-Body dementia</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$q_g?>">
                                <input type="radio" value="Good" id="q_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$q_b?>">
                                <input type="radio" value="Basic" id="q_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$q_n?>">
                                <input type="radio" value="None"  id="q_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="27" name="data[UserSkill][16][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][16][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Mental health</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$r_g?>">
                                <input type="radio" value="Good" id="r_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$r_b?>">
                                <input type="radio" value="Basic" id="r_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$r_n?>">
                                <input type="radio" value="None"  id="r_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="17" name="data[UserSkill][17][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][17][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Multiple sclerosis</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$s_g?>">
                                <input type="radio" value="Good" id="s_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$s_b?>">
                                <input type="radio" value="Basic" id="s_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$s_n?>">
                                <input type="radio" value="None"  id="s_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="19" name="data[UserSkill][18][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][18][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Parkinson's disease</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$t_g?>">
                                <input type="radio" value="Good" id="t_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$t_b?>">
                                <input type="radio" value="Basic" id="t_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$t_n?>">
                                <input type="radio" value="None"  id="t_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="16" name="data[UserSkill][19][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][19][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Special need children</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$u_g?>">
                                <input type="radio" value="Good" id="u_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$u_b?>">
                                <input type="radio" value="Basic" id="u_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$u_n?>">
                                <input type="radio" value="None"  id="u_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="5" name="data[UserSkill][20][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][20][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Stroke care</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$v_g?>">
                                <input type="radio" value="Good" id="v_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$v_b?>">
                                <input type="radio" value="Basic" id="v_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$v_n?>">
                                <input type="radio" value="None"  id="v_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="18" name="data[UserSkill][21][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][21][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Terminally III</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$w_g?>">
                                <input type="radio" value="Good" id="w_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$w_b?>">
                                <input type="radio" value="Basic" id="w_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$w_n?>">
                                <input type="radio" value="None"  id="w_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="21" name="data[UserSkill][22][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][22][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                  
                <div class="col-md-6">
                    <h5 class="col-sm-6"><strong>Tube feeding</strong></h5>
                    <div data-toggle="buttons" class="btn-group pull-right col-sm-6">
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$x_g?>">
                                <input type="radio" value="Good" id="x_g">Good</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$x_b?>">
                                <input type="radio" value="Basic" id="x_b">Basic</label>
                            <label class="btn btn-sm btn-primary pink getuserskillid <?=$x_n?>">
                                <input type="radio" value="None"  id="x_n">None</label>
                                               
                        <input disabled="disabled" class="hiddenDatafield"  type="hidden" value="10" name="data[UserSkill][23][skill_id]">
                        <input disabled="disabled" type="hidden" name="data[UserSkill][23][user_id]" value=""class="hiddenDatafield getUsr">

                    </div>  
                    <div class="form-group col-sm-12 p-front"> <hr style="margin: 2px;"> </div>
                </div>
                        </div>
    </div>
    <div class="overlay"></div>   
</div><!-- /.box-body -->
<script>
    $(".getuserskillid").on('click', function () {		

        $(this).parent('div.btn-group').find("input.hiddenDatafield").prop('disabled', false);

    });
</script> 
						   
						   
<script> 
	$(function(){
		$(".buttonNext").click(function(e){
			var step = $("#PrevStep").val();
			e.preventDefault();
			function doRadio(id, id2, id3){
				var gid = '#'+id;
				var gid2 = '#'+id2;
				var gid3 = '#'+id3;
				if($(gid).prop("checked")){
					$(gid2).removeAttr("checked");
					$(gid3).removeAttr("checked");
					return 1;
				}else{
					return 0;
				}
			}
			if(step == "step-3"){
				var a_g = doRadio("a_g", "a_b", "a_n");
				var b_g = doRadio("b_g", "b_b", "b_n");
				var c_g = doRadio("c_g", "c_b", "c_n");
				var d_g = doRadio("d_g", "d_b", "d_n");
				var e_g = doRadio("e_g", "e_b", "e_n");
				var f_g = doRadio("f_g", "f_b", "f_n");
				var g_g = doRadio("g_g", "g_b", "g_n");
				var h_g = doRadio("h_g", "h_b", "h_n");
				var i_g = doRadio("i_g", "i_b", "i_n");
				var j_g = doRadio("j_g", "j_b", "j_n");
				var k_g = doRadio("k_g", "k_b", "k_n");
				var l_g = doRadio("l_g", "l_b", "l_n");
				var m_g = doRadio("m_g", "m_b", "m_n");
				var n_g = doRadio("n_g", "n_b", "n_n");
				var o_g = doRadio("o_g", "o_b", "o_n");
				var p_g = doRadio("p_g", "p_b", "p_n");
				var q_g = doRadio("q_g", "q_b", "q_n");
				var r_g = doRadio("r_g", "r_b", "r_n");
				var s_g = doRadio("s_g", "s_b", "s_n");
				var t_g = doRadio("t_g", "t_b", "t_n");
				var u_g = doRadio("u_g", "u_b", "u_n");
				var v_g = doRadio("v_g", "v_b", "v_n");
				var w_g = doRadio("w_g", "w_b", "w_n");
				var x_g = doRadio("x_g", "x_b", "x_n");
				var a_b = doRadio("a_b", "a_g", "a_n");
				var b_b = doRadio("b_b", "b_g", "b_n");
				var c_b = doRadio("c_b", "c_g", "c_n");
				var d_b = doRadio("d_b", "d_g", "d_n");
				var e_b = doRadio("e_b", "e_g", "e_n");
				var f_b = doRadio("f_b", "f_g", "f_n");
				var g_b = doRadio("g_b", "g_g", "g_n");
				var h_b = doRadio("h_b", "h_g", "h_n");
				var i_b = doRadio("i_b", "i_g", "i_n");
				var j_b = doRadio("j_b", "j_g", "j_n");
				var k_b = doRadio("k_b", "k_g", "k_n");
				var l_b = doRadio("l_b", "l_g", "l_n");
				var m_b = doRadio("m_b", "m_g", "m_n");
				var n_b = doRadio("n_b", "n_g", "n_n");
				var o_b = doRadio("o_b", "o_g", "o_n");
				var p_b = doRadio("p_b", "p_g", "p_n");
				var q_b = doRadio("q_b", "q_g", "q_n");
				var r_b = doRadio("r_b", "r_g", "r_n");
				var s_b = doRadio("s_b", "s_g", "s_n");
				var t_b = doRadio("t_b", "t_g", "t_n");
				var u_b = doRadio("u_b", "u_g", "u_n");
				var v_b = doRadio("v_b", "v_g", "v_n");
				var w_b = doRadio("w_b", "w_g", "w_n");
				var x_b = doRadio("x_b", "x_g", "x_n");
				var a_n = doRadio("a_n", "a_b", "a_g");
				var b_n = doRadio("b_n", "b_b", "b_g");
				var c_n = doRadio("c_n", "c_b", "c_g");
				var d_n = doRadio("d_n", "d_b", "d_g");
				var e_n = doRadio("e_n", "e_b", "e_g");
				var f_n = doRadio("f_n", "f_b", "f_g");
				var g_n = doRadio("g_n", "g_b", "g_g");
				var h_n = doRadio("h_n", "h_b", "h_g");
				var i_n = doRadio("i_n", "i_b", "i_g");
				var j_n = doRadio("j_n", "j_b", "j_g");
				var k_n = doRadio("k_n", "k_b", "k_g");
				var l_n = doRadio("l_n", "l_b", "l_g");
				var m_n = doRadio("m_n", "m_b", "m_g");
				var n_n = doRadio("n_n", "n_b", "n_g");
				var o_n = doRadio("o_n", "o_b", "o_g");
				var p_n = doRadio("p_n", "p_b", "p_g");
				var q_n = doRadio("q_n", "q_b", "q_g");
				var r_n = doRadio("r_n", "r_b", "r_g");
				var s_n = doRadio("s_n", "s_b", "s_g");
				var t_n = doRadio("t_n", "t_b", "t_g");
				var u_n = doRadio("u_n", "u_b", "u_g");
				var v_n = doRadio("v_n", "v_b", "v_g");
				var w_n = doRadio("w_n", "w_b", "w_g");
				var x_n = doRadio("x_n", "x_b", "x_g");
				$(".overlay").show();
				$(".ajax-spinner").show();
				$.ajax({
					type: "POST",
					url: "includes/step4/",
					data: "a_g="+a_g+"&b_g="+b_g+"&c_g="+c_g+"&d_g="+d_g+"&e_g="+e_g+"&f_g="+f_g+"&g_g="+g_g+"&h_g="+h_g+"&i_g="+i_g+"&j_g="+j_g+"&k_g="+k_g+"&l_g="+l_g+"&m_g="+m_g+"&n_g="+n_g+"&o_g="+o_g+"&p_g="+p_g+"&q_g="+q_g+"&r_g="+r_g+"&s_g="+s_g+"&t_g="+t_g+"&u_g="+u_g+"&v_g="+v_g+"&w_g="+w_g+"&x_g="+x_g+"&a_b="+a_b+"&b_b="+b_b+"&c_b="+c_b+"&d_b="+d_b+"&e_b="+e_b+"&f_b="+f_b+"&g_b="+g_b+"&h_b="+h_b+"&i_b="+i_b+"&j_b="+j_b+"&k_b="+k_b+"&l_b="+l_b+"&m_b="+m_b+"&n_b="+n_b+"&o_b="+o_b+"&p_b="+p_b+"&q_b="+q_b+"&r_b="+r_b+"&s_b="+s_b+"&t_b="+t_b+"&u_b="+u_b+"&v_b="+v_b+"&w_b="+w_b+"&x_b="+x_b+"&a_n="+a_n+"&b_n="+b_n+"&c_n="+c_n+"&d_n="+d_n+"&e_n="+e_n+"&f_n="+f_n+"&g_n="+g_n+"&h_n="+h_n+"&i_n="+i_n+"&j_n="+j_n+"&k_n="+k_n+"&l_n="+l_n+"&m_n="+m_n+"&n_n="+n_n+"&o_n="+o_n+"&p_n="+p_n+"&q_n="+q_n+"&r_n="+r_n+"&s_n="+s_n+"&t_n="+t_n+"&u_n="+u_n+"&v_n="+v_n+"&w_n="+w_n+"&x_n="+x_n+"&posted=step4",
					cache: false,
					success: function(datum){
						var result = datum;
						if(result == 'correct'){
							$(".overlay").hide();
							$(".ajax-spinner").hide();
							$("#step-4").hide();
							$("#step-5").show();
							$('#innerStep4').addClass("done");
							$('#innerStep4').removeClass("selected");
							$('#innerStep5').removeClass("disabled");
							$('#innerStep5').addClass("selected");
							$('.buttonFinish').removeClass("buttonDisabled");
							$('.buttonNext').addClass("buttonDisabled");
							$("#PrevStep").val("step-4");
							window.location = "./#circleBtn1";
						}else{
							alert(datum);
						}
					}
				});
			}
		});
				
	});
</script> 
											
						</div>