<?php

	include_once("db.php");
	
	$title = $f_name = $l_name = $m_name = $email = $mobile = $dob = $address1 = $address2 = $address_his1 = $address_his2 = $address_his3 = $address_his4 = $address_his5 = $town = $borough = $otherBorough = $postcode = $password = $prof = $lang = $post = $otherPosition = $expert = $dbs = $car_license = $insure_no = $complete = ""; 
	$step = "NONE";
	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM p_info WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){
				$title 			= $rows['title'];
				$f_name 		= $rows['f_name'];
				$l_name 		= $rows['l_name'];
				$m_name 		= $rows['m_name'];
				$email			= $rows['email'];
				$mobile			= $rows['mobile'];
				$dob			= $rows['dob'];
				$address1		= $rows['address1'];
				$address2		= $rows['address2'];
				$address_his1	= $rows['address_his1'];
				$address_his2	= $rows['address_his2'];
				$address_his3	= $rows['address_his3'];
				$address_his4	= $rows['address_his4'];
				$address_his5	= $rows['address_his5'];
				$town			= $rows['town'];
				$borough		= $rows['borough'];
				$otherBorough	= $rows['otherBorough'];
				$postcode		= $rows['postcode'];
				$password		= $rows['password'];
				$prof			= $rows['prof'];
				$lang			= $rows['lang'];
				$post			= $rows['post'];
				$otherPosition	= $rows['otherPosition'];
				$expert			= $rows['expert'];
				$dbs			= $rows['dbs'];
				$car_license	= $rows['car_license'];
				$insure_no		= $rows['insure_no'];
				$complete		= $rows['complete'];
				$step			= $rows['step'];
			}
		}
	}
	
?>

<div>
<h5 class="nocapitalize homemessage">
If you encounter problems completing this application form, call us or email us. <br/><br/>You can save your application form at any stage and come back to complete it at a any time. 
<br/>Please remember to click submit when completed.<br/>
We make initial selection decisions based on the information in the application form, therefore enter as much information as you can to increase your chances. 
<br/><br/>Click on sign in to start from where you stopped on your application.
</h5>
</div>
<form action="#" id="app_form_steps" role="form" class="form-validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	<div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>	
	<div class="row setup-content" id="step-form-1">
		<div class="box box-warning pinkBorder">
			<div class="box-header with-border">
				<div class="col-md-9">
					<h3 class="box-title">Personal Information</h3>
					<h5 class="text-muted">Fill your personal information and continue to the next step.</h5>
				</div>
				<button id='applyBtn1' class="btn btn-success nextBtn pull-right" type="button">Apply Now</button>
				<button id="nextBtn1" class="btn btn-success nextBtn pull-right" style="display:none;" type="button">Next </button>
				 <span class="ajax-spinner pull-left" style="margin-top: 10px; margin-right: 15px;">
					<i class="fa fa-refresh fa-spin"></i>
				</span>
			</div>				
			<!-- /.box-header -->
			<!-- form start -->
			<div class="box-body">
				<div class="form-group col-md-2">
					<label>Title</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="title">
							<option value="">Select Title</option>
							<option value="Mr">Mr</option>
							<option value="Mrs">Mrs</option>
							<option value="Ms">Ms</option>
							<option value="Miss">Miss</option>
						</select>
					</div>					
				</div>	
				<div class="form-group col-md-4">
					<label>First Name</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="255" type="text" id="f_name" value="<?=$f_name?>"/>
					</div>					
				</div>
				<div class="form-group col-md-3">
					<label>Last Name</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="255" type="text" id="l_name" value="<?=$l_name?>"/>
					</div>					
				</div>
				<div class="form-group col-md-3">
					<label>Middle Name</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="255" type="text" id="m_name" value="<?=$m_name?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Email</label><span class="red">*</span>
					<div class="input email required"><input class="form-control" required="required" maxlength="200" type="email" id="email" value="<?=$email?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Confirm Email</label><span class="red">*</span>
					<div class="input email"><input class="form-control" required="required" type="email" id="c_email" value="<?=$email?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Telephone/Mobile</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="50" type="text" id="mobile" value="<?=$mobile?>"/>
					</div>
				</div>
				<div class="form-group col-md-6 ">
					<label>Date of Birth</label><span class="red">*</span>
					<div class="input-group">
						<div class="input text">
							<input placeholder="Select date" class="form-control" required="required" type="text" id="dob" readonly value="<?=$dob?>"/>
						</div>							
						<span class="input-group-addon" id="cal-btn"><i class="fa fa-calendar"></i></span>
					</div>
					
				</div>

				<div class="form-group col-md-6">
					<label>Current Address</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address1" value="<?=$address1?>"/>
					</div>
				</div>
				<div class="form-group col-md-6">
					<label>Current Address Second Line</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address2" value="<?=$address2?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Address History Year 1</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address_his1" value="<?=$address_his1?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Address History Year 2</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address_his2" value="<?=$address_his2?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Address History Year 3</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address_his3" value="<?=$address_his3?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Address History Year 4</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address_his4" value="<?=$address_his4?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Address History Year 5</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="500" type="text" id="address_his5" value="<?=$address_his5?>"/>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Town</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="255" type="text" id="town" value="<?=$town?>"/>
					</div>
				</div>
				<div class="form-group col-md-6">
					<label>Borough</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="borough">
							<option value="">Select</option>
							<option value="37">Barking and Dagenham</option>
							<option value="18">Barnet</option>
							<option value="8">Bexley</option>
							<option value="28">Brent</option>
							<option value="21">Bromley</option>
							<option value="1">Camden</option>
							<option value="2">City of London</option>
							<option value="22">Croydon</option>
							<option value="29">Ealing</option>
							<option value="19">Enfield</option>
							<option value="9">Greenwich</option>
							<option value="10">Hackney</option>
							<option value="40">Hammersmith and Fulham</option>
							<option value="20">Haringey</option>
							<option value="32">Harrow</option>
							<option value="11">Havering</option>
							<option value="33">Hillingdon</option>
							<option value="34">Hounslow</option>
							<option value="4">Islington</option>
							<option value="3">Kensington and Chelsea</option>
							<option value="39">Kingston upon Thames</option>
							<option value="7">Lambeth</option>
							<option value="12">Lewisham</option>
							<option value="25">Merton</option>
							<option value="13">Newham</option>
							<option value="14">Redbridge</option>
							<option value="35">Richmond upon Thames</option>
							<option value="5">Southwark</option>
							<option value="26">Sutton</option>
							<option value="15">Tower Hamlets</option>
							<option value="38">Waltham Forest </option>
							<option value="27">Wandsworth</option>
							<option value="6">Westminster</option>
							<option value="9999">Non-london Borough</option>
						</select>
					</div>
					<div id="non_london_borough">
						
						
					</div>			
				</div>
				<div class="form-group col-md-6">
					<label>Postcode</label><span class="red">*</span>
					<div class="input text"><input class="form-control" required="required" maxlength="100" type="text" id="postcode" value="<?=$postcode?>"/>
					</div>
				</div>
				 <div class="form-group col-md-6">
					<label>Create Password</label><span class="red">*</span>
					<div class="input password required"><input class="form-control" required="required" type="password" id="password" value="<?=$password?>"/>
					</div>
				</div>
				<div class="form-group col-md-6">
					<label>Confirm Password</label><span class="red">*</span>
					<div class="input password required"><input class="form-control" required="required" type="password" id="c_password" value="<?=$password?>"/>
					</div>
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">
					<hr style="margin: 2px;">
				</div>

				<div class="form-group col-md-6">
					<label>Proficiency in English (if not first language)</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="prof">
							<option value="">Select</option>
							<option value="Fluent">Fluent</option>
							<option value="Good">Good</option>
							<option value="Basic">Basic</option>
							<option value="Weak">Weak</option>
						</select>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Which other languages do you speak?</label><span class="red">*</span>
					<div class="input select">
						<select id="lang" class="form-control select2" required="required">
							<option value="">Select</option>
							<option value="0">Can&#039;t speak another language</option>
							<option value="1">Acehnese</option>
							<option value="2">Acholi</option>
							<option value="3">Afrikaans</option>
							<option value="4">Albanian</option>
							<option value="5">Amharic</option>
							<option value="6">Anyuak/Anuak</option>
							<option value="7">Arabic</option>
							<option value="8">Arakanese / Rakhinz</option>
							<option value="9">Armenian</option>
							<option value="10">Asante/Ashanti</option>
							<option value="11">Assamese</option>
							<option value="12">Assyrian</option>
							<option value="13">Azari</option>
							<option value="14">Azerbaijani</option>
							<option value="15">Baluchi/Balochi</option>
							<option value="16">Bambara</option>
							<option value="18">Basque</option>
							<option value="19">Bassa</option>
							<option value="20">Bemba</option>
							<option value="21">Bengali/Bangla</option>
							<option value="22">Bislama</option>
							<option value="23">Bosnian</option>
							<option value="24">Breton</option>
							<option value="25">Bulgarian</option>
							<option value="26">Byelorussian</option>
							<option value="27">Cantonese (Chinese)</option>
							<option value="28">Catalan</option>
							<option value="29">Cebuano</option>
							<option value="30">Chaldean</option>
							<option value="35">Chi-Nyanja/Nyanja</option>
							<option value="31">Chichewa/Chewa</option>
							<option value="32">Chin</option>
							<option value="33">Chin Haka (Dialect of Chin)</option>
							<option value="34">Chin Zophei</option>
							<option value="36">Chizigula/Zigula</option>
							<option value="37">Coptic</option>
							<option value="38">Croatian</option>
							<option value="39">Czech</option>
							<option value="40">Danish</option>
							<option value="41">Dari</option>
							<option value="42">Dinka</option>
							<option value="43">Divehi</option>
							<option value="44">Dutch</option>
							<option value="45">Dzongkha</option>
							<option value="46">Esperanto</option>
							<option value="47">Estonian</option>
							<option value="48">Ewe</option>
							<option value="49">Falam Chin (Dialect of Chin)</option>
							<option value="50">Fanti/Fante</option>
							<option value="51">Farsi (Persian)</option>
							<option value="52">Fiji Hindi</option>
							<option value="53">Fijian</option>
							<option value="54">Filipino/Tagalog</option>
							<option value="55">Finnish</option>
							<option value="56">Flemish</option>
							<option value="57">French</option>
							<option value="58">Fulani</option>
							<option value="59">Fullah</option>
							<option value="60">Fulliru</option>
							<option value="61">Fuqing </option>
							<option value="62">Fur</option>
							<option value="63">Fuzhou (Chinese)</option>
							<option value="64">Gaelic (Ireland)</option>
							<option value="65">Gaelic (Scotland)</option>
							<option value="66">Galician</option>
							<option value="67">Gan</option>
							<option value="68">Georgian</option>
							<option value="69">German</option>
							<option value="70">Gio/Dan</option>
							<option value="71">Greek</option>
							<option value="72">Gro</option>
							<option value="73">Gujarati</option>
							<option value="74">Hainanese (Chinese)</option>
							<option value="75">Haitian</option>
							<option value="76">Hakka (Chinese)</option>
							<option value="77">Hakka (Timorese)</option>
							<option value="78">Harari</option>
							<option value="79">Hasseniyya/Hassaniya/Hassani (Dialect of Arabic)</option>
							<option value="80">Hausa</option>
							<option value="81">Hawaiian</option>
							<option value="82">Hazaragi</option>
							<option value="83">Hebrew</option>
							<option value="84">Hindi</option>
							<option value="85">Hindustani/Hindi-Urdu</option>
							<option value="86">Hmong</option>
							<option value="87">Hokkien/Fukien/Foochow (Chinese)</option>
							<option value="88">Hungarian</option>
							<option value="89">Icelandic</option>
							<option value="90">Igbo/Ibo</option>
							<option value="91">Ikalanga/Kalanga</option>
							<option value="92">Indonesian</option>
							<option value="93">Italian</option>
							<option value="94">Japanese</option>
							<option value="95">Javanese</option>
							<option value="96">Juba/Arabic Pidgin</option>
							<option value="97">Kachin</option>
							<option value="98">Kakwa</option>
							<option value="99">Kalentin</option>
							<option value="100">Kannadai/Kannarese</option>
							<option value="101">Karen</option>
							<option value="102">Karenni/Kayah</option>
							<option value="103">Kashmiri</option>
							<option value="104">Kazakh</option>
							<option value="105">Khmer</option>
							<option value="106">Khumi (Dialect of Chin)</option>
							<option value="107">Ki-Kongo</option>
							<option value="109">Ki-Luba/Luba</option>
							<option value="108">Kikuyu</option>
							<option value="110">Kingoni</option>
							<option value="111">Kinjanda</option>
							<option value="112">Kinyarwanda</option>
							<option value="113">Kirghiz</option>
							<option value="114">Kirundi (Burundi)</option>
							<option value="115">Kissi</option>
							<option value="116">Kiswahili</option>
							<option value="117">Kono</option>
							<option value="118">Korean</option>
							<option value="119">Kpelle</option>
							<option value="120">Krio</option>
							<option value="121">Kuanua/Tolai</option>
							<option value="122">Kuku</option>
							<option value="123">Kunama</option>
							<option value="124">Kurdish (Badini)</option>
							<option value="125">Kurdish (Faili)</option>
							<option value="126">Kurdish (Gorani)</option>
							<option value="127">Kurdish (Irani/Kurdistani)</option>
							<option value="128">Kurdish (Kurmanji)</option>
							<option value="129">Kurdish (Sorani)</option>
							<option value="130">Kwa</option>
							<option value="131">Lao</option>
							<option value="132">Latin</option>
							<option value="133">Latvian</option>
							<option value="134">Liberian Pidgin</option>
							<option value="135">Limba</option>
							<option value="136">Lingala</option>
							<option value="137">Lisu</option>
							<option value="138">Lithuanian</option>
							<option value="139">Loko</option>
							<option value="140">Loma</option>
							<option value="141">Lori</option>
							<option value="142">Luganda</option>
							<option value="143">Luo</option>
							<option value="144">Luwa</option>
							<option value="145">Macedonian</option>
							<option value="146">Madi</option>
							<option value="147">Malagasy</option>
							<option value="148">Malayalam</option>
							<option value="149">Malaysian/Malay/Cocos Malay</option>
							<option value="150">Malinka</option>
							<option value="151">Maltese</option>
							<option value="152">Mandarin</option>
							<option value="153">Mandingo</option>
							<option value="154">Mano</option>
							<option value="155">Maori</option>
							<option value="156">Mara (Dialect of Chin)</option>
							<option value="157">Marathi</option>
							<option value="158">Maru</option>
							<option value="159">Mashi</option>
							<option value="160">Matu (Dialect of Chin)</option>
							<option value="161">Mauritian Creole</option>
							<option value="162">Mende</option>
							<option value="163">Mina</option>
							<option value="164">Mizo Chin (Dialect of Chin)</option>
							<option value="165">Moldavian</option>
							<option value="166">Mon</option>
							<option value="167">Mongolian</option>
							<option value="168">Moru</option>
							<option value="169">Motu</option>
							<option value="170">Mundari</option>
							<option value="171">Myanmar (Burmese)</option>
							<option value="172">Nauruan</option>
							<option value="173">Ndebele/Jindebele</option>
							<option value="174">Nepali</option>
							<option value="175">Norwegian</option>
							<option value="176">Nubia</option>
							<option value="177">Nuer</option>
							<option value="178">Nyagwara</option>
							<option value="179">Oriya/Odia</option>
							<option value="180">Oromo</option>
							<option value="181">Ov-Ambo</option>
							<option value="182">Papiamento (Creole language of Dutch West-Indies)</option>
							<option value="183">Pashtu/Pashto</option>
							<option value="184">Pedi/Northern Soto</option>
							<option value="185">Pidgin/Neo-Melanesian</option>
							<option value="186">Pijin Solomon Islands</option>
							<option value="187">Pojulu</option>
							<option value="188">Polish</option>
							<option value="189">Portuguese</option>
							<option value="190">Pukapuka</option>
							<option value="191">Pulaar</option>
							<option value="192">Punjabi</option>
							<option value="193">Pwo Karen (Dialect of Karen)</option>
							<option value="194">Quechua</option>
							<option value="195">Rajasthani</option>
							<option value="196">Rohingya (Myanmar)</option>
							<option value="197">Rohinya (Cook Islands)</option>
							<option value="198">Romanian</option>
							<option value="199">Romansch</option>
							<option value="200">Russian</option>
							<option value="201">Saho</option>
							<option value="202">Samoan</option>
							<option value="203">Sanskrit</option>
							<option value="204">Sardinian</option>
							<option value="205">Serbian</option>
							<option value="206">Setswana</option>
							<option value="207">Sgaw Karen (Dialect of Karen)</option>
							<option value="208">Shan</option>
							<option value="209">Shanghainese</option>
							<option value="210">Shilenge</option>
							<option value="211">Shiluk</option>
							<option value="212">Shona/Chishona</option>
							<option value="213">Sichuan</option>
							<option value="214">Sidamic</option>
							<option value="215">Sindhi</option>
							<option value="216">Sinhalese</option>
							<option value="217">Siym (Dialect of Chin)</option>
							<option value="218">Slovak</option>
							<option value="219">Slovene</option>
							<option value="220">Somali</option>
							<option value="221">Sotho</option>
							<option value="222">Spanish</option>
							<option value="223">Sudanese Arabic</option>
							<option value="224">Sukuma</option>
							<option value="225">Susu</option>
							<option value="226">Swahili/Ki-Swahili</option>
							<option value="227">Swazi</option>
							<option value="228">Swedish</option>
							<option value="229">Syriac</option>
							<option value="230">Taiwanese</option>
							<option value="231">Tajik</option>
							<option value="232">Tamil</option>
							<option value="233">Tartar</option>
							<option value="234">Tedim Chin (Dialect of Chin)</option>
							<option value="235">Telugu</option>
							<option value="236">Temne</option>
							<option value="237">Teo Chiew (Chinese)</option>
							<option value="238">Tetum (Timorese)</option>
							<option value="239">Thai</option>
							<option value="240">Tibetan</option>
							<option value="241">Tigre</option>
							<option value="242">Tigrinya</option>
							<option value="243">Tiv</option>
							<option value="244">Tok Pisin</option>
							<option value="245">Tokelauan</option>
							<option value="246">Tongan</option>
							<option value="247">Torres Strait Creole</option>
							<option value="248">Tshiluba</option>
							<option value="249">Tsonga</option>
							<option value="250">Turkish</option>
							<option value="251">Turkmen</option>
							<option value="252">Twi</option>
							<option value="253">Uighur</option>
							<option value="254">Ukrainian</option>
							<option value="255">Urdu</option>
							<option value="256">Uzbek</option>
							<option value="257">Vietnamese</option>
							<option value="258">Watchi</option>
							<option value="259">Welsh</option>
							<option value="260">Wendish</option>
							<option value="261">Wolof</option>
							<option value="262">Xhosa</option>
							<option value="263">Yalunka</option>
							<option value="264">Yiddish</option>
							<option value="265">Yoruba</option>
							<option value="266">Zande</option>
							<option value="267">Zomi (Dialect of Chin)</option>
							<option value="268">Zonot (Dialect of Chin)</option>
							<option value="269">Zulu</option>
						</select>
					</div>					
				</div>		
				 <div class="col-sm-12 form-field" id="langThis">
						<!--a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="lang_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Another Language</a-->
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">
					<hr style="margin: 2px;">
				</div>			
				<div class="form-group col-md-4" >
					<label class="txt-info">Position applied for</label><span class="red">*</span> 
					<div class="input select">
						<select class="form-control" id="post">
							<option value="">Select</option>
							<option value="1">Care assistant</option>
							<option value="5">Care Coordinator</option>
							<option value="4">Deputy Manager</option>
							<!-- <option value="6">Field Care Supervisor</option> -->
							<option value="3">Registered Manager</option>
							<option value="2">Support Worker</option>
							<option value="12">Special Needs Childcare</option>
							<option value="9">Tutor</option>
							<option value="10">Others</option>
						</select>
					</div>						 
				</div>
				<div id="otherPosition">
					
				</div>
				
				<div class="form-group col-md-4 hide" id="DoPersonalCare">
					<!--label>Are you willing to do personal care for?</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" id="UserApplicationPersonalCare">
							<option value="">select</option>
							<option value="Male Only">Male Only</option>
							<option value="Female Only">Female Only</option>
							<option value="Both">Both</option>
						</select>
					</div-->	
				</div>
				<div class="form-group col-md-4">
					<label>Total experience in the position applied for</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="expert">
							<option value="">Select</option>
							<option value="not_previously_employed">Not previously employed</option>
							<option value="less_than_3_months">Less than 3 months</option>
							<option value="less_than_1_year">Less than 1 year</option>
							<option value="1_2_years">5-8 years</option>
							<option value="2_5_years">2-5 years</option>
							<option value="8_10_years">8-10 years</option>
						</select>
					</div>
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">
					<hr style="margin: 2px;">
				</div>
				<div class="form-group col-md-6">
					<label>Do you have updated DBS?</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="dbs">
							<option value="">Select</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					</div>					
				</div>
				<div class="form-group col-md-6">
					<label>Do you currently have your own car and licence?</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="car_license">
							<option value="">Select</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					</div> 						
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">
					<hr style="margin: 2px;">
				</div>
				<div class="form-group col-md-6">
					<label>National Insurance Number</label><span class="red">*</span>
					 <div class="input text">
						<input placeholder="National Insurance Number" class="form-control" required="required" maxlength="255" type="text" id="insure_no" value="<?=$insure_no?>"/>
					</div>   
				</div>
			</div>
			<div class="overlay"></div>    
			<!-- /.box-body -->
			<div class="box-footer">
				<button id='applyBtn2' class="btn btn-success nextBtn pull-right" type="button">Apply Now</button>
				<button id="nextBtn2" class="btn btn-success nextBtn pull-right" style="display:none;" type="button" data-step=0>Next </button>
				<input type="hidden" value="personal-info" id="WizardCurrStep"/>    						
			</div>
		</div>
		<!-- /.box -->
	</div>
</form>
<?php include_once("includes/stepFrom1js.php");?>
<script>
	$(function(){
		$("#title").val("<?=$title?>").change();
		$("#borough").val("<?=$borough?>").change();
		$("#prof").val("<?=$prof?>").change();
		$("#lang").val("<?=$lang?>").change();
		$("#post").val("<?=$post?>").change();
		$("#expert").val("<?=$expert?>").change();
		$("#dbs").val("<?=$dbs?>").change();
		$("#car_license").val("<?=$car_license?>").change();

		
			$("#borough").change(function(){
				var borough = $("#borough").val();
				console.log(borough);
			if(borough == "9999"){
				$('#non_london_borough').append("<div class='input text'><label>Non-London Borough</label><input type='text' placeholder='Name of Borough' name='otherBorough' id='otherBorough' value='<?=$otherBorough?>' class='form-control'/></div>");
			} else {
				$('#non_london_borough').empty();
			}
			});

			$("#post").change(function(){
				var post = $("#post").val();
				if(post == "10"){
					$("#otherPosition").append("<div class='form-group col-md-4'><label class='txt-info'>If Others please name</label><span class='red'>*</span><div class='input select'><input type='text' class='form-control' name='otherPositionValue' id='otherPositionValue' value='<?=$otherPosition?>'/></div></div>");
				} else {
					$("#otherPosition").empty();
				}
			});
			
		

		var userMail = "<?=$userMail?>";
		if(userMail != ""){
			$('#nextBtn1').show();
			$('#nextBtn2').show();
			$('#applyBtn1').hide();
			$('#applyBtn2').hide();
			$('#login1').hide();
			$('#login2').show();
		}
	});
</script>