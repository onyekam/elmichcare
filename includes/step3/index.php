<?php
	session_start();
	
	if(isset($_POST['posted'])){
		include_once("db.php");
		$institution 	= trim(mysqli_real_escape_string($CONN, $_POST['institution']));
		$institution2 	= trim(mysqli_real_escape_string($CONN, $_POST['institution2']));
		$institution3 	= trim(mysqli_real_escape_string($CONN, $_POST['institution3']));
		$institution4 	= trim(mysqli_real_escape_string($CONN, $_POST['institution4']));
		$qual_sub 		= trim(mysqli_real_escape_string($CONN, $_POST['qual_sub']));
		$qual_sub2		= trim(mysqli_real_escape_string($CONN, $_POST['qual_sub2']));
		$qual_sub3 		= trim(mysqli_real_escape_string($CONN, $_POST['qual_sub3']));
		$qual_sub4 		= trim(mysqli_real_escape_string($CONN, $_POST['qual_sub4']));
		$qual_obt	 	= trim(mysqli_real_escape_string($CONN, $_POST['qual_obt']));
		$qual_obt2	 	= trim(mysqli_real_escape_string($CONN, $_POST['qual_obt2']));
		$qual_obt3	 	= trim(mysqli_real_escape_string($CONN, $_POST['qual_obt3']));
		$qual_obt4	 	= trim(mysqli_real_escape_string($CONN, $_POST['qual_obt4']));
		$qual_yr		= trim(mysqli_real_escape_string($CONN, $_POST['qual_yr']));
		$qual_yr2		= trim(mysqli_real_escape_string($CONN, $_POST['qual_yr2']));
		$qual_yr3		= trim(mysqli_real_escape_string($CONN, $_POST['qual_yr3']));
		$qual_yr4		= trim(mysqli_real_escape_string($CONN, $_POST['qual_yr4']));
		$ref1_name		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_name']));
		$ref1_company	= trim(mysqli_real_escape_string($CONN, $_POST['ref1_company']));
		$ref1_job		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_job']));
		$ref1_email		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_email']));
		$ref1_address1	= trim(mysqli_real_escape_string($CONN, $_POST['ref1_address1']));
		$ref1_address2	= trim(mysqli_real_escape_string($CONN, $_POST['ref1_address2']));
		$ref1_town		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_town']));
		$ref1_phone		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_phone']));
		$ref1_postcode	= trim(mysqli_real_escape_string($CONN, $_POST['ref1_postcode']));
		$ref1_yes		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_yes']));
		$ref1_no		= trim(mysqli_real_escape_string($CONN, $_POST['ref1_no']));
		$ref2_name		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_name']));
		$ref2_company	= trim(mysqli_real_escape_string($CONN, $_POST['ref2_company']));
		$ref2_job		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_job']));
		$ref2_email		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_email']));
		$ref2_address1	= trim(mysqli_real_escape_string($CONN, $_POST['ref2_address1']));
		$ref2_address2	= trim(mysqli_real_escape_string($CONN, $_POST['ref2_address2']));
		$ref2_town		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_town']));
		$ref2_phone		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_phone']));
		$ref2_postcode	= trim(mysqli_real_escape_string($CONN, $_POST['ref2_postcode']));
		$ref2_yes		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_yes']));
		$ref2_no		= trim(mysqli_real_escape_string($CONN, $_POST['ref2_no']));
		$email			= $_SESSION['userMail'];
		
		$str = "SELECT email FROM a_step3 WHERE email='$email'";
		$res = mysqli_query($CONN, $str);
		$row = mysqli_num_rows($res);
		
		$actStr = "";
		if($row > 0){
			$actStr = "UPDATE a_step3 SET institution='$institution', qual_sub='$qual_sub', qual_obt='$qual_obt', qual_yr='$qual_yr', institution2='$institution2', qual_sub2='$qual_sub2', qual_obt2='$qual_obt2', qual_yr2='$qual_yr2',institution3='$institution3', qual_sub3='$qual_sub3', qual_obt3='$qual_obt3', qual_yr3='$qual_yr3', institution4='$institution4', qual_sub4='$qual_sub4', qual_obt4='$qual_obt4', qual_yr4='$qual_yr4', ref1_name='$ref1_name', ref1_company='$ref1_company', ref1_job='$ref1_job', ref1_email='$ref1_email', ref1_address1='$ref1_address1', ref1_address2='$ref1_address2', ref1_town='$ref1_town', ref1_phone='$ref1_phone', ref1_postcode='$ref1_postcode', ref1_yes='$ref1_yes', ref1_no='$ref1_no', ref2_name='$ref2_name', ref2_company='$ref2_company', ref2_job='$ref2_job', ref2_email='$ref2_email', ref2_address1='$ref2_address1', ref2_address2='$ref2_address2', ref2_town='$ref2_town', ref2_phone='$ref2_phone', ref2_postcode='$ref2_postcode', ref2_yes='$ref2_yes', ref2_no='$ref2_no' WHERE email='$email'";
		}else{
			$actStr = "INSERT INTO a_step3 VALUES('$institution', '$qual_sub', '$qual_obt', '$qual_yr', '$institution2', '$qual_sub2', '$qual_obt2', '$qual_yr2', '$institution3', '$qual_sub3', '$qual_obt3', '$qual_yr3', '$institution4', '$qual_sub4', '$qual_obt4', '$qual_yr4','$ref1_name', '$ref1_company', '$ref1_job', '$ref1_email', '$ref1_address1', '$ref1_address2', '$ref1_town', '$ref1_phone', '$ref1_postcode', '$ref1_yes', '$ref1_no', '$ref2_name', '$ref2_company', '$ref2_job', '$ref2_email', '$ref2_address1', '$ref2_address2', '$ref2_town', '$ref2_phone', '$ref2_postcode', '$ref2_yes', '$ref2_no', '$email')";
		}
		
		$actRes = mysqli_query($CONN, $actStr);
		if($actRes == 1){
			mysqli_query($CONN, "UPDATE p_info SET step='step3' WHERE email='$email'");
			echo "correct";
		}else{
			echo 'Connection failed: ' . mysqli_error($CONN);
		}
	}
?>