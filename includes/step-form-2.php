
	<div class="row setup-content" id="step-form-2" style="display: block;">
		<div class="box box-warning">
				<div class="box-header with-border">
					<div class="col-md-9">
						<h3 class="box-title">Application Form</h3>
						<h5 class="text-muted">This form having various section. Fill all sections to boost your job search.</h5>
					</div>

				   
					
					 <button class="btn btn-success nextBtn pull-right job-app hide" type="button">Next</button>
					<button class="btn btn-default prevBtn pull-right" type="button" style="margin-right: 10px;" id="backBtn1">
						Back to Personal Information
					</button>
					

				</div>				
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">

<form action="#" id="form-application" role="form" class="form-validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	<div style="display:block;"><input type="hidden" name="_method" value="POST"></div>							 
					<!-- Smart Wizard -->
					<div id="wizard" class="swMain">
						<div class="actionBar">
							<div class="msgBox">
								<div class="content"></div>
								<a href="#" class="close">X</a>
							</div>
							<div class="loader">Loading</div>
							<a href="#" class="buttonFinish buttonDisabled">Finish</a>
							<a href="#" class="buttonNext">Next</a>
							<a href="#" class="buttonPrevious buttonDisabled">Previous</a>
						</div>
						
						<ul id="myInnerWizard" class="clearfix anchor">
							<li>
								<a id="innerStep1" href="#step-1" class="selected" rel="1">
									<span class="stepDesc">Availability<br>
										<small>Specify days and times</small>
									</span>
								</a>
							</li>
							<li>
								<a id="innerStep2" href="#step-2" class="disabled" rel="2">
									<span class="stepDesc">Employment History<br>
										<small>Employment Experience</small>
									</span>
								</a>
							</li>
							<li>
								<a id="innerStep3" href="#step-3" class="disabled" rel="3">
									<span class="stepDesc">Education<br>
										<small>Specify your qualifications</small>
									</span>                   
								</a>
							</li>
							<li>
								<a id="innerStep4" href="#step-4" class="disabled" rel="4">
									<span class="stepDesc">Skills &amp; Experience<br>
										<small>Your skills and experience</small>
									</span>                   
								</a>
							</li>
							<li>
								<a id="innerStep5" href="#step-5" class="disabled" rel="5">
									<span class="stepDesc">Declaration<br>
										<small>Applicant Form Declaration</small>
									</span>                   
								</a>
							</li>
						</ul>
						<input type="hidden" id="PrevStep">    
						<input type="hidden" id="NextStep">
						
					<?php include_once("includes/step-1.php");?>
					
					<?php include_once("includes/step-2.php");?>
	
					<?php include_once("includes/step-3.php");?>
	
					<?php include_once("includes/step-4.php");?>
	
					<?php include_once("includes/step-5.php");?>
	
						
								
								<div class="actionBar">
									<div class="msgBox">
										<div class="content"></div>
										<a href="#" class="close">X</a>
									</div>
									<div class="loader">Loading</div>
									<a href="#" class="buttonFinish buttonDisabled">Finish</a>
									<a href="#" class="buttonNext">Next</a>
									<a href="#" class="buttonPrevious buttonDisabled">Previous</a>
								</div>
							</div>
						</div>
					</form>
				</div>              
				
				<div class="box-footer">					
					<button class="btn btn-default prevBtn pull-right" type="button" style="margin-right: 10px;" id="backBtn2">Back to Personal Information</button>
					<button class="btn btn-success nextBtn pull-right job-app hide" type="button" data-step="1">Next</button>
					<input type="hidden" name="wizard-step" value="application-form"> 
				</div>
			</div>
	</div>
   