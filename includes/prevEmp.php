
<div id="prevEmp" class="hide">

	<div class="form-group col-md-12" style="margin-top: 5px;">
		<div class="col-sm-6">
			<h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Previous employers</span></strong></h5>
		</div>
		<div class="col-sm-6" style="margin-top: 10px;">
			<button class="btn btn-xs btn-danger pull-right del_emp" type="button" style=""> <i class="fa fa-minus"></i> &nbsp;Remove</button>
		</div>
		<div class="col-sm-12"><hr style="margin: 2px;"></div>

	</div>

	<div class="col-md-12 emp_hist_block">		
		<div class="col-sm-6 form-group">
			<label>Employer</label><span class="red">*</span>
			<div class="input text"><input name="data[UserExperience][{@}][company]" placeholder="Employer Name" class="form-control emp-company" required="required" value="" maxlength="255" type="text" id="UserExperienceCompany"/></div><input type="hidden" name="data[UserExperience][{@}][user_id]" class="getUsr" value="" id="UserId"/>                                                         
		</div>
		<div class="col-sm-6 form-group">
			<label>Address</label><span class="red">*</span>
			<div class="input text"><input name="data[UserExperience][{@}][address]" placeholder="Address" class="form-control emp-address" required="required" value="" maxlength="500" type="text" id="UserExperienceAddress"/></div> 
		</div>
		<div class="col-sm-6 form-group">
			<label>Position Held</label><span class="red">*</span>
			<div class="input text"><input name="data[UserExperience][{@}][position]" placeholder="Position" class="form-control emp-position" required="required" value="" maxlength="100" type="text" id="UserExperiencePosition"/></div>                                                        
		</div>
		<div class="col-sm-6 form-group">
			<label>Telephone Number</label><span class="red">*</span>
			<div class="input text"><input name="data[UserExperience][{@}][phone]" placeholder="Contact Number" class="form-control emp-phone" required="required" value="" maxlength="100" type="text" id="UserExperiencePhone"/></div>		</div>
		<div class="col-sm-6 ">
			<div class="row">
				<div class="col-sm-6 form-group" id="testinput">
					<label>From</label><span class="red">*</span>
					<div class="input text"><input name="data[UserExperience][{@}][year_start]" id="UserExperienceYearStart{@}" placeholder="dd-mm-yyyy" class="form-control clonepicker date_from" required="required" value="" maxlength="255" type="text"/></div>                                                                               
				</div>
				<div class="col-sm-6 form-group">
					<label>To</label><span class="red">*</span>
					<div class="input text"><input name="data[UserExperience][{@}][year_end]" id="UserExperienceYearEnd{@}" placeholder="dd-mm-yyyy" class="form-control clonepicker date_to" required="required" value="" maxlength="255" type="text"/></div>    
				</div>
			</div>
		</div>
		<div class="col-sm-3 form-group">
			<label>Leaving date or notice  (if relevant)</label>
			<div class="input text"><input name="data[UserExperience][{@}][notice]" id="UserExperienceNotice{@}" placeholder="dd-mm-yyyy" class="form-control clonepicker emp-notice" required="required" value="" maxlength="255" type="text"/></div>                                                        
		</div>
		<div class="col-sm-3 form-group">
			<label>When can you start?</label>
			<div class="input text"><input name="data[UserExperience][{@}][notice]" id="UserExperienceNotice{@}" placeholder="dd-mm-yyyy" class="form-control clonepicker start_when" required="required" value="" maxlength="255" type="text"/></div>                                                        
		</div>
		<div class="col-sm-6 form-group">
			<label>Key Tasks/Responsibilities</label>
			<div class="input textarea"><textarea name="data[UserExperience][{@}][job_desc]" placeholder="" class="form-control emp-job_desc" cols="30" rows="6" id="UserExperienceJobDesc"></textarea></div>                                                        
		</div>
		<div class="col-sm-6 form-group">
			<label>Reason for leaving</label><span class="red">*</span>
			<div class="input textarea"><textarea name="data[UserExperience][{@}][reason_leaving]" placeholder="" class="form-control emp-reason_leaving" cols="30" rows="6" id="UserExperienceReasonLeaving"></textarea></div>                                                        
		</div>
	</div> 
</div>
