
<script type="text/javascript">



var blockLang = $("#lang_block").html();
			
			
	var iWizard = {
	   navListItems : $('div.setup-panel'),
	   allWells : $('.setup-content'),
	   allNextBtn : $('.nextBtn'),
	   allPrevBtn : $('.prevBtn'),

	   allWells : $('.setup-content'),
	   currentStep : 0,
	   steps : ["#step-form-1","#step-form-2","#step-form-3","#step-form-4"],
	   goto:function(step){
		   $(this.navListItems).find(".stepwizard-step a").removeClass('btn-warning').removeClass('btn-warning-pink').addClass('btn-default');
		   $(this.navListItems).find(".stepwizard-step:eq("+step+ ") a").removeClass('btn-default').addClass('btn-warning').addClass('btn-warning-pink').removeAttr('disabled'); 
		   $(this.allWells).hide();
		   $target = $(this.steps[step]),
		   $target.show();
		   $target.find('input:first').focus();  
		   //console.log(this.steps[step]);
		   this.allNextBtn.data('step-id',this.steps[step]);
		   this.allPrevBtn.data('step-id',this.steps[step]);
	   },
	   init:function(){
		   var _this = this;
		  this.allNextBtn.on('click',function (event) {
			  var current = $(this).data('step');
			  var next = (_this.currentStep <_this.steps.length)?_this.currentStep+1:0;
			  _this.currentStep = next;
			  //_this.goto(_this.currentStep ); 
			  $(this).trigger('iWizard.click');
		  });
		  this.allPrevBtn.on('click',function (event) {
			  var current = $(this).data('step');
			  var prev = (current >0)?_this.currentStep-1:0;
			  _this.currentStep = prev;
			  //_this.goto(_this.currentStep );
			  $(this).trigger('iWizard.click');
		  });
		 _this.goto(_this.currentStep );
	   }
   }
	$(".ajax-spinner").hide();

	$(".overlay").hide();
   

$(".ref3").hide();
$("#step-form-2").hide();
$("#step-form-3").hide();

	$(document).ready(function () {
		
		$('#UserApplicationJobCategoryId').change(function(){
			
			var UserApplicationJobCategoryId = $('#UserApplicationJobCategoryId').val();
			
			if (UserApplicationJobCategoryId =='1') {
				
				$('#DoPersonalCare').removeClass('hide').addClass('show');

			}else{

				$('#DoPersonalCare').removeClass('show').addClass('hide');
			}

		});
		///////////////////////////////////////////////
		var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1; //January is 0!

	    var yyyy = today.getFullYear();
	    if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 
	   
	    var today = yyyy+'-'+mm+'-'+dd
	    
	    if ( $('#UserProfileSignatureDate').val()=='') {

		    $('#UserProfileSignatureDate').val(today);
		}

		///////////////////////////////////////////////
		$('#UserJobAppliedApplyForm').submit(function(){

			var tick 						= $('#useragrfee').prop('checked');
			var UserProfileSignatureName 	=  $('#UserProfileSignatureName').val();
		
			if (!tick) {
				$('.input-error').html('Please tick this checkbox before submitting application form.');
				isValid = false;
				return false;
			}
			if ($('#UserProfileSignatureName').length>0 && UserProfileSignatureName == '') {
					   
				$('#UserProfileSignatureName').closest(".form-group").addClass("has-error");
				isValid = false;
				return false;
			   
			}
			if ($('#UserProfileSignatureName').length>0 && UserProfileSignatureName!='' && UserProfileFName.length == 1 && UserProfileSignatureName.match(stringPatternOnlyOneSpace)) {

				$('#UserProfileSignatureName').closest(".form-group").addClass("has-error");
				$('#UserProfileSignatureName').after('<span class="input-error">Please enter full name or remove this space.</span>');
				isValid = false;
				return false;
			}
			if ($('#UserProfileSignatureName').length>0 && UserProfileSignatureName!='' && UserProfileFName.length > 1 && UserProfileSignatureName.match(stringPatternMultipleSpace)) {

				$('#UserProfileSignatureName').closest(".form-group").addClass("has-error");
				$('#UserProfileSignatureName').after('<span class="input-error">Only one space is allow in full name.</span>');
				isValid = false;
				return false;
			}
		});
		///////////////////////////////////////////////////
		$("input[type='text'],input[type='url'],input[type='email'],input[type='password']").click(function(){
			$(this).next('span').remove();

		});
		///////////////////////////////////////////////////////////
		/*
		$('#UserApplicationJobCategoryId').change(function(){
			
			var url = $(this).data('url');
			var cId = $(this).val();
			var cId2= $(this);

			$.ajax({
				type: "POST",
				url: "",
				data: {cId:cId},
				success: function (response) {
					if (response) {
						alert('You have applied once for this job please choose another.');
						cId2.val('');
						cId2.closest(".form-group").addClass("has-error");
						return false;
					}else{
						cId2.closest(".form-group").removeClass("has-error");
					}
				},
				error: function () {
					alert('Application not submitted. Please try laterw');
				}
			});
		});
		*/
		
		$(".setdatePicker").datepicker({ format:"dd/mm/yyyy",  autoclose: true});

		/////////////////////////////////////////////////////
		$("#lang_add").on('click', function () {
			$("#langThis").before($("#lang_block").html());
		});
		/////////////////////////////////////////////////////////
		$(document).on('click', ".lang_remove", function () {
			$(this).closest('.info_block_row').slideUp('slow', function () {
				$(this).detach();
			});
		});
		/////////////////////////////////////////////////////////////        
		  $("#sel_lang").on('change', function(){ 
			 if ($(this).children('option:selected').index() == 1) {
				  $("#langThis").hide();
				 $(".info_block_row").detach();
				 $("#lang_block").html(blockLang);
				 
			 } else {
				  $(".info_block_row").show();
				 $("#langThis").show();
			 }
		});
		 ///////////////////////////////////////////

		$('.tooltip-wrapper').tooltip({position: "bottom"});

	   /*
	   * set the condition for select child bar
	   */
		var childBar            = $('#last_selected_value').val();
		var selectedChildBar    = '';
		var selectedMainBar     = 0;
		var efb 				= false;
		var enableAllStepsV		= false;
	   
		if (childBar=='') {
			selectedChildBar    = '0';
			selectedMainBar     = 0;
		}
		if (childBar=='personal-info') {
			
			selectedChildBar    = 0;
			selectedMainBar     = 1;
		}
		if (childBar=='step-1') {
			selectedChildBar    = 1;
			selectedMainBar     = 1;
		}
		if (childBar=='step-2') {
			selectedChildBar    = 2;
			selectedMainBar     = 1;
		}
		if (childBar=='step-3') {
			selectedChildBar    = 3;
			selectedMainBar     = 1;
		}
		if (childBar=='step-4') {
			selectedChildBar    = 4;
			selectedMainBar     = 1;
			efb 				= true;
		}
		
		if (childBar=='final') {
			selectedChildBar    = '';
			selectedMainBar     = 2;
		}
		
		if (childBar=='completed') {
			selectedChildBar    = 0;
			selectedMainBar     = 0;
			enableAllStepsV      = true;
		}
		 //enableFinishButton: false, 
		$('#wizard').smartWizard({
			enableFinishButton: true, 
			selected: selectedChildBar,
			enableAllSteps:enableAllStepsV,  
			onFinish:function () {
				
				$(".currStep").attr('value', '4'); // last step of application form wizard
				$.ajax({
						type: "POST",
						//url: "adds",
						url: "",
						data: $('#form-application').serialize(),
						beforeSend: function () {

							$(".overlay").show();
							$('.loader').show();
						},
						success: function (response) {
							$('.loader').hide();
							$(".overlay").hide();
							iWizard.goto(2);                                    

						},
						error: function () {
							alert('Application not submitted. Please try laterw1');
						}
				});
			}
		});
	
	$('#wizard .actionBar .buttonFinish').on('click',function(){
		 
		$(".form-group").removeClass("has-error");

		var UserQuestion1Answer            = $('#UserQuestion1Answer').val();
		var UserQuestion1AnsExtenstion     = $('#UserQuestion1AnsExtenstion').val();
		 var UserQuestion2Answer           = $('#UserQuestion2Answer').val();
		var UserQuestion2AnsExtenstion     = $('#UserQuestion2AnsExtenstion').val();
		 var UserQuestion3Answer           = $('#UserQuestion3Answer').val();
		var UserQuestion3AnsExtenstion     = $('#UserQuestion3AnsExtenstion').val();
		 var UserQuestion4Answer           = $('#UserQuestion4Answer').val();
		var UserQuestion4AnsExtenstion     = $('#UserQuestion4AnsExtenstion').val();
		 var UserQuestion5Answer           = $('#UserQuestion5Answer').val();
		var UserQuestion5AnsExtenstion     = $('#UserQuestion5AnsExtenstion').val();
		 var UserQuestion6Answer           = $('#UserQuestion6Answer').val();
		var UserQuestion6AnsExtenstion     = $('#UserQuestion6AnsExtenstion').val();
		var GivePermissionForAnyEnq        = $('#GivePermissionForAnyEnq').prop('checked');
		var ConfirmMyInfoCorrect           = $('#ConfirmMyInfoCorrect').prop('checked'); 

		
				
		   var ser_dept = $(this).find('.selectVal').val();
		   var detailBox2 = $(this).find('.detailBox2').val();

		  $('select').change(function(){

                $(this).removeClass('input-error');             
                $(this).closest(".form-group").removeClass("has-error");
                $(this).next('span').remove();

            });

			if (UserQuestion1Answer == 'select') {
	   
				$('#UserQuestion1Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion1Answer == 'Yes' && UserQuestion1AnsExtenstion=='') {
	   
			   $('#UserQuestion1AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (UserQuestion2Answer == 'select') {
	   
				$('#UserQuestion2Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion2Answer == 'Yes' && UserQuestion2AnsExtenstion=='') {
	   
			   $('#UserQuestion2AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (UserQuestion3Answer == 'select') {
	   
				$('#UserQuestion3Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion3Answer == 'Yes' && UserQuestion3AnsExtenstion=='') {
	   
			   $('#UserQuestion3AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (UserQuestion4Answer == 'select') {
	   
				$('#UserQuestion4Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion4Answer == 'Yes' && UserQuestion4AnsExtenstion=='') {
	   
			   $('#UserQuestion4AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (UserQuestion5Answer == 'select') {
	   
				$('#UserQuestion5Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion5Answer == 'Yes' && UserQuestion5AnsExtenstion=='') {
	   
			   $('#UserQuestion5AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (UserQuestion6Answer == 'select') {
	   
				$('#UserQuestion6Answer').closest(".form-group").addClass("has-error");
				return false;
			   
			}
			if (UserQuestion6Answer == 'Yes' && UserQuestion6AnsExtenstion=='') {
	   
			   $('#UserQuestion6AnsExtenstion').addClass('has-error');
			   return false;
			   
			}
			if (!GivePermissionForAnyEnq || !ConfirmMyInfoCorrect) {

				
					$('#ConfirmMyInfoCorrect').after('<span class="input-error2">Please select both the check boxes before clicking the Finish button.</span>');
					isValid = false;
					return false;
			}
	  
	});

	iWizard.init(); 
	/*
	 * set the condition for select main bar
	 */
	iWizard.goto(selectedMainBar);   

	
		var navListItems = $('div.setup-panel div a'),
				allWells = $('.setup-content'),
				allNextBtn = $('.nextBtn'),
				allPrevBtn = $('.prevBtn'); 
			
		/*navListItems.click(function (e) {
			e.preventDefault();
			var $target = $($(this).attr('href')),
				$item = $(this);

			if (!$item.hasClass('disabled')) {
				navListItems.removeClass('btn-warning').addClass('btn-default pinactive');
				$item.addClass('btn-warning pactive');
				allWells.hide();
				$target.show();
				$target.find('input:eq(0)').focus();
			}
		});*/

		allNextBtn.on('iWizard.click',function () { 
			var curStep = $(this).closest(".setup-content"),
					curStepBtn = curStep.attr("id"),
					nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
				   // curInputs = curStep.find("input[type='text'],select,input[type='url']"),
					curInputs = curStep.find("input[type='text'],select,input[type='url'],input[type='email'],input[type='number'],input[type='password'],textarea"),
					
					isValid = true;
				
			$(".form-group").removeClass("has-error");
			/*           
			
			for (var i = 0; i < curInputs.length; i++) {
				if (!curInputs[i].validity.valid) {
					isValid = false;
					$(curInputs[i]).closest(".form-group").addClass("has-error");
				}
			}
			*/
			
		   
			/*
			* This pattern will check only one space 
			*/
			var stringPatternOnlyOneSpace = /\s{1,}/g;
			/*
			* This pattern will check more than one space more than one place
			*/
			var stringPatternMultipleSpace = /\s{2,}/g;
			/*
			* for valid password
			*/
			var minNumberofPass         = 6;
			var maxNumberofPass         = 16;
			var regularExpressionPass   = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
			
			/*
			* for valid phone no.
			*/
			var minNumberofPhone        = 6;
			var maxNumberofPhone        = 16;
			//var regularExpressionPhone  = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
			var regularExpressionPhone  = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
			/*
			* for valid email
			*/
			var regularExpressionEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			 //var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			/*
			* Zipcode
			*/
			var minNumberofZipcode          = 3;
			var maxNumberofZipcode          = 10;
			//var regularExpressionZipcode    = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR ?0AA)$/;
			var regularExpressionZipcode    = /^([a-pr-uwyzA-PR-UWYZ0-9][a-hk-yA-HK-Y0-9][aehmnprtvxyAEHMNPRTVXY0-9]?[abehmnprvwxyABEHMNPRVWXY0-9]? {0,2}[0-9][abd-hjln-uw-zABD-HJLN-UW-Z]{2}|GIR ?0AA)$/;
			/* 
			*  url or link validation http and www
			*/
			var stringPatternLinkUrl = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;

			/*
			* insureance patteren
			*/
			//var insureanceNumberPattern = /^[A-CEGHJ-PR-TV-Y]{1}[A-CEGHJ-NPR-TV-Y]{1}[0-9]{6}[A-Z]{1}/;
			//#######var insureanceNumberPattern = /^((A[ABEHKLMPRSTWXYZ])|(B[ABEHKLMT])|(C[ABEHKLR])|(E[ABEHKLMPRSTWXYZ])|(GY)|(H[ABEHKLMPRSTWXYZ])|(J[ABCEGHJKLMNPRSTWXYZ])|(K[ABEHKLMPRSTWXYZ])|(L[ABEHKLMPRSTWXYZ])|(M[AWX])|(N[ABEHLMPRSWXYZ])|(O[ABEHKLMPRSX])|(P[ABCEGHJKLMNPRSTWXY])|(R[ABEHKMPRSTWXYZ])|(S[ABCEGHJKLMNPRSTWXYZ])|(T[ABEHKLMPRSTWXYZ])|(W[ABEKLMP])|(Y[ABEHKLMPRSTWXYZ])|(Z[ABEHKLMPRSTWXY]))\d{6}([A-D]|\s)$/;

			/*
			* fix lenght = 9
			*/
			var fixLength = 9;
			/* validation start here 
			*  frist name validation
			*/
			
			$('input').click(function(){

				$(this).removeClass('input-error');				
				$(this).closest(".form-group").removeClass("has-error");
				$(this).next('span').remove();

			});
			$('select').change(function(){

                $(this).removeClass('input-error');             
                $(this).closest(".form-group").removeClass("has-error");
                $(this).next('span').remove();

            });

			 var curStepBtn = $(this).data("step-id");

			if (curStepBtn =='#step-form-1') {

				var UserProfileHonorific            = $('#UserProfileHonorific').val();
				var UserProfileFName            	= $('#UserProfileFName').val();
				var UserProfileLName            	= $('#UserProfileLName').val();
				var UserEmail                   	= $('#UserEmail').val();
				var UserCemail                   	= $('#UserCemail').val();
				var UserProfilePhoneNo          	= $('#UserProfilePhoneNo').val();
				var UserProfileDob              	= $('#UserProfileDob').val();
				var UserProfileAddress          	= $('#UserProfileAddress').val();
				var UserProfileAddress2         	= $('#UserProfileAddress2').val();
				var UserProfileTown             	= $('#UserProfileTown').val();
				var UserProfileBoroughId        	= $('#UserProfileBoroughId').val();
				var UserProfilePostcode         	= $('#UserProfilePostcode').val();
				var UserPassword                	= $('#UserPassword').val();
				var UserCpassword               	= $('#UserCpassword').val();            
				var UserProfileProficiencyEnglish 	= $('#UserProfileProficiencyEnglish').val();
				var sel_lang                    	= $('#sel_lang').val();
				var UserApplicationJobCategoryId    = $('#UserApplicationJobCategoryId').val();
				var UserApplicationPersonalCare		= $('#UserApplicationPersonalCare').val();
				var UserApplicationTotalExperience  = $('#UserApplicationTotalExperience').val();
				var UserProfileUpdatedDbs       	= $('#UserProfileUpdatedDbs').val();
				var UserProfileCarLicence       	= $('#UserProfileCarLicence').val();
				var UserProfileInsuranceNumber  	= $('#UserProfileInsuranceNumber').val();
				/*
				* check first name
				*/
				
			//	$(".info_block_row").not('.pblock').detach();
				
				if ($('#UserProfileHonorific').length>0 && UserProfileHonorific == '') {
					   
					$('#UserProfileHonorific').closest(".form-group").addClass("has-error");
					isValid = false;
				   
				}
				if ($('#UserProfileFName').length>0 && UserProfileFName == '') {
					   
					$('#UserProfileFName').closest(".form-group").addClass("has-error");
					isValid = false;
				   
				}
				if ($('#UserProfileFName').length>0 && UserProfileFName!='' && UserProfileFName.length == 1 && UserProfileFName.match(stringPatternOnlyOneSpace)) {

					$('#UserProfileFName').closest(".form-group").addClass("has-error");
					$('#UserProfileFName').next('span').remove();
					$('#UserProfileFName').after('<span class="input-error">Please enter first name or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfileFName').length>0 && UserProfileFName!='' && UserProfileFName.length > 1 && UserProfileFName.match(stringPatternMultipleSpace)) {

					$('#UserProfileFName').closest(".form-group").addClass("has-error");
					$('#UserProfileFName').next('span').remove();
					$('#UserProfileFName').after('<span class="input-error">Only one space is allow in first name.</span>');
					isValid = false;
					return false;
				}
				/*
				* check last name
				*/
				if ($('#UserProfileLName').length>0 && UserProfileLName == '') {
					   
					$('#UserProfileLName').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				if ($('#UserProfileLName').length>0 && UserProfileLName!='' && UserProfileLName.length == 1 && UserProfileLName.match(stringPatternOnlyOneSpace)) {

					$('#UserProfileLName').closest(".form-group").addClass("has-error");
					$('#UserProfileLName').next('span').remove();
					$('#UserProfileLName').after('<span class="input-error">Please enter last name or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfileLName').length>0 && UserProfileLName!='' && UserProfileLName.length > 1 && UserProfileLName.match(stringPatternMultipleSpace)) {
					
					$('#UserProfileLName').closest(".form-group").addClass("has-error");
					$('#UserProfileLName').next('span').remove();
					$('#UserProfileLName').after('<span class="input-error">Only one space is allow in last name.</span>');
					isValid = false;
					return false;
				}
				/*
				*  email
				*/
				if ($('#UserEmail').length>0 && UserEmail == '') {
					   
					$('#UserEmail').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				if ($('#UserEmail').length>0 && UserEmail!='' && UserEmail.length == 1 && UserEmail.match(stringPatternOnlyOneSpace)) {

					$('#UserEmail').closest(".form-group").addClass("has-error");
					$('#UserEmail').next('span').remove();
					$('#UserEmail').after('<span class="input-error">Please enter email or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserEmail').length>0 && UserEmail!='' && UserEmail.length > 1 && UserEmail.match(stringPatternMultipleSpace)) {
					
					$('#UserEmail').closest(".form-group").addClass("has-error");
					$('#UserEmail').next('span').remove();
					$('#UserEmail').after('<span class="input-error">Only one space is allow in email.</span>');
					isValid = false;
					return false;
				}
				if($('#UserEmail').length>0 && UserEmail!='' && !regularExpressionEmail.test(UserEmail)) {
					
					$('#UserEmail').closest(".form-group").addClass("has-error");
					$('#UserEmail').next('span').remove();
					$('#UserEmail').after('<span class="input-error">Enter valid email.</span>');
					isValid = false;
					return false;
				} 
				if($('#UserEmail').length>0 && UserEmail!='' ) {
				   
						
						var url = $('#UserEmail').data('url');
						var id 	= $('#UserEmail').data('id');


					$.ajax({
							type: "POST",
							url: url,                        
							dataType: "json",
							data: {email:UserEmail,id:id},
							success: function (response) {

								if (response=="1") {
									
									$('#UserEmail').closest(".form-group").addClass("has-error");
									$('#UserEmail').next('span').remove();
									$('#UserEmail').after('<span class="input-error">This email is already in use.</span>');
									isValid = false;
									return false;
								}else{
									//isValid = true;
								}
							},
							error: function () {
								alert('Application not submitted. Please try later');
							}
						});
				} 
				/*
				*  email
				*/
				if ($('#UserCemail').length>0 && UserCemail == '') {
					   
					$('#UserCemail').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				if ($('#UserCemail').length>0 && UserCemail!='' && UserCemail.length == 1 && UserCemail.match(stringPatternOnlyOneSpace)) {

					$('#UserCemail').closest(".form-group").addClass("has-error");
					$('#UserCemail').next('span').remove();
					$('#UserCemail').after('<span class="input-error">Please enter confirm email or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserCemail').length>0 && UserCemail!='' && UserCemail.length > 1 && UserCemail.match(stringPatternMultipleSpace)) {
					
					$('#UserCemail').closest(".form-group").addClass("has-error");
					$('#UserCemail').next('span').remove();
					$('#UserCemail').after('<span class="input-error">Only one space is allow in confirm email.</span>');
					isValid = false;
					return false;
				}
				if($('#UserCemail').length>0 && UserCemail!='' && !regularExpressionEmail.test(UserCemail)) {
					
					$('#UserCemail').closest(".form-group").addClass("has-error");
					$('#UserCemail').next('span').remove();
					$('#UserCemail').after('<span class="input-error">Enter valid confirm email.</span>');
					isValid = false;
					return false;
				} 
				if(UserEmail!=UserCemail){
					$('#UserCemail').closest(".form-group").addClass("has-error");
					$('#UserCemail').next('span').remove();
					$('#UserCemail').after('<span class="input-error">Email and confirm email doesn\'t match please re enter.</span>');
					isValid = false;
					return false;
				}

				/*
				* phone number validation
				*/
				if ($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo == '') {
					   
					$('#UserProfilePhoneNo').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				if ($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo!='' && UserProfilePhoneNo.length == 1 && UserProfilePhoneNo.match(stringPatternOnlyOneSpace)) {

					$('#UserProfilePhoneNo').closest(".form-group").addClass("has-error");
					$('#UserProfilePhoneNo').next('span').remove();
					$('#UserProfilePhoneNo').after('<span class="input-error">Please enter phone number in numeric format only.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo!='' && UserProfilePhoneNo.length > 1 && UserProfilePhoneNo.match(stringPatternMultipleSpace)) {
					
					$('#UserProfilePhoneNo').closest(".form-group").addClass("has-error");
					$('#UserProfilePhoneNo').next('span').remove();
					$('#UserProfilePhoneNo').after('<span class="input-error">Please enter phone number in numeric format only.</span>');
					isValid = false;
					return false;
				}
				if(($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo!='' && UserProfilePhoneNo.length < minNumberofPhone) || ($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo!='' && UserProfilePhoneNo.length > maxNumberofPhone)){

					$('#UserProfilePhoneNo').closest(".form-group").addClass("has-error");
					$('#UserProfilePhoneNo').next('span').remove();
					$('#UserProfilePhoneNo').after('<span class="input-error">Phone length should be 6 to 16.</span>');
					isValid = false;
					return false;
				}
				if($('#UserProfilePhoneNo').length>0 && UserProfilePhoneNo!='' && !regularExpressionPhone.test(UserProfilePhoneNo)) {
					
					$('#UserProfilePhoneNo').closest(".form-group").addClass("has-error");
					$('#UserProfilePhoneNo').next('span').remove();
					$('#UserProfilePhoneNo').after('<span class="input-error">Please enter phone number in numeric format only.</span>');
					isValid = false;
					return false;
				}
				/*
				* address
				*/
				if ($('#UserProfileDob').length>0 && UserProfileDob == '') {
					   
					$('#UserProfileDob').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				/*
				* address
				*/
				if ($('#UserProfileAddress').length>0 && UserProfileAddress == '') {
					   
				   $('#UserProfileAddress').closest(".form-group").addClass("has-error");
				}
				if ($('#UserProfileAddress').length>0 && UserProfileAddress!='' && UserProfileAddress.length == 1 && UserProfileAddress.match(stringPatternOnlyOneSpace)) {

					$('#UserProfileAddress').closest(".form-group").addClass("has-error");
					$('#UserProfileAddress').next('span').remove();
					$('#UserProfileAddress').after('<span class="input-error">Please enter address or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfileAddress').length>0 && UserProfileAddress!='' && UserProfileAddress.length > 1 && UserProfileAddress.match(stringPatternMultipleSpace)) {
					
					$('#UserProfileAddress').closest(".form-group").addClass("has-error");
					$('#UserProfileAddress').next('span').remove();
					$('#UserProfileAddress').after('<span class="input-error">Only one space is allow in address.</span>');
					isValid = false;
					return false;
				}
				/*
				* addre3ss two
				*/
				if ($('#UserProfileAddress2').length>0 && UserProfileAddress2 == '') {
					   
				  $('#UserProfileAddress2').closest(".form-group").addClass("has-error");
				  isValid = false;
				}
				if ($('#UserProfileAddress2').length>0 && UserProfileAddress2!='' && UserProfileAddress2.length == 1 && UserProfileAddress2.match(stringPatternOnlyOneSpace)) {

					$('#UserProfileAddress2').closest(".form-group").addClass("has-error");
					$('#UserProfileAddress2').next('span').remove();
					$('#UserProfileAddress2').after('<span class="input-error">Please enter second line address or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfileAddress2').length>0 && UserProfileAddress2!='' && UserProfileAddress2.length > 1 && UserProfileAddress2.match(stringPatternMultipleSpace)) {
					
					$('#UserProfileAddress2').closest(".form-group").addClass("has-error");
					$('#UserProfileAddress2').next('span').remove();
					$('#UserProfileAddress2').after('<span class="input-error">Only one space is allow in second line address.</span>');
					isValid = false;
					return false;
				}
				/*
				* town
				*/

				if ($('#UserProfileTown').length>0 && UserProfileTown == '') {
					   
				   $('#UserProfileTown').closest(".form-group").addClass("has-error");
				   isValid = false;
				}
				if ($('#UserProfileTown').length>0 && UserProfileTown!='' && UserProfileTown.length == 1 && UserProfileTown.match(stringPatternOnlyOneSpace)) {

					$('#UserProfileTown').closest(".form-group").addClass("has-error");
					$('#UserProfileTown').next('span').remove();
					$('#UserProfileTown').after('<span class="input-error">Please enter town or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfileTown').length>0 && UserProfileTown!='' && UserProfileTown.length > 1 && UserProfileTown.match(stringPatternMultipleSpace)) {
					
					$('#UserProfileTown').closest(".form-group").addClass("has-error");
					$('#UserProfileTown').next('span').remove();
					$('#UserProfileTown').after('<span class="input-error">Only one space is allow in town.</span>');
					isValid = false;
					return false;
				}
				/*
				* brought
				*/
				if ($('#UserProfileBoroughId').length>0 && UserProfileBoroughId == '') {
					   
					$('#UserProfileBoroughId').closest(".form-group").addClass("has-error");
					isValid = false;
				}

				/*
				* zipcode validation
				*/
				if ($('#UserProfilePostcode').length>0 && UserProfilePostcode == '') {
					   
					$('#UserProfilePostcode').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				if ($('#UserProfilePostcode').length>0 && UserProfilePostcode!='' && UserProfilePostcode.length == 1 && UserProfilePostcode.match(stringPatternOnlyOneSpace)) {

					$('#UserProfilePostcode').closest(".form-group").addClass("has-error");
					$('#UserProfilePostcode').next('span').remove();
					$('#UserProfilePostcode').after('<span class="input-error">Please enter postcode or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserProfilePostcode').length>0 && UserProfilePostcode!='' && UserProfilePostcode.length > 1 && UserProfilePostcode.match(stringPatternMultipleSpace)) {
					
					$('#UserProfilePostcode').closest(".form-group").addClass("has-error");
					$('#UserProfilePostcode').next('span').remove();
					$('#UserProfilePostcode').after('<span class="input-error">Only one space is allow in postcode.</span>');
					isValid = false;
					return false;
				}
				if(($('#UserProfilePostcode').length>0 && UserProfilePostcode!='' && UserProfilePostcode.length < minNumberofZipcode) || ($('#UserProfilePostcode').length>0 && UserProfilePostcode!='' && UserProfilePostcode.length > maxNumberofZipcode)){

					$('#UserProfilePostcode').closest(".form-group").addClass("has-error");
					$('#UserProfilePostcode').next('span').remove();
					$('#UserProfilePostcode').after('<span class="input-error">Postcode length should be 3 to 10.</span>');
					isValid = false;
					return false;
				}
				if($('#UserProfilePostcode').length>0 && UserProfilePostcode!='' && !regularExpressionZipcode.test(UserProfilePostcode)) {
					
					$('#UserProfilePostcode').closest(".form-group").addClass("has-error");
					$('#UserProfilePostcode').next('span').remove();
					$('#UserProfilePostcode').after('<span class="input-error">Please enter postcode in valid format.</span>');
					isValid = false;
					return false;
				}
				/*
				* password validation
				*/
								if ($('#UserPassword').length>0 && UserPassword == '') {
					   
					$('#UserPassword').closest(".form-group").addClass("has-error");
					isValid = false;
				}
								
				if ($('#UserPassword').length>0 && UserPassword!='' && UserPassword.length == 1 && UserPassword.match(stringPatternOnlyOneSpace)) {

					
					$('#UserPassword').closest(".form-group").addClass("has-error");
					$('#UserPassword').next('span').remove();
					$('#UserPassword').after('<span class="input-error">Please enter password or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserPassword').length>0 && UserPassword!='' && UserPassword.length > 1 && UserPassword.match(stringPatternMultipleSpace)) {
					
					$('#UserPassword').closest(".form-group").addClass("has-error");
					$('#UserPassword').next('span').remove();
					$('#UserPassword').after('<span class="input-error">Only one space is allow in password.</span>');
					isValid = false;
					return false;
				}
				if(($('#UserPassword').length>0 && UserPassword!='' && UserPassword.length < minNumberofPass) || ($('#UserPassword').length>0 && UserPassword!='' && UserPassword.length > maxNumberofPass)){
					
					$('#UserPassword').closest(".form-group").addClass("has-error");
					$('#UserPassword').next('span').remove();
					$('#UserPassword').after('<span class="input-error">Password length should be 6 to 16.</span>');
					isValid = false;
					return false;
				}
				/*if(UserPassword!='' && !regularExpressionPass.test(UserPassword)) {
					alert("password should contain atleast one number and one special character.");
					return false;
				}
				/*
				* confirm password
				*/
								if ($('#UserCpassword').length>0 && UserCpassword == '') {
					   
					$('#UserCpassword').closest(".form-group").addClass("has-error");
					isValid = false;
				}
								
				if ($('#UserCpassword').length>0 && UserCpassword!='' && UserCpassword.length == 1 && UserCpassword.match(stringPatternOnlyOneSpace)) {

					$('#UserCpassword').closest(".form-group").addClass("has-error");
					$('#UserCpassword').next('span').remove();
					$('#UserCpassword').after('<span class="input-error">Please enter confirm password or remove this space.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserCpassword').length>0 && UserCpassword!='' && UserCpassword.length > 1 && UserCpassword.match(stringPatternMultipleSpace)) {
					
					$('#UserCpassword').closest(".form-group").addClass("has-error");
					$('#UserCpassword').next('span').remove();
					$('#UserCpassword').after('<span class="input-error">Only one space is allow confirm password.</span>');
					isValid = false;
					return false;
				}
				if ($('#UserCpassword').length>0 && UserCpassword!='' && UserPassword!='' && UserCpassword!=UserPassword) {
					
					$('#UserCpassword').closest(".form-group").addClass("has-error");
					$('#UserCpassword').next('span').remove();
					$('#UserCpassword').after('<span class="input-error">Confirm password do not match please re enter.</span>');
					isValid = false;
					return false;
				}
				/*
				* language
				*/
				if ($('#UserProfileProficiencyEnglish').length>0 && UserProfileProficiencyEnglish == '') {
					   
					$('#UserProfileProficiencyEnglish').closest(".form-group").addClass("has-error");
				   isValid = false;
				}
				/*
				* language
				*/
				if ($('#sel_lang').length>0 && sel_lang == '') {
					   
				   $('#sel_lang').closest(".form-group").addClass("has-error");
				   isValid = false;
				}
				/*
				* language other
				*/
				
				if($(".otherLanguage").not('.hide .otherLanguage').length > 0) {
					$(".otherLanguage").not('.hide .otherLanguage').each(function(){
						if($(this).val() == "") {
							$(this).closest(".form-group").addClass("has-error");
							isValid = false;
						}
					});
				}
				
				/*
				* job category
				*/
				if (UserApplicationJobCategoryId == '') {
					   
					$('#UserApplicationJobCategoryId').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				/*
				* job category
				*/
				if (UserApplicationJobCategoryId =! '' && UserApplicationJobCategoryId == '1' && UserApplicationPersonalCare == '') {
					   
					$('#UserApplicationPersonalCare').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				/*
				* UserProfileTotalExperience
				*/
				if (UserApplicationTotalExperience == '') {
					   
					$('#UserApplicationTotalExperience').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				/*
				*
				*/
				if ($('#UserProfileUpdatedDbs').length>0 && UserProfileUpdatedDbs == '') {
					   
					$('#UserProfileUpdatedDbs').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				/*
				*
				*/
				if ($('#UserProfileCarLicence').length>0 && UserProfileCarLicence == '') {
					   
				   $('#UserProfileCarLicence').closest(".form-group").addClass("has-error");
				   isValid = false;
				}
				/*
				* 
				*/
				if ($('#UserProfileInsuranceNumber').length>0 && UserProfileInsuranceNumber == '') {
					   
					$('#UserProfileInsuranceNumber').closest(".form-group").addClass("has-error");
					isValid = false;
				}
				
				/*if ($('#UserProfileInsuranceNumber').length>0 && UserProfileInsuranceNumber!='' && !insureanceNumberPattern.test(UserProfileInsuranceNumber) ) {

					$('#UserProfileInsuranceNumber').closest(".form-group").addClass("has-error");
					$('#UserProfileInsuranceNumber').next('span').remove();
					$('#UserProfileInsuranceNumber').after('<span class="input-error">Insurance number is not valid follow proper pattern.</span>');
					isValid = false;
					return false;
				}*/

				/*if ($('#UserProfileInsuranceNumber').length>0 && UserProfileInsuranceNumber != '' && UserProfileInsuranceNumber.length != fixLength) {					   
				  
					$('#UserProfileInsuranceNumber').closest(".form-group").addClass("has-error");
					$('#UserProfileInsuranceNumber').next('span').remove();
					$('#UserProfileInsuranceNumber').after('<span class="input-error">Insurance number must be of 9 characters.</span>');
					isValid = false;
					return false;
				}*/
				if($('#UserProfileInsuranceNumber').length>0 && UserProfileDob!='' && UserProfileInsuranceNumber!='') { 
					   
					var id  = $('#UserEmail').data('id');

					$.ajax({
							type: "POST",
							url: "/users/checkDuplicateInsuranceNumber",                      
							dataType: "json",
							data: {UserProfileInsuranceNumber:UserProfileInsuranceNumber,UserProfileDob:UserProfileDob,id:id},
							success: function (response) {

								if (response!='' && response!='0') {
									
									$('#UserProfileInsuranceNumber').closest(".form-group").addClass("has-error");
									$('#UserProfileInsuranceNumber').next('span').remove();
									$('#UserProfileInsuranceNumber').after('<span class="input-error">This National Insurance Number and Date of Birth details are already in our records. Please login with your registered Email Id. In case you have forgotten the password, please click on forgot password link.</span>');
									isValid = false;
									return false;
								}else{
									//isValid = true;
								}
							},
							error: function () {
								alert('Application not submitted. Please try later');
							}
						});
				}  
			 }               
			/*
			************************validation end for Personal Information**************
			*/

			//console.log($(this).data('step-id'));

			setTimeout(function(){
					if (isValid) {
					
					if(curStepBtn=="#step-form-1"){
						
						$.ajax({
							type: "POST",
							url: "/",
							data: $('#app_form_steps').serialize(),
							dataType: "json",
							beforeSend: function () {
								$(".ajax-spinner").show();
								$(".overlay").show();
							},
							success: function (response) {
								
								$(".ajax-spinner").hide();
								$(".overlay").hide();
								
								if(response.status == '999' && response.id =='0')
								{
										$( "#dialog-message" ).dialog({
											modal: true,
											buttons: {
												Ok: function() {
													$( this ).dialog( "close" );
													iWizard.goto(0);  
												}
											}
										});
								}
								else
								{
									$(".getUsr").attr('value', response.id);
																		$('#login1').hide();
									$('#login2').show();
									$('#applyBtn1').hide();
									$('#nextBtn1').show();
									$('#applyBtn2').hide();
									$('#nextBtn2').show();
									iWizard.goto(1);  
									$("html, body").animate({ scrollTop: 0 }, "slow");							
								}
							},
							error: function () {
								alert('Application not submitted. Please try later');
							}
						});
					}
					else {
					   // nextStepWizard.removeAttr('disabled').trigger('click');
					}
					
				  
					if(curStepBtn=="#step-form-2"){

					   iWizard.goto(2);       
					} 
					
				}
			},2500);
			
		});

		allPrevBtn.on("iWizard.click",function () {
			var step = $(this).data("step");
			iWizard.goto(step-1);       
		});

		//$('div.setup-panel div a.btn-warning').trigger('click');
		//When checkboxes/radios checked/unchecked, toggle background color
		$('.form-group').on('click', 'input[type=radio]', function () {
			$(this).closest('.form-group').find('.radio-inline, .radio').removeClass('checked');
			$(this).closest('.radio-inline, .radio').addClass('checked');
		});

		//DOB calendar

		if (!$.fn.bootstrapDP && $.fn.datepicker && $.fn.datepicker.noConflict) {
		   var datepicker = $.fn.datepicker.noConflict();
		   $.fn.bootstrapDP = datepicker;
		}
		
		$("#UserProfileDob").datepicker({
			maxDate: new Date(),
			yearRange: '-100:-10', 
			dateFormat:"dd/mm/yy",
			changeMonth: true,
			changeYear: true,
			todayHighlight: true,
		});

	   
		$("#UserQuestion1Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a1").show();
			} else {
				$("#a1").hide();
			}
		});

		$("#UserQuestion2Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a2").show();
			} else {
				$("#a2").hide();
			}
		});

		$("#UserQuestion3Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a3").show();
			} else {
				$("#a3").hide();
			}
		});

		$("#UserQuestion4Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a4").show();
			} else {
				$("#a4").hide();
			}
		});

		$("#UserQuestion5Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a5").show();
			} else {
				$("#a5").hide();
			}
		});

		$("#UserQuestion6Answer").on('change', function () {
			if ($(this).children('option:selected').index() == 1) {
				$("#a6").show();
			} else {
				$("#a6").hide();
			}
		});
		
		
		


	});

   

	// $(".select2").select2();



	$('select#JobEmployerId').on('change', function () {
		var clientID = $(this).val();

		// send ajax
		$(".ajax-spinner").show();
		$.ajax({
			url: '/admin/jobs/getClientDetailsById',
			method: 'post',
			data: "id=" + clientID,
			success: function (response) {
				$("#job_client_box").html(response);
				$(".ajax-spinner").hide();
			}
		});
	});

	$('#add_new_client').on('click', function () {
		$('select#JobEmployerId').val('').trigger('change');
	});

</script>