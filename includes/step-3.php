<?php
	
	include_once("db.php");
	
	function crsrad($val){
		if($val == 0)
			return "";
		else
			return "active";
	}
	$ref2_yes = $ref1_yes = "";
	$institution = $qual_sub = $qual_obt = $qual_yr = $ref1_name = $ref1_company = $ref1_job = $ref1_email = $ref1_address1 = $ref1_address2 = $ref1_town = $ref1_phone = $ref1_postcode = $ref2_name = $ref2_company = $ref2_job = $ref2_email = $ref2_address1 = $ref2_address2 = $ref2_town = $ref2_phone = $ref2_postcode = $ref2_no = $ref1_no = "";
	$institution2 = $qual_sub2 = $qual_obt2 = $qual_yr2 = "";
	$institution3 = $qual_sub3 = $qual_obt3 = $qual_yr3 = "";
	$institution4 = $qual_sub4 = $qual_obt4 = $qual_yr4 = "";
	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM a_step3 WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){	
				$institution 	= $rows['institution'];
				$qual_sub 		= $rows['qual_sub'];
				$qual_obt	 	= $rows['qual_obt'];
				$qual_yr		= $rows['qual_yr'];
				$institution2 	= $rows['institution2'];
				$qual_sub2		= $rows['qual_sub2'];
				$qual_obt2	 	= $rows['qual_obt2'];
				$qual_yr2		= $rows['qual_yr2'];
				$institution3 	= $rows['institution3'];
				$qual_sub3 		= $rows['qual_sub3'];
				$qual_obt3	 	= $rows['qual_obt3'];
				$qual_yr3		= $rows['qual_yr3'];
				$institution4 	= $rows['institution4'];
				$qual_sub4 		= $rows['qual_sub4'];
				$qual_obt4	 	= $rows['qual_obt4'];
				$qual_yr4		= $rows['qual_yr4'];
				$ref1_name		= $rows['ref1_name'];
				$ref1_company	= $rows['ref1_company'];
				$ref1_job		= $rows['ref1_job'];
				$ref1_email		= $rows['ref1_email'];
				$ref1_address1	= $rows['ref1_address1'];
				$ref1_address2	= $rows['ref1_address2'];
				$ref1_town		= $rows['ref1_town'];
				$ref1_phone		= $rows['ref1_phone'];
				$ref1_postcode	= $rows['ref1_postcode'];
				$ref1_yes		= crsrad($rows['ref1_yes']);
				$ref1_no		= crsrad($rows['ref1_no']);
				$ref2_name		= $rows['ref2_name'];
				$ref2_company	= $rows['ref2_company'];
				$ref2_job		= $rows['ref2_job'];
				$ref2_email		= $rows['ref2_email'];
				$ref2_address1	= $rows['ref2_address1'];
				$ref2_address2	= $rows['ref2_address2'];
				$ref2_town		= $rows['ref2_town'];
				$ref2_phone		= $rows['ref2_phone'];
				$ref2_postcode	= $rows['ref2_postcode'];
				$ref2_yes		= crsrad($rows['ref2_yes']);
				$ref2_no		= crsrad($rows['ref2_no']);
			}
		}//echo "<script>alert('$ref2_no')</script>";
	}
?>
						<div id="step-3">

							<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Education</h3>
        <h5 class="text-muted">Complete your education history.</h5>
    </div><!-- /.box-header -->
    <div class="box-body">

        <div class="form-group col-md-12" style="margin-top: 10px;">
            <h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Qualifications, Trainings and Certificates</span></strong></h5>
            <hr style="margin: 2px;">
        </div>

        
        <div class="col-md-12">
            <div id="block_edu">
				<div class="row">
					<div class="col-sm-3 form-group" id="qualifications">
							<label>Institution</label>                        
							<div class="input text">
								<input placeholder="Institution" class="form-control institution_title" value="<?=$institution?>" required="required" type="text" id="institution"/>
							</div>
							<!-- <input type="hidden" class="getUsr" value="" id="UserEducationUserId"/> -->
						</div>
					<div class="col-sm-3 form-group">
                        <label>Subjects or Title</label>                        
                        <div class="input text">
							<input placeholder="Subjects or Title" class="form-control education_title" value="<?=$qual_sub?>" required="required" type="text" id="qual_sub"/>
						</div>
						<input type="hidden" class="getUsr" value="" id="UserEducationUserId"/>
					</div>
                    <div class="col-sm-2 form-group">
                        <label>Qualification Obtained</label>
                        <div class="input text"><input placeholder="Qualification" class="form-control qualification_obt" value="<?=$qual_obt?>" required="required" maxlength="255" type="text" id="qual_obt"/></div>                                                            
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Year Awarded</label>
                         <div class="input select">
							<select placeholder="Year - YYYY" class="form-control year_awarded" required="required" id="qual_yr">
							<option value="">Select</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
						</select>
					</div>          						                                                  
				</div>
					<!-- <div class="col-sm-2 form-group">
						<label for="">Add Institution</label>
					<h1>+</h1>
					</div> -->
            </div>
				<!--div class="row" id="afterThis">
                    <div class="col-sm-6 pull-right">                                  
                        <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Qualification</a>
                    </div>                                                                    
                </div-->
            </div>               
		</div>
		
		<!-- Education 2 -->
		<div class="col-md-12">
            <div id="block_edu">
				<div class="row">
					<div class="col-sm-3 form-group" id="qualifications">
							<label>Institution</label>                        
							<div class="input text">
								<input placeholder="Institution" class="form-control institution_title" value="<?=$institution2?>" type="text" id="institution2"/>
							</div>
							<!-- <input type="hidden" class="getUsr" value="" id="UserEducationUserId"/> -->
						</div>
					<div class="col-sm-3 form-group">
                        <label>Subjects or Title</label>                        
                        <div class="input text">
							<input placeholder="Subjects or Title" class="form-control education_title" value="<?=$qual_sub2?>" type="text" id="qual_sub2"/>
						</div>
						<input type="hidden" class="getUsr" value="" id="UserEducationUserId"/>
					</div>
                    <div class="col-sm-2 form-group">
                        <label>Qualification Obtained</label>
                        <div class="input text"><input placeholder="Qualification" class="form-control qualification_obt" value="<?=$qual_obt2?>" maxlength="255" type="text" id="qual_obt2"/></div>                                                            
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Year Awarded</label>
                         <div class="input select">
							<select placeholder="Year - YYYY" class="form-control year_awarded" id="qual_yr2">
							<option value="">Select</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
						</select>
					</div>          						                                                  
				</div>
					<!-- <div class="col-sm-2 form-group">
						<label for="">Add Institution</label>
					<h1>+</h1>
					</div> -->
            </div>
				<!--div class="row" id="afterThis">
                    <div class="col-sm-6 pull-right">                                  
                        <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Qualification</a>
                    </div>                                                                    
                </div-->
            </div>               
        </div>

		<!-- Education 3 -->
		<div class="col-md-12">
            <div id="block_edu">
				<div class="row">
					<div class="col-sm-3 form-group" id="qualifications">
							<label>Institution</label>                        
							<div class="input text">
								<input placeholder="Institution" class="form-control institution_title" value="<?=$institution3?>" type="text" id="institution3"/>
							</div>
							<!-- <input type="hidden" class="getUsr" value="" id="UserEducationUserId"/> -->
						</div>
					<div class="col-sm-3 form-group">
                        <label>Subjects or Title</label>                        
                        <div class="input text">
							<input placeholder="Subjects or Title" class="form-control education_title" value="<?=$qual_sub3?>" type="text" id="qual_sub3"/>
						</div>
						<input type="hidden" class="getUsr" value="" id="UserEducationUserId"/>
					</div>
                    <div class="col-sm-2 form-group">
                        <label>Qualification Obtained</label>
                        <div class="input text"><input placeholder="Qualification" class="form-control qualification_obt" value="<?=$qual_obt3?>" maxlength="255" type="text" id="qual_obt3"/></div>                                                            
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Year Awarded</label>
                         <div class="input select">
							<select placeholder="Year - YYYY" class="form-control year_awarded" id="qual_yr3">
							<option value="">Select</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
						</select>
					</div>          						                                                  
				</div>
					<!-- <div class="col-sm-2 form-group">
						<label for="">Add Institution</label>
					<h1>+</h1>
					</div> -->
            </div>
				<!--div class="row" id="afterThis">
                    <div class="col-sm-6 pull-right">                                  
                        <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Qualification</a>
                    </div>                                                                    
                </div-->
            </div>               
		</div>
		
		<!-- Education 4 -->
		<div class="col-md-12">
            <div id="block_edu">
				<div class="row">
					<div class="col-sm-3 form-group" id="qualifications">
							<label>Institution</label>                        
							<div class="input text">
								<input placeholder="Institution" class="form-control institution_title" value="<?=$institution4?>" type="text" id="institution4"/>
							</div>
							<!-- <input type="hidden" class="getUsr" value="" id="UserEducationUserId"/> -->
						</div>
					<div class="col-sm-3 form-group">
                        <label>Subjects or Title</label>                        
                        <div class="input text">
							<input placeholder="Subjects or Title" class="form-control education_title" value="<?=$qual_sub4?>" type="text" id="qual_sub4"/>
						</div>
						<input type="hidden" class="getUsr" value="" id="UserEducationUserId"/>
					</div>
                    <div class="col-sm-2 form-group">
                        <label>Qualification Obtained</label>
                        <div class="input text"><input placeholder="Qualification" class="form-control qualification_obt" value="<?=$qual_obt4?>" maxlength="255" type="text" id="qual_obt4"/></div>                                                            
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Year Awarded</label>
                         <div class="input select">
						<select placeholder="Year - YYYY" class="form-control year_awarded" id="qual_yr4">
							<option value="">Select</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
						</select>
					</div>          						                                                  
				</div>
					<!-- <div class="col-sm-2 form-group">
						<label for="">Add Institution</label>
					<h1>+</h1>
					</div> -->
            </div>
				<!--div class="row" id="afterThis">
                    <div class="col-sm-6 pull-right">                                  
                        <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Qualification</a>
                    </div>                                                                    
                </div-->
            </div>               
        </div>


    </div><!-- /.box-body -->
</div>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">References</h3>
        <!-- <h5 class="text-muted">Please provide details for at least two professional referees from your most recent employers. At least one of these referees should be able to confirm your experience in care. If you worked for private clients only three references are required.</h5> -->
		<h5 class="text-muted">At least one must be your most recent employer; your direct supervisor or senior.  Not staff on the same level. References from friends are not acceptable.Minimum of 2 references</h5>

    </div><!-- /.box-header -->
    <div class="box-body">
        <input type="hidden" value="" class="getUsr" id="UserReferenceUserId"/>
            <div class="form-group col-md-12 " style="margin-top: 10px;">
                <h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Reference 1</span></strong><strong><span class="red">*</span></strong> <span class="pull-right red" style="font-size:12px;">* All fields are required</span></h5>
                <hr style="margin: 2px;">
            </div>


            <div class="col-md-12  u_r_b">
                <div class="col-sm-6 form-group">
                    <label>Name</label>
                    <div class="input text"><input placeholder="Name" class="form-control user_reference_name" value="<?=$ref1_name?>" required="required" maxlength="255" type="text" id="ref1_name"/></div>
                    <input type="hidden" placeholder="Name" class="form-control user_reference_name" value="" id="UserReferenceId"/>                
				</div>
                <div class="col-sm-6 form-group">
                    <label>Company</label>
                    <div class="input text"><input placeholder="Company" class="form-control user_reference_company" value="<?=$ref1_company?>" required="required" maxlength="500" type="text" id="ref1_company"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Job Title</label>
                    <div class="input text"><input placeholder="Job Title" class="form-control user_reference_job_title" value="<?=$ref1_job?>" required="required" maxlength="255" type="text" id="ref1_job"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Email</label>
                    <div class="input email"><input placeholder="Email" class="form-control user_reference_email" value="<?=$ref1_email?>" required="required" maxlength="255" type="email" id="ref1_email"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Address</label>
                    <div class="input text"><input placeholder="Address" class="form-control user_reference_address" value="<?=$ref1_address1?>" required="required" maxlength="500" type="text" id="ref1_address1"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Address2</label>
                    <div class="input text"><input placeholder="Address2" class="form-control user_reference_address2" value="<?=$ref1_address2?>" required="required" type="text" id="ref1_address2"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Town</label>
                    <div class="input text"><input placeholder="Town" class="form-control user_reference_town" value="<?=$ref1_town?>" required="required" maxlength="255" type="text" id="ref1_town"/></div>
				</div>
                <div class="col-sm-6 form-group">
                    <label>Contact Number</label>
                    <div class="input text"><input placeholder="Contact Number" class="form-control user_reference_contact_number" value="<?=$ref1_phone?>" required="required" maxlength="100" type="text" id="ref1_phone"/></div>                                                       
                </div>

                <div class="col-sm-6 form-group">
                    <label>Postcode</label>
                    <div class="input text"><input placeholder="Postcode" class="form-control user_reference_postcode" value="<?=$ref1_postcode?>" required="required" maxlength="100" type="text" id="ref1_postcode"/></div>                                                        
                </div>
                
                <div class="col-sm-6 form-group">
                    <label style="margin-top: 30px;">OK to contact?</label> 
                    <div class="btn-group pull-right" data-toggle="buttons"  style="margin-top: 30px;">
						<label class="btn btn-default <?=$ref1_yes?>">
                            <input type="radio" class="ok_contact" id="ref1_yes"/> Yes
                        </label> 
                        <label class="btn btn-default <?=$ref1_no?>">
                            <input type="radio" class="ok_contact" id="ref1_no"/> No
                        </label> 
                    </div>                    
                </div> 
            </div>
         
<input type="hidden" name="data[UserReference][1][user_id]" value="" class="getUsr" id="UserReferenceUserId"/>
            <div class="form-group col-md-12 " style="margin-top: 10px;">
                <h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Reference 2</span></strong><strong><span class="red">*</span></strong> <span class="pull-right red" style="font-size:12px;">* All fields are required</span></h5>
                <hr style="margin: 2px;">
            </div>


            <div class="col-md-12  u_r_b">
                <div class="col-sm-6 form-group">
                    <label>Name</label>
                    <div class="input text"><input placeholder="Name" class="form-control user_reference_name" value="<?=$ref2_name?>" required="required" maxlength="255" type="text" id="ref2_name"/></div>
                    <input type="hidden" placeholder="Name" class="form-control user_reference_name" value="" id="UserReferenceId"/>                
				</div>
                <div class="col-sm-6 form-group">
                    <label>Company</label>
                    <div class="input text"><input placeholder="Company" class="form-control user_reference_company" value="<?=$ref2_company?>" required="required" maxlength="500" type="text" id="ref2_company"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Job Title</label>
                    <div class="input text"><input placeholder="Job Title" class="form-control user_reference_job_title" value="<?=$ref2_job?>" required="required" maxlength="255" type="text" id="ref2_job"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Email</label>
                    <div class="input email"><input placeholder="Email" class="form-control user_reference_email" value="<?=$ref2_email?>" required="required" maxlength="255" type="email" id="ref2_email"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Address</label>
                    <div class="input text"><input placeholder="Address" class="form-control user_reference_address" value="<?=$ref2_address1?>" required="required" maxlength="500" type="text" id="ref2_address1"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Address2</label>
                    <div class="input text"><input placeholder="Address2" class="form-control user_reference_address2" value="<?=$ref2_address2?>" required="required" type="text" id="ref2_address2"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Town</label>
                    <div class="input text"><input placeholder="Town" class="form-control user_reference_town" value="<?=$ref2_town?>" required="required" maxlength="255" type="text" id="ref2_town"/></div>
				</div>
                <div class="col-sm-6 form-group">
                    <label>Contact Number</label>
                    <div class="input text"><input placeholder="Contact Number" class="form-control user_reference_contact_number" value="<?=$ref2_phone?>" required="required" maxlength="100" type="text" id="ref2_phone"/></div>                                                       
                </div>

                <div class="col-sm-6 form-group">
                    <label>Postcode</label>
                    <div class="input text"><input placeholder="Postcode" class="form-control user_reference_postcode" value="<?=$ref2_postcode?>" required="required" maxlength="100" type="text" id="ref2_postcode"/></div>                                                        
                </div>
                
                <div class="col-sm-6 form-group">
                    <label style="margin-top: 30px;">OK to contact?</label> 
                    <div class="btn-group pull-right" data-toggle="buttons"  style="margin-top: 30px;">
						<label class="btn btn-default <?=$ref2_yes?>">
                            <input type="radio" class="ok_contact" id="ref2_yes"/> Yes
                        </label> 
                        <label class="btn btn-default <?=$ref2_no?>">
                            <input type="radio" class="ok_contact" id="ref2_no"/> No
                        </label> 
                    </div>                    
                </div> 
            </div>
         
<!--input type="hidden" name="data[UserReference][2][user_id]" value="" class="getUsr" id="UserReferenceUserId"/>
            <div class="form-group col-md-12 ref3" style="margin-top: 10px;">
                <h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Reference 3</span></strong></h5>
                <hr style="margin: 2px;">
            </div>


            <div class="col-md-12 ref3 u_r_b">
                <div class="col-sm-6 form-group">
                    <label>Name</label>
                    <div class="input text"><input name="data[UserReference][2][name]" placeholder="Name" class="form-control user_reference_name" value="" ="" maxlength="255" type="text" id="UserReferenceName"/></div>
                    <input type="hidden" name="data[UserReference][2][id]" placeholder="Name" class="form-control user_reference_name" value="" id="UserReferenceId"/>                </div>
                <div class="col-sm-6 form-group">
                    <label>Company</label>
                    <div class="input text"><input name="data[UserReference][2][company_name]" placeholder="Company" class="form-control user_reference_company" value="" ="" maxlength="500" type="text" id="UserReferenceCompanyName"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Job Title</label>
                    <div class="input text"><input name="data[UserReference][2][job_title]" placeholder="Job Title" class="form-control user_reference_job_title" value="" ="" maxlength="255" type="text" id="UserReferenceJobTitle"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Email</label>
                    <div class="input email"><input name="data[UserReference][2][email]" placeholder="Email" class="form-control user_reference_email" value="" ="" maxlength="255" type="email" id="UserReferenceEmail"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Address</label>
                    <div class="input text"><input name="data[UserReference][2][address]" placeholder="Address" class="form-control user_reference_address" value="" ="" maxlength="500" type="text" id="UserReferenceAddress"/></div>                                                        
                </div>

                <div class="col-sm-6 form-group">
                    <label>Address2</label>
                    <div class="input text"><input name="data[UserReference][2][address2]" placeholder="Address2" class="form-control user_reference_address2" value="" ="" type="text" id="UserReferenceAddress2"/></div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Town</label>
                    <div class="input text"><input name="data[UserReference][2][town]" placeholder="Town" class="form-control user_reference_town" value="" ="" maxlength="255" type="text" id="UserReferenceTown"/></div>                </div>
                 <div class="col-sm-6 form-group"> 
                    <label>Borough</label> -->
                                   <!--  </div> -->

                <!-- <div class="col-sm-6 form-group">
                    <label>Contact Number</label>
                    <div class="input text"><input name="data[UserReference][2][phone]" placeholder="Contact Number" class="form-control user_reference_contact_number" value="" ="" maxlength="100" type="text" id="UserReferencePhone"/></div>                                                       
                </div>

                <div class="col-sm-6 form-group">
                    <label>Postcode</label>
                    <div class="input text"><input name="data[UserReference][2][postcode]" placeholder="Postcode" class="form-control user_reference_postcode" value="" ="" maxlength="100" type="text" id="UserReferencePostcode"/></div>                                                        
                </div>
                
                <div class="col-sm-6 form-group">
                    <label style="margin-top: 30px;">OK to contact?</label> 
                    <div class="btn-group pull-right" data-toggle="buttons"  style="margin-top: 30px;">
                                                <label class="btn btn-default ">
                            <input type="radio" class="ok_contact"  name="data[UserReference][2][can_contact]" value="Yes"  /> Yes
                        </label> 
                        <label class="btn btn-default ">
                            <input type="radio" class="ok_contact"  name="data[UserReference][2][can_contact]" value="No" /> No
                        </label> 
                    </div>                    
                </div> 
            </div>
			
        <button class="btn btn-warning pull-right" type="button" id="add_ref" style="margin-right: 15px;"> <i class="fa fa-plus"></i>Add Reference</button>
        <button class="btn btn-danger pull-right ref3" type="button" id="del_ref" style="margin-right: 15px;"> <i class="fa fa-minus"></i> &nbsp;Remove Reference</button-->

    </div>
</div>

<script>
    function setWizardHeight() {
        var smartWizard = $('#wizard').data('smartWizard');
        smartWizard.fixHeight();
    }

	var edu_counter = 0;
    $("#edu_add").on('click', function () {
         edu_counter = edu_counter + 1;
         var newHtmlEdu = $("#more_edu").clone();
         
         $(newHtmlEdu).find("input").each(function () {
            var valName = $(this).attr('name');
            valName = valName.replace('{@}', edu_counter);
            $(this).attr('name', valName);
        });

         $(newHtmlEdu).find("select").each(function () {
            var valName = $(this).attr('name');
            valName = valName.replace('{@}', edu_counter);
            $(this).attr('name', valName);
        }); 

        $(newHtmlEdu).find("textarea").each(function () {
            var valNameTxt = $(this).attr('name');
            valNameTxt = valNameTxt.replace('{@}', edu_counter);
            $(this).attr('name', valNameTxt);
        });
        $("#afterThis").before($(newHtmlEdu).html());

        /*$(".edu_row").find("input.clonepicker").each(function () {
			$(this).datepicker({
				maxDate: new Date(),
				yearRange: '-30:'+new Date(), 
				dateFormat:"dd/mm/yy",
				changeMonth: true,
				changeYear: true,
				todayHighlight: true,
			});
		});*/
        setWizardHeight(); 
    });
    $(document).on('click', ".edu_remove", function () {
        edu_counter = edu_counter - 1; 
        $(this).closest('.edu_row').slideUp('slow', function () {
            $(this).detach();
            setWizardHeight();
        });
    });
    $("#add_ref").on('click', function () {
        $(".ref3").show();
        $(this).hide();
        setWizardHeight();
    });

    $(document).on('click', '#del_ref', function () {
        $(".ref3").hide();
        $(this).hide();
        $("#add_ref").show();
        setWizardHeight();
    });
    $(function(){
		$("#qual_yr").val("<?=$qual_yr?>").change();
		/*function chkOpt(id, opt){
			var isChecked = true;
			if(opt == "checked")
				isChecked = true;
			else
				isChecked = false;
			
			if(isChecked) {
				$button.removeClass('btn-default').addClass('btn-' + color + ' active');
			}else {
				$button.removeClass('btn-' + color + ' active').addClass('btn-default');
			}
		}*/
        $('.button-checkbox').each(function () {
            // Settings
            var $widget = $(this),
			$button = $widget.find('button'),
			$checkbox = $widget.find('input:checkbox'),
			color = $button.data('color'),
			settings = {
				on: {
					icon: 'glyphicon glyphicon-check'
				},
				off: {
					icon: 'glyphicon glyphicon-unchecked'
				}
			};
            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");
                // Set the button's icon
                $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
                // Update the button's color
                if (isChecked) {
                    $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                }else {
                    $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                }
            }
            // Initialization
            function init() {
                updateDisplay();
                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
</script>

<script>
	$(function(){
		$("#institution").blur(function(){
			var a = $("#institution").val().trim();
			if(a == ""){
				$('#institution').closest(".form-group").addClass("has-error");
			}else{
				$('#institution').closest(".form-group").removeClass("has-error");
			}
		});
		// $("#institution2").blur(function(){
		// 	var a = $("#institution2").val().trim();
		// 	if(a == ""){
		// 		$('#institution2').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#institution2').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#institution3").blur(function(){
		// 	var a = $("#institution3").val().trim();
		// 	if(a == ""){
		// 		$('#institution3').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#institution3').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#institution4").blur(function(){
		// 	var a = $("#institution4").val().trim();
		// 	if(a == ""){
		// 		$('#institution4').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#institution4').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		$("#qual_sub").blur(function(){
			var a = $("#qual_sub").val().trim();
			if(a == ""){
				$('#qual_sub').closest(".form-group").addClass("has-error");
			}else{
				$('#qual_sub').closest(".form-group").removeClass("has-error");
			}
		});
		// $("#qual_sub2").blur(function(){
		// 	var a = $("#qual_sub2").val().trim();
		// 	if(a == ""){
		// 		$('#qual_sub2').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_sub2').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_sub3").blur(function(){
		// 	var a = $("#qual_sub3").val().trim();
		// 	if(a == ""){
		// 		$('#qual_sub3').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_sub3').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_sub4").blur(function(){
		// 	var a = $("#qual_sub4").val().trim();
		// 	if(a == ""){
		// 		$('#qual_sub4').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_sub4').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		$("#qual_obt").blur(function(){
			var a = $("#qual_obt").val().trim();
			if(a == ""){
				$('#qual_obt').closest(".form-group").addClass("has-error");
			}else{
				$('#qual_obt').closest(".form-group").removeClass("has-error");
			}
		});
		// $("#qual_obt2").blur(function(){
		// 	var a = $("#qual_obt2").val().trim();
		// 	if(a == ""){
		// 		$('#qual_obt2').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_obt2').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_obt3").blur(function(){
		// 	var a = $("#qual_obt3").val().trim();
		// 	if(a == ""){
		// 		$('#qual_obt3').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_obt3').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_obt4").blur(function(){
		// 	var a = $("#qual_obt4").val().trim();
		// 	if(a == ""){
		// 		$('#qual_obt4').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_obt4').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		$("#qual_yr").blur(function(){
			var a = $("#qual_yr").val().trim();
			if(a == ""){
				$('#qual_yr').closest(".form-group").addClass("has-error");
			}else{
				$('#qual_yr').closest(".form-group").removeClass("has-error");
			}
		});
		// $("#qual_yr2").blur(function(){
		// 	var a = $("#qual_yr2").val().trim();
		// 	if(a == ""){
		// 		$('#qual_yr2').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_yr2').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_yr3").blur(function(){
		// 	var a = $("#qual_yr3").val().trim();
		// 	if(a == ""){
		// 		$('#qual_yr3').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_yr3').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		// $("#qual_yr4").blur(function(){
		// 	var a = $("#qual_yr4").val().trim();
		// 	if(a == ""){
		// 		$('#qual_yr4').closest(".form-group").addClass("has-error");
		// 	}else{
		// 		$('#qual_yr4').closest(".form-group").removeClass("has-error");
		// 	}
		// });
		$("#ref1_name").blur(function(){
			var a = $("#ref1_name").val().trim();
			if(a == ""){
				$('#ref1_name').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_name').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_company").blur(function(){
			var a = $("#ref1_company").val().trim();
			if(a == ""){
				$('#ref1_company').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_company').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_job").blur(function(){
			var a = $("#ref1_job").val().trim();
			if(a == ""){
				$('#ref1_job').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_job').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_email").blur(function(){
			var a = $("#ref1_email").val().trim();
			if(a == ""){
				$('#ref1_email').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_email').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_address1").blur(function(){
			var a = $("#ref1_address1").val().trim();
			if(a == ""){
				$('#ref1_address1').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_address1').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_address2").blur(function(){
			var a = $("#ref1_address2").val().trim();
			if(a == ""){
				$('#ref1_address2').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_address2').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_town").blur(function(){
			var a = $("#ref1_town").val().trim();
			if(a == ""){
				$('#ref1_town').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_town').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_phone").blur(function(){
			var a = $("#ref1_phone").val().trim();
			if(a == ""){
				$('#ref1_phone').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_phone').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_postcode").blur(function(){
			var a = $("#ref1_postcode").val().trim();
			if(a == ""){
				$('#ref1_postcode').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_postcode').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_yes").blur(function(){
			var a = $("#ref1_yes").val().trim();
			if(a == ""){
				$('#ref1_yes').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_yes').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref1_no").blur(function(){
			var a = $("#ref1_no").val().trim();
			if(a == ""){
				$('#ref1_no').closest(".form-group").addClass("has-error");
			}else{
				$('#ref1_no').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_name").blur(function(){
			var a = $("#ref2_name").val().trim();
			if(a == ""){
				$('#ref2_name').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_name').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_company").blur(function(){
			var a = $("#ref2_company").val().trim();
			if(a == ""){
				$('#ref2_company').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_company').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_job").blur(function(){
			var a = $("#ref2_job").val().trim();
			if(a == ""){
				$('#ref2_job').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_job').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_email").blur(function(){
			var a = $("#ref2_email").val().trim();
			if(a == ""){
				$('#ref2_email').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_email').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_address1").blur(function(){
			var a = $("#ref2_address1").val().trim();
			if(a == ""){
				$('#ref2_address1').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_address1').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_address2").blur(function(){
			var a = $("#ref2_address2").val().trim();
			if(a == ""){
				$('#ref2_address2').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_address2').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_town").blur(function(){
			var a = $("#ref2_town").val().trim();
			if(a == ""){
				$('#ref2_town').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_town').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_phone").blur(function(){
			var a = $("#ref2_phone").val().trim();
			if(a == ""){
				$('#ref2_phone').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_phone').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_postcode").blur(function(){
			var a = $("#ref2_postcode").val().trim();
			if(a == ""){
				$('#ref2_postcode').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_postcode').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_yes").blur(function(){
			var a = $("#ref2_yes").val().trim();
			if(a == ""){
				$('#ref2_yes').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_yes').closest(".form-group").removeClass("has-error");
			}
		});
		$("#ref2_no").blur(function(){
			var a = $("#ref2_no").val().trim();
			if(a == ""){
				$('#ref2_no').closest(".form-group").addClass("has-error");
			}else{
				$('#ref2_no').closest(".form-group").removeClass("has-error");
			}
		});
				
		$(".buttonNext").click(function(e){
			var step = $("#PrevStep").val();
			e.preventDefault();
			function doRadio(id, id2){
				var gid = '#'+id;
				var gid2 = '#'+id2;
				if($(gid).prop("checked")){
					$(gid2).removeAttr("checked");
					return 1;
				}else{
					return 0;
				}
			}
			
			if(step == "step-2"){
				var institution 	= $("#institution").val().trim();
				var institution2 	= $("#institution2").val();
				var institution3 	= $("#institution3").val();
				var institution4 	= $("#institution4").val();
				var qual_sub 		= $("#qual_sub").val().trim();
				var qual_sub2 		= $("#qual_sub2").val();
				var qual_sub3 		= $("#qual_sub3").val();
				var qual_sub4 		= $("#qual_sub4").val();
				var qual_obt	 	= $("#qual_obt").val().trim();
				var qual_obt2	 	= $("#qual_obt2").val();
				var qual_obt3	 	= $("#qual_obt3").val();
				var qual_obt4	 	= $("#qual_obt4").val();
				var qual_yr		 	= $("#qual_yr").val().trim();
				var qual_yr2		= $("#qual_yr2").val();
				var qual_yr3		= $("#qual_yr3").val();
				var qual_yr4		= $("#qual_yr4").val();
				var ref1_name		= $("#ref1_name").val().trim();
				var ref1_company	= $("#ref1_company").val().trim();
				var ref1_job		= $("#ref1_job").val().trim();
				var ref1_email		= $("#ref1_email").val().trim();
				var ref1_address1	= $("#ref1_address1").val().trim();
				var ref1_address2	= $("#ref1_address2").val().trim();
				var ref1_town		= $("#ref1_town").val().trim();
				var ref1_phone		= $("#ref1_phone").val().trim();
				var ref1_postcode	= $("#ref1_postcode").val().trim();
				var ref1_yes		= doRadio("ref1_yes", "ref1_no");
				var ref1_no			= doRadio("ref1_no", "ref1_yes");
				var ref2_name		= $("#ref2_name").val().trim();
				var ref2_company	= $("#ref2_company").val().trim();
				var ref2_job		= $("#ref2_job").val().trim();
				var ref2_email		= $("#ref2_email").val().trim();
				var ref2_address1	= $("#ref2_address1").val().trim();
				var ref2_address2	= $("#ref2_address2").val().trim();
				var ref2_town		= $("#ref2_town").val().trim();
				var ref2_phone		= $("#ref2_phone").val().trim();
				var ref2_postcode	= $("#ref2_postcode").val().trim();
				var ref2_yes		= doRadio("ref2_yes", "ref2_no");
				var ref2_no			= doRadio("ref2_no", "ref2_yes");
				//alert(ref2_no);
				
				if(institution=="" || qual_sub=="" || qual_obt=="" || qual_yr=="" || ref1_name=="" || ref1_company=="" || ref1_job=="" || ref1_email=="" || ref1_address1=="" || ref1_address2=="" || ref1_town=="" || ref1_phone=="" || ref1_postcode=="" || ref2_name=="" || ref2_company=="" || ref2_job=="" || ref2_email=="" || ref2_address1=="" || ref2_address2=="" || ref2_town=="" || ref2_phone=="" || ref2_postcode==""){
					//alert(ref1_yes+":"+ref1_no+":"+ref2_yes+":"+ref2_no);
					console.log(institution);
					console.log(qual_sub);
					console.log(qual_obt);
					console.log(qual_yr);
					alert("Some important fields were left blank!");
				}else{
					$(".overlay").show();
					$(".ajax-spinner").show();
					$.ajax({
						type: "POST",
						url: "includes/step3/",
						data: "institution="+institution+"institution2="+institution2+"institution3="+institution3+"institution4="+institution4+"qual_sub="+qual_sub+"qual_sub2="+qual_sub2+"qual_sub3="+qual_sub3+"qual_sub4="+qual_sub4+"&qual_obt="+qual_obt+"&qual_obt2="+qual_obt2+"&qual_obt3="+qual_obt3+"&qual_obt4="+qual_obt4+"&qual_yr="+qual_yr+"&qual_yr2="+qual_yr2+"&qual_yr3="+qual_yr3+"&qual_yr4="+qual_yr4+"&ref1_name="+ref1_name+"&ref1_company="+ref1_company+"&ref1_job="+ref1_job+"&ref1_email="+ref1_email+"&ref1_address1="+ref1_address1+"&ref1_address2="+ref1_address2+"&ref1_town="+ref1_town+"&ref1_phone="+ref1_phone+"&ref1_postcode="+ref1_postcode+"&ref1_yes="+ref1_yes+"&ref1_no="+ref1_no+"&ref2_name="+ref2_name+"&ref2_company="+ref2_company+"&ref2_job="+ref2_job+"&ref2_email="+ref2_email+"&ref2_address1="+ref2_address1+"&ref2_address2="+ref2_address2+"&ref2_town="+ref2_town+"&ref2_phone="+ref2_phone+"&ref2_postcode="+ref2_postcode+"&ref2_yes="+ref2_yes+"&ref2_no="+ref2_no+"&posted=step3",
						cache: false,
						success: function(datum){
							var result = datum;
							if(result == 'correct'){
								$(".overlay").hide();
								$(".ajax-spinner").hide();
								$("#step-3").hide();
								$("#step-4").show();
								$('#innerStep3').addClass("done");
								$('#innerStep3').removeClass("selected");
								$('#innerStep4').removeClass("disabled");
								$('#innerStep4').addClass("selected");
								$("#PrevStep").val("step-3");
								window.location = "./#circleBtn1";
							}else{
								alert(datum);
							}
						}
					});
				}
			}
		});
	});
</script>

<script>
</script>
												 
</div>