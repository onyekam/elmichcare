<?php

	include_once("db.php");
	
	$institution1 = $qual_sub1 = $qual_obt1 = $qual_yr1 = $ex_company = $ex_address = $ex_post = $ex_phone = $ex_start = $ex_end = $ex_notice = $start_when = $ex_task = $ex_reason = "";
	
	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM a_step2 WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){
				$institution1 	= $rows['institution1'];
				$qual_sub1 		= $rows['qual_sub1'];
				$qual_obt1	 	= $rows['qual_obt1'];
				$qual_yr1		= $rows['qual_yr1'];
				$ex_company 	= $rows['ex_company'];
				$ex_address	 	= $rows['ex_address'];
				$ex_post		= $rows['ex_post'];
				$ex_phone		= $rows['ex_phone'];
				$ex_start		= $rows['ex_start'];
				$ex_end			= $rows['ex_end'];
				$ex_notice		= $rows['ex_notice'];
				$start_when		= $rows['start_when'];
				$ex_task		= $rows['ex_task'];
				$ex_reason		= $rows['ex_reason'];
			}
		}
	}
?>
						<div id="step-2">
								
							<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Employment History</h3>
		<h5 class="text-muted">Your complete employment experience.</h5>
		<h5>Please write your college or university if coming out from full time education </h5>
		
		<!-- Education 3 -->
		<div class="col-md-12">
            <div id="block_edu">
				<div class="row">
					<div class="col-sm-3 form-group" id="qualifications">
							<label>Institution</label>                        
							<div class="input text">
								<input placeholder="Institution" class="form-control institution_title" value="<?=$institution1?>" type="text" id="institution1"/>
							</div>
							<!-- <input type="hidden" class="getUsr" value="" id="UserEducationUserId"/> -->
						</div>
					<div class="col-sm-3 form-group">
                        <label>Subjects or Title</label>                        
                        <div class="input text">
							<input placeholder="Subjects or Title" class="form-control education_title" value="<?=$qual_sub1?>" type="text" id="qual_sub1"/>
						</div>
						<input type="hidden" class="getUsr" value="" id="UserEducationUserId"/>
					</div>
                    <div class="col-sm-2 form-group">
                        <label>Qualification Obtained</label>
                        <div class="input text"><input placeholder="Qualification" class="form-control qualification_obt" value="<?=$qual_obt1?>" maxlength="255" type="text" id="qual_obt1"/></div>                                                            
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Year Awarded</label>
                         <div class="input select">
							<select placeholder="Year - YYYY" class="form-control year_awarded" id="qual_yr1">
							<option value="">Select</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
						</select>
					</div>          						                                                  
				</div>
					<!-- <div class="col-sm-2 form-group">
						<label for="">Add Institution</label>
					<h1>+</h1>
					</div> -->
            </div>
				<!--div class="row" id="afterThis">
                    <div class="col-sm-6 pull-right">                                  
                        <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Qualification</a>
                    </div>                                                                    
                </div-->
            </div>               
		</div>
    </div><!-- /.box-header -->
    <input type="hidden" id="invalid_dates" />
    <div id="employments" class="box-body">
<div class="row row_emp employment-row ">
    
                <div class="form-group col-md-12" style="margin-top: 10px;">
                    <h5><strong><i class="fa fa-map-marker margin-r-5"></i><span class="text-primary">Most Recent Employer</span></strong></h5>
                    <hr style="margin: 2px;">
                </div>
                        <div class="col-md-12 emp_hist_block">		
                <div class="col-sm-6 form-group">
                    <label>Employer</label><span class="red">*</span>
                    <div class="input text">
						<input placeholder="Employer Name" class="form-control emp-company" value="<?=$ex_company?>" maxlength="255" type="text" id="ex_company"/>
					</div>
					<input type="hidden" placeholder="Employer Name" class="form-control" value="" id="UserExperienceId"/>
					<input type="hidden" class="getUsr" value="" id="UserExperienceUserId"/>
                </div>
                <div class="col-sm-6 form-group">
                    <label>Address</label><span class="red">*</span>
                    <div class="input text">
						<input placeholder="Address" class="form-control emp-address" value="<?=$ex_address?>" maxlength="500" type="text" id="ex_address"/>
					</div> 
                </div>
                <div class="col-sm-6 form-group">
                    <label>Position Held</label><span class="red">*</span>
                    <div class="input text">
						<input placeholder="Position" class="form-control emp-position" value="<?=$ex_post?>" maxlength="100" type="text" id="ex_post"/>
					</div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Telephone Number</label><span class="red">*</span>
                    <div class="input text">
						<input placeholder="Contact Number" class="form-control emp-phone" value="<?=$ex_phone?>" maxlength="100" type="text" id="ex_phone"/>
					</div>
				</div>
                <div class="col-sm-6 ">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>From</label><span class="red">*</span>
                            <div class="input text">
								<input placeholder="dd-mm-yyyy" id="ex_start" class="form-control setdatePicker newStart date_from" value="<?=$ex_start?>" maxlength="255" type="text"/>
							</div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>To</label><span class="red">*</span>
                            <div class="input text">
								<input placeholder="dd-mm-yyyy" id="ex_end" class="form-control setdatePicker date_to" value="<?=$ex_end?>" maxlength="255" type="text"/>
							</div>
						</div>
                    </div>
                </div>
                <div class="col-sm-3 form-group">
                    <label>Leaving date or notice (if relevant)</label><span class="red">*</span>
                    <div class="input text">
						<input id="ex_notice" placeholder="dd-mm-yyyy" class="form-control setdatePicker emp-notice" value="<?=$ex_notice?>" maxlength="255" type="text"/>
					</div>                                                        
                </div>
				<div class="col-sm-3 form-group">
                    <label>When can you start?</label><span class="red">*</span>
                    <div class="input text">
						<input id="start_when" placeholder="dd-mm-yyyy" class="form-control setdatePicker start_when" value="<?=$start_when?>" maxlength="255" type="text"/>
					</div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Key Tasks/Responsibilities</label><span class="red">*</span>
                    <div class="input textarea">
						<textarea placeholder="" class="form-control emp-job_desc" cols="30" rows="6" id="ex_task"><?=$ex_task?></textarea>
					</div>                                                        
                </div>
                <div class="col-sm-6 form-group">
                    <label>Reason for leaving</label><span class="red">*</span>
                    <div class="input textarea">
						<textarea placeholder="" id="ex_reason" class="form-control emp-reason_leaving" cols="30" rows="6"><?=$ex_reason?></textarea>
					</div>                                                        
                </div>
            </div> 
        </div>
        
    </div>
</div>
<div id="employmentsResult"></div>
<div class="box box-default">    
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12 ">
                                <div id="block_gap">

                    
                    
                                         <div class="row">
                       
                    </div>
                                        

                    <!-- <div class="row" id="afterThisGap">
                        <div class="col-sm-6 pull-right">                                  
                            <a class="btn btn-warning btn-flat pull-right" href="javascript:void(0);" id="gap_add">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Gap</a>
                        </div>                                                                    
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            
            <div class="overlay"></div>
        </div>
    </div>              
</div>


<script id="employmentGapTemplate" type="text/x-jQuery-tmpl">
        <span class="gap_error">Please Explain the Gaps In Employment below from ${startMonth} ${startYear} to  ${endMonth} ${endYear}. You Can Mention For Example That You Were Student, Housewife, self employed</span>
		<div class="row employment-gap employment-gap-${index}">
		  <div class="col-sm-4 form-group">
			<label>Date From</label>
			<div class="input text">
			  <input type="text" value="${start}" name="data[GapsInEmployment][${index}][gapDateFrom]" class="form-control" readonly="readonly">
			</div>
		  </div>
		  <div class="col-sm-4 form-group">
			<label>Date To</label>
			<div class="input text">
			  <input type="text" value="${end}"  name="data[GapsInEmployment][${index}][gapDateTo]" class="form-control" readonly="readonly">
			</div>
		  </div>
		  <div class="col-sm-4 form-group">
			<label>Reason for gap</label>
			<div class="input text">
			  <input type="text" class="form-control reasonForGap"  name="data[GapsInEmployment][${index}][gapReason]" placeholder="Reason for gap">
			</div>
		  </div>
		</div>
    </script> 

<script>
    var emp_counter = 2;
    var gap_counter = 0;
	
    $(function(){
        $('.button-checkbox').each(function () {
            // Settings
            var $widget = $(this),
				$button = $widget.find('button'),
				$checkbox = $widget.find('input:checkbox'),
				color = $button.data('color'),
				settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					},
					off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
				};
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                }else {
                    $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                }
            }
            // Initialization
            function init() {
                updateDisplay();
                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
</script>

<script>
	$(function(){
		$("#ex_company").blur(function(){
			var a = $("#ex_company").val().trim();
			if(a == ""){
				$('#ex_company').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_company').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_address").blur(function(){
			var a = $("#ex_address").val().trim();
			if(a == ""){
				$('#ex_address').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_address').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_post").blur(function(){
			var a = $("#ex_post").val().trim();
			if(a == ""){
				$('#ex_post').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_post').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_phone").blur(function(){
			var a = $("#ex_phone").val().trim();
			if(a == ""){
				$('#ex_phone').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_phone').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_start").blur(function(){
			var a = $("#ex_start").val().trim();
			if(a == ""){
				$('#ex_start').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_start').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_end").blur(function(){
			var a = $("#ex_end").val().trim();
			if(a == ""){
				$('#ex_end').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_end').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_notice").blur(function(){
			var a = $("#ex_notice").val().trim();
			if(a == ""){
				$('#ex_notice').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_notice').closest(".form-group").removeClass("has-error");
			}
		});

		$("#start_when").blur(function(){
			var a = $("#start_when").val().trim();
			if(a == ""){
				$('#start_when').closest(".form-group").addClass("has-error");
			}else{
				$('#start_when').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_task").blur(function(){
			var a = $("#ex_task").val().trim();
			if(a == ""){
				$('#ex_task').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_task').closest(".form-group").removeClass("has-error");
			}
		});
		
		$("#ex_reason").blur(function(){
			var a = $("#ex_reason").val().trim();
			if(a == ""){
				$('#ex_reason').closest(".form-group").addClass("has-error");
			}else{
				$('#ex_reason').closest(".form-group").removeClass("has-error");
			}
		});
				
		$(".buttonNext").click(function(e){
			var step = $("#PrevStep").val();
			e.preventDefault();
			if(step == "step-1"){				
				var ex_company 		= $("#ex_company").val().trim();
				var ex_address	 	= $("#ex_address").val().trim();
				var ex_post		 	= $("#ex_post").val().trim();
				var ex_phone		= $("#ex_phone").val().trim();
				var ex_start		= $("#ex_start").val().trim();
				var ex_end			= $("#ex_end").val().trim();
				var ex_notice		= $("#ex_notice").val().trim();
				var start_when		= $("#start_when").val().trim();
				var ex_task			= $("#ex_task").val().trim();
				var ex_reason		= $("#ex_reason").val().trim();
				var institution1 	= $("#institution1").val();
				var qual_sub1 		= $("#qual_sub1").val();
				var qual_obt1	 	= $("#qual_obt1").val();
				var qual_yr1		 	= $("#qual_yr1").val();
				
				
				if(ex_company=="" || ex_address=="" || ex_post=="" || ex_phone=="" || ex_start=="" || ex_end=="" || ex_notice=="" || start_when=="" || ex_task=="" || ex_reason==""){
					alert("Some important fields were left blank!");
				}else{
					$(".overlay").show();
					$(".ajax-spinner").show();
					$.ajax({
						type: "POST",
						url: "includes/step2/",
						data: "institution1="+institution1+"qual_sub1="+qual_sub1+"&qual_obt1="+qual_obt1+"&qual_yr1="+qual_yr1+"ex_company="+ex_company+"&ex_address="+ex_address+"&ex_post="+ex_post+"&ex_phone="+ex_phone+"&ex_start="+ex_start+"&ex_end="+ex_end+"&ex_notice="+ex_notice+"&start_when="+start_when+"&ex_task="+ex_task+"&ex_reason="+ex_reason+"&posted=step2",
						cache: false,
						success: function(datum){
							var result = datum.trim();
							if(result == 'correct'){
								$(".overlay").hide();
								$(".ajax-spinner").hide();
								$("#step-2").hide();
								$("#step-3").show();
								$('#innerStep2').addClass("done");
								$('#innerStep2').removeClass("selected");
								$('#innerStep3').removeClass("disabled");
								$('#innerStep3').addClass("selected");
								$("#PrevStep").val("step-2");
								window.location = "./#circleBtn1";
							}else{
								alert(datum);
							}
						}
					});
				}
			}
		});
	});
</script>
						   
</div> 