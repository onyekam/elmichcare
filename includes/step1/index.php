<?php
	session_start();
	
	if(isset($_POST['posted'])){
		include_once("db.php");
		$em_mon			= trim(mysqli_real_escape_string($CONN, $_POST['em_mon'])); 
		$em_tue			= trim(mysqli_real_escape_string($CONN, $_POST['em_tue'])); 
		$em_wed			= trim(mysqli_real_escape_string($CONN, $_POST['em_wed'])); 
		$em_thu			= trim(mysqli_real_escape_string($CONN, $_POST['em_thu'])); 
		$em_fri			= trim(mysqli_real_escape_string($CONN, $_POST['em_fri'])); 
		$em_sat			= trim(mysqli_real_escape_string($CONN, $_POST['em_sat'])); 
		$em_sun			= trim(mysqli_real_escape_string($CONN, $_POST['em_sun'])); 
		$lm_mon			= trim(mysqli_real_escape_string($CONN, $_POST['lm_mon'])); 
		$lm_tue			= trim(mysqli_real_escape_string($CONN, $_POST['lm_tue'])); 
		$lm_wed			= trim(mysqli_real_escape_string($CONN, $_POST['lm_wed'])); 
		$lm_thu			= trim(mysqli_real_escape_string($CONN, $_POST['lm_thu'])); 
		$lm_fri			= trim(mysqli_real_escape_string($CONN, $_POST['lm_fri'])); 
		$lm_sat			= trim(mysqli_real_escape_string($CONN, $_POST['lm_sat'])); 
		$lm_sun			= trim(mysqli_real_escape_string($CONN, $_POST['lm_sun'])); 
		$ea_mon			= trim(mysqli_real_escape_string($CONN, $_POST['ea_mon'])); 
		$ea_tue			= trim(mysqli_real_escape_string($CONN, $_POST['ea_tue'])); 
		$ea_wed			= trim(mysqli_real_escape_string($CONN, $_POST['ea_wed'])); 
		$ea_thu			= trim(mysqli_real_escape_string($CONN, $_POST['ea_thu'])); 
		$ea_fri			= trim(mysqli_real_escape_string($CONN, $_POST['ea_fri'])); 
		$ea_sat			= trim(mysqli_real_escape_string($CONN, $_POST['ea_sat'])); 
		$ea_sun			= trim(mysqli_real_escape_string($CONN, $_POST['ea_sun'])); 
		$la_mon			= trim(mysqli_real_escape_string($CONN, $_POST['la_mon'])); 
		$la_tue			= trim(mysqli_real_escape_string($CONN, $_POST['la_tue'])); 
		$la_wed			= trim(mysqli_real_escape_string($CONN, $_POST['la_wed'])); 
		$la_thu			= trim(mysqli_real_escape_string($CONN, $_POST['la_thu'])); 
		$la_fri			= trim(mysqli_real_escape_string($CONN, $_POST['la_fri'])); 
		$la_sat			= trim(mysqli_real_escape_string($CONN, $_POST['la_sat'])); 
		$la_sun			= trim(mysqli_real_escape_string($CONN, $_POST['la_sun'])); 
		$ev_mon			= trim(mysqli_real_escape_string($CONN, $_POST['ev_mon'])); 
		$ev_tue			= trim(mysqli_real_escape_string($CONN, $_POST['ev_tue'])); 
		$ev_wed			= trim(mysqli_real_escape_string($CONN, $_POST['ev_wed'])); 
		$ev_thu			= trim(mysqli_real_escape_string($CONN, $_POST['ev_thu'])); 
		$ev_fri			= trim(mysqli_real_escape_string($CONN, $_POST['ev_fri'])); 
		$ev_sat			= trim(mysqli_real_escape_string($CONN, $_POST['ev_sat'])); 
		$ev_sun			= trim(mysqli_real_escape_string($CONN, $_POST['ev_sun'])); 
		$wn_mon			= trim(mysqli_real_escape_string($CONN, $_POST['wn_mon'])); 
		$wn_tue			= trim(mysqli_real_escape_string($CONN, $_POST['wn_tue'])); 
		$wn_wed			= trim(mysqli_real_escape_string($CONN, $_POST['wn_wed'])); 
		$wn_thu			= trim(mysqli_real_escape_string($CONN, $_POST['wn_thu'])); 
		$wn_fri			= trim(mysqli_real_escape_string($CONN, $_POST['wn_fri'])); 
		$wn_sat			= trim(mysqli_real_escape_string($CONN, $_POST['wn_sat'])); 
		$wn_sun			= trim(mysqli_real_escape_string($CONN, $_POST['wn_sun'])); 
		$sn_mon			= trim(mysqli_real_escape_string($CONN, $_POST['sn_mon'])); 
		$sn_tue			= trim(mysqli_real_escape_string($CONN, $_POST['sn_tue'])); 
		$sn_wed			= trim(mysqli_real_escape_string($CONN, $_POST['sn_wed'])); 
		$sn_thu			= trim(mysqli_real_escape_string($CONN, $_POST['sn_thu'])); 
		$sn_fri			= trim(mysqli_real_escape_string($CONN, $_POST['sn_fri'])); 
		$sn_sat			= trim(mysqli_real_escape_string($CONN, $_POST['sn_sat'])); 
		$sn_sun			= trim(mysqli_real_escape_string($CONN, $_POST['sn_sun'])); 
		$will_hrs		= trim(mysqli_real_escape_string($CONN, $_POST['will_hrs']));
		$live_work_uk	= trim(mysqli_real_escape_string($CONN, $_POST['live_work_uk'])); 
		$f_name			= trim(mysqli_real_escape_string($CONN, $_POST['f_name'])); 
		$relation		= trim(mysqli_real_escape_string($CONN, $_POST['relation'])); 
		$contact		= trim(mysqli_real_escape_string($CONN, $_POST['contact']));
		$heard			= trim(mysqli_real_escape_string($CONN, $_POST['heard']));
		$otherSearchEngine			= trim(mysqli_real_escape_string($CONN, $_POST['otherSearchEngine']));
		$otherJobSite			= trim(mysqli_real_escape_string($CONN, $_POST['otherJobSite']));
		$elmichCareStaff			= trim(mysqli_real_escape_string($CONN, $_POST['elmichCareStaff']));
		$email			= $_SESSION['userMail'];
		
		$str = "SELECT email FROM a_step1 WHERE email='$email'";
		$res = mysqli_query($CONN, $str);
		$row = mysqli_num_rows($res);
		
		$actStr = "";
		if($row > 0){
			$actStr = "UPDATE a_step1 SET em_mon='$em_mon', em_tue='$em_tue', em_wed='$em_wed', em_thu='$em_thu', em_fri='$em_fri', em_sat='$em_sat', em_sun='$em_sun', lm_mon='$lm_mon', lm_tue='$lm_tue', lm_wed='$lm_wed', lm_thu='$lm_thu', lm_fri='$lm_fri', lm_sat='$lm_sat', lm_sun='$lm_sun', ea_mon='$ea_mon', ea_tue='$ea_tue', ea_wed='$ea_wed', ea_thu='$ea_thu', ea_fri='$ea_fri', ea_sat='$ea_sat', ea_sun='$ea_sun', la_mon='$la_mon', la_tue='$la_tue', la_wed='$la_wed', la_thu='$la_thu', la_fri='$la_fri', la_sat='$la_sat', la_sun='$la_sun', ev_mon='$ev_mon', ev_tue='$ev_tue', ev_wed='$ev_wed', ev_thu='$ev_thu', ev_fri='$ev_fri', ev_sat='$ev_sat', ev_sun='$ev_sun', wn_mon='$wn_mon', wn_tue='$wn_tue', wn_wed='$wn_wed', wn_thu='$wn_thu', wn_fri='$wn_fri', wn_sat='$wn_sat', wn_sun='$wn_sun', sn_mon='$sn_mon', sn_tue='$sn_tue', sn_wed='$sn_wed', sn_thu='$sn_thu', sn_fri='$sn_fri', sn_sat='$sn_sat', sn_sun='$sn_sun', will_hrs='$will_hrs', live_work_uk='$live_work_uk', f_name='$f_name', relation='$relation', contact='$contact', heard='$heard', otherSearchEngine='$otherSearchEngine', otherJobSite='$otherJobSite', elmichCareStaff='$elmichCareStaff' WHERE email='$email'";
		}else{
			$actStr = "INSERT INTO a_step1 VALUES('$em_mon', '$em_tue', '$em_wed', '$em_thu', '$em_fri', '$em_sat', '$em_sun', '$lm_mon', '$lm_tue', '$lm_wed', '$lm_thu', '$lm_fri', '$lm_sat', '$lm_sun', '$ea_mon', '$ea_tue', '$ea_wed', '$ea_thu', '$ea_fri', '$ea_sat', '$ea_sun', '$la_mon', '$la_tue', '$la_wed', '$la_thu', '$la_fri', '$la_sat', '$la_sun', '$ev_mon', '$ev_tue', '$ev_wed', '$ev_thu', '$ev_fri', '$ev_sat', '$ev_sun', '$wn_mon', '$wn_tue', '$wn_wed', '$wn_thu', '$wn_fri', '$wn_sat', '$wn_sun', '$sn_mon', '$sn_tue', '$sn_wed', '$sn_thu', '$sn_fri', '$sn_sat', '$sn_sun', '$will_hrs', '$live_work_uk', '$f_name', '$relation', '$contact', '$heard', '$otherSearchEngine', '$otherJobSite', '$elmichCareStaff', '$email')";
		}
		
		$actRes = mysqli_query($CONN, $actStr);
		if($actRes == 1){
			mysqli_query($CONN, "UPDATE p_info SET step='step1' WHERE email='$email'");
			echo "correct";
		}else{
			echo 'Connection failed: ' . mysqli_error($CONN);
		}
	}
?>