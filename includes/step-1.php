<?php

	include_once("db.php");
	
	function crschk($val){
		if($val == 0)
			return "";
		else
			return "checked";
	}
	$em_mon = $em_tue = $em_wed = $em_thu = $em_fri = $em_sat = $em_sun = "";
	$lm_mon = $lm_tue = $lm_wed = $lm_thu = $lm_fri = $lm_sat = $lm_sun = "";
	$ea_mon = $ea_tue = $ea_wed = $ea_thu = $ea_fri = $ea_sat = $ea_sun = "";
	$la_mon = $la_tue = $la_wed = $la_thu = $la_fri = $la_sat = $la_sun = "";
	$ev_mon = $ev_tue = $ev_wed = $ev_thu = $ev_fri = $ev_sat = $ev_sun = "";
	$wn_mon = $wn_tue = $wn_wed = $wn_thu = $wn_fri = $wn_sat = $wn_sun = "";
	$sn_mon = $sn_tue = $sn_wed = $sn_thu = $sn_fri = $sn_sat = $sn_sun = "";
	$will_hrs = $live_work_uk = $f_name = $relation = $contact = $heard = $otherSearchEngine = $otherJobSite = $elmichCareStaff = "";

	
	if($userMail != "NONE"){
		$strSF1 = "SELECT * FROM a_step1 WHERE email='$userMail'";
		$resSF1 = mysqli_query($CONN, $strSF1);
		
		if(mysqli_num_rows($resSF1) > 0){
			while($rows = mysqli_fetch_array($resSF1)){
				$em_mon			= crschk($rows['em_mon']); 
				$em_tue			= crschk($rows['em_tue']); 
				$em_wed			= crschk($rows['em_wed']); 
				$em_thu			= crschk($rows['em_thu']); 
				$em_fri			= crschk($rows['em_fri']); 
				$em_sat			= crschk($rows['em_sat']); 
				$em_sun			= crschk($rows['em_sun']); 
				$lm_mon			= crschk($rows['lm_mon']); 
				$lm_tue			= crschk($rows['lm_tue']); 
				$lm_wed			= crschk($rows['lm_wed']); 
				$lm_thu			= crschk($rows['lm_thu']); 
				$lm_fri			= crschk($rows['lm_fri']); 
				$lm_sat			= crschk($rows['lm_sat']); 
				$lm_sun			= crschk($rows['lm_sun']); 
				$ea_mon			= crschk($rows['ea_mon']); 
				$ea_tue			= crschk($rows['ea_tue']); 
				$ea_wed			= crschk($rows['ea_wed']); 
				$ea_thu			= crschk($rows['ea_thu']); 
				$ea_fri			= crschk($rows['ea_fri']); 
				$ea_sat			= crschk($rows['ea_sat']); 
				$ea_sun			= crschk($rows['ea_sun']); 
				$la_mon			= crschk($rows['la_mon']); 
				$la_tue			= crschk($rows['la_tue']); 
				$la_wed			= crschk($rows['la_wed']); 
				$la_thu			= crschk($rows['la_thu']); 
				$la_fri			= crschk($rows['la_fri']); 
				$la_sat			= crschk($rows['la_sat']); 
				$la_sun			= crschk($rows['la_sun']); 
				$ev_mon			= crschk($rows['ev_mon']); 
				$ev_tue			= crschk($rows['ev_tue']); 
				$ev_wed			= crschk($rows['ev_wed']); 
				$ev_thu			= crschk($rows['ev_thu']); 
				$ev_fri			= crschk($rows['ev_fri']); 
				$ev_sat			= crschk($rows['ev_sat']); 
				$ev_sun			= crschk($rows['ev_sun']); 
				$wn_mon			= crschk($rows['wn_mon']); 
				$wn_tue			= crschk($rows['wn_tue']); 
				$wn_wed			= crschk($rows['wn_wed']); 
				$wn_thu			= crschk($rows['wn_thu']); 
				$wn_fri			= crschk($rows['wn_fri']); 
				$wn_sat			= crschk($rows['wn_sat']); 
				$wn_sun			= crschk($rows['wn_sun']); 
				$sn_mon			= crschk($rows['sn_mon']); 
				$sn_tue			= crschk($rows['sn_tue']); 
				$sn_wed			= crschk($rows['sn_wed']); 
				$sn_thu			= crschk($rows['sn_thu']); 
				$sn_fri			= crschk($rows['sn_fri']); 
				$sn_sat			= crschk($rows['sn_sat']); 
				$sn_sun			= crschk($rows['sn_sun']); 
				$will_hrs		= $rows['will_hrs'];
				$live_work_uk	= $rows['live_work_uk']; 
				$f_name			= $rows['f_name']; 
				$relation		= $rows['relation']; 
				$contact		= $rows['contact'];
				$heard			= $rows['heard'];
				$otherSearchEngine			= $rows['otherSearchEngine'];
				$otherJobSite			= $rows['otherJobSite'];
				$elmichCareStaff			= $rows['elmichCareStaff'];
			}
		}
	}
?>
	<div id="step-1">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Availability</h3>
				<h5 class="text-muted">Please specify what days and time you are available to work (you may choose more than one shift pattern).</h5>
			</div>    
			<input type="hidden" id="inner_steps" value="step-1">
			<div class="box-body">
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Early Mornings</label>
						<h4><span class="label label-default">7:00 am - 10:00 am</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="em_mon" <?=$em_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="em_tue" <?=$em_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="em_wed" <?=$em_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="em_thu" <?=$em_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="em_fri" <?=$em_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="em_sat" <?=$em_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="em_sun" <?=$em_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Late Mornings</label>
						<h4><span class="label label-default">10:00 am - 12:00 pm</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="lm_mon" <?=$lm_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="lm_tue" <?=$lm_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="lm_wed" <?=$lm_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="lm_thu" <?=$lm_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="lm_fri" <?=$lm_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="lm_sat" <?=$lm_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="lm_sun" <?=$lm_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
		  
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Early Afternoons</label>
						<h4><span class="label label-default">12:00 pm - 3:00 pm</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="ea_mon" <?=$ea_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="ea_tue" <?=$ea_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="ea_wed" <?=$ea_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="ea_thu" <?=$ea_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="ea_fri" <?=$ea_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="ea_sat" <?=$ea_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="ea_sun" <?=$ea_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Late Afternoons</label>
						<h4><span class="label label-default">3:00 pm - 6:00 pm</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="la_mon" <?=$la_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="la_tue" <?=$la_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="la_wed" <?=$la_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="la_thu" <?=$la_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="la_fri" <?=$la_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="la_sat" <?=$la_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="la_sun" <?=$la_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Evenings</label>
						<h4><span class="label label-default">6:00 pm - 10:00 pm</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="ev_mon" <?=$ev_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="ev_tue" <?=$ev_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="ev_wed" <?=$ev_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="ev_thu" <?=$ev_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="ev_fri" <?=$ev_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="ev_sat" <?=$ev_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="ev_sun" <?=$ev_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Waking Nights</label>
						<h4><span class="label label-default">8:00 pm - 8:00 am</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="wn_mon" <?=$wn_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="wn_tue" <?=$wn_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="wn_wed" <?=$wn_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="wn_thu" <?=$wn_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="wn_fri" <?=$wn_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="wn_sat" <?=$wn_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="wn_sun" <?=$wn_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
				<div class="form-group col-md-12">
					<div class=" col-md-3">
						<label>Sleeping Nights</label>
						<h4><span class="label label-default">8:00 pm - 8:00 am</span></h4>
					</div>
					<div class="col-md-9">
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Monday</button>
							<input type="checkbox" class="hidden"  value="Monday" id="sn_mon" <?=$sn_mon?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Tuesday</button>
							<input type="checkbox" class="hidden"  value="Tuesday" id="sn_tue" <?=$sn_tue?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Wednesday</button>
							<input type="checkbox" class="hidden"  value="Wednesday" id="sn_wed" <?=$sn_wed?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Thursday</button>
							<input type="checkbox" class="hidden"  value="Thursday" id="sn_thu" <?=$sn_thu?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Friday</button>
							<input type="checkbox" class="hidden"  value="Friday" id="sn_fri" <?=$sn_fri?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Saturday</button>
							<input type="checkbox" class="hidden"  value="Saturday" id="sn_sat" <?=$sn_sat?>/>
						</span>
						<span class="button-checkbox " style="margin: 0px 5px;">
							<button type="button" class="btn btn-sm " data-color="primary">Sunday</button>
							<input type="checkbox" class="hidden"  value="Sunday" id="sn_sun" <?=$sn_sun?>/>
						</span>
					</div>
					<hr style="margin: 2px 0px; float: left; width: 100%;">
				</div>
		  
				<div class="form-group col-md-12" style="margin-top: 10px;"></div>      
	
				<div class="form-group col-md-6">
					<label>How many hours per week are you willing to work?</label><span class="red">*</span>
					<div class="input text">
						<input placeholder="Hours" maxlength="3" class="form-control" required="required" type="text" id="will_hrs" value="<?=$will_hrs?>"/>
					</div>        
				</div>
				<div class="form-group col-md-6">
					<label>Do you have current right to live and work in the UK?</label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" required="required" id="live_work_uk">
							<option value="">Select</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					</div> 
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">
					<strong>
						<i class="fa fa-map-marker margin-r-5"></i>
						<span class="text-primary">Emergency Contact</span>
					</strong>
					<hr style="margin: 2px;">
				</div>
				<div class="form-group col-md-6">
					<label>Full Name</label><span class="red">*</span>
					<div class="input text">
						<input placeholder="Full Name" class="form-control" required="required" maxlength="255" type="text" id="f_nameX" value="<?=$f_name?>"/>
					</div>                                                    
				</div>
				<div class="form-group col-md-6">
					<label>Relationship</label><span class="red">*</span>
					<div class="input text">
						<input placeholder="Relationship" class="form-control" required="required" maxlength="255" type="text" id="relation" value="<?=$relation?>"/>
					</div>                                                    
				</div>
				<div class="form-group col-md-6">
					<label>Contact number</label><span class="red">*</span>
					<div class="input text">
						<input placeholder="Contact number" class="form-control" required="required" maxlength="100" type="text" id="contact" value="<?=$contact?>"/>
					</div>                                                    
				</div>
				<div class="form-group col-md-12" style="margin-top: 10px;">               
					<hr style="margin: 2px;">
				</div>
				<div class="form-group col-md-6">
					<label>How did you Hear about us? </label><span class="red">*</span>
					<div class="input select">
						<select class="form-control" id="heard">
							<option value="">Select</option>
							<option value="Friend">Friend</option>
							<option value="Leaflet">Leaflet</option>
							<option value="Job Centre">Job Centre</option>
							<option value="Elmich Care Website">Elmich Care Website</option>
							<option value="Google Search">Google Search</option>
							<option value="Other Search Engine">Other Search Engine</option>
							<option value="Gumtree">Gumtree</option>
							<option value="reeds.co.uk">reeds.co.uk</option>
							<option value="Other job site">Other Job site</option>
							<option value="Elmich Care Staff">Elmich Care Staff</option>
						</select>
					</div>

					
					
					
					
				</div>
				<div class="form-group col-md-6" id="otherHeards">
					
					
					
				</div>
			</div><!-- /.box-body -->
			<div class="overlay"></div>    	 
		</div>

		<script>
			$(function () {
				$("#live_work_uk").val("<?=$live_work_uk?>").change();
				$("#heard").val("<?=$heard?>").change();
				$('.button-checkbox').each(function () {
					var $widget = $(this),
					$button = $widget.find('button'),
					$checkbox = $widget.find('input:checkbox'),
					color = $button.data('color'),
					settings = {
						on: {
							icon: 'glyphicon glyphicon-check'
						},
						off: {
							icon: 'glyphicon glyphicon-unchecked'
						}
					};                
					// Event Handlers
					$button.on('click', function () {
						$checkbox.prop('checked', !$checkbox.is(':checked'));
						$checkbox.triggerHandler('change');
						updateDisplay();
					});
					$checkbox.on('change', function () {
						updateDisplay();
					});
					// Actions
					function updateDisplay() {
						var isChecked = $checkbox.is(':checked');
						$button.data('state', (isChecked) ? "on" : "off");
						$button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
						// Update the button's color
						if (isChecked) {
							$button.removeClass('btn-default').addClass('btn-' + color + ' active');
						} else {
							$button.removeClass('btn-' + color + ' active').addClass('btn-default');
						}
					}
					
					function init() {
						updateDisplay();
						if ($button.find('.state-icon').length == 0) {
							$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
						}
					}
					init();
				});
			});
		</script>
	</div>
	<script>
		$(function(){
			$("#heard").change(function(){
				var heard = $("#heard").val();
				if (heard == "Other Search Engine") {
					$("#otherHeards").empty();
					$("#otherHeards").append('<div class="input text"><label>Other Search Engine </label><span class="red">*</span><input type="text" name="otherSearchEngine" id="otherSearchEngine" class="form-control" placeholder="Name of other search engine" value="<?=$otherSearchEngine?>"/></div>');
				} else if (heard == "Other job site") {
					$("#otherHeards").empty();
					$("#otherHeards").append('<div class="input text"><label>Other Job Site </label><span class="red">*</span><input type="text" name="otherJobSite" id="otherJobSite" class="form-control" placeholder="Name of other job site" value="<?=$otherJobSite?>"/></div>');
				} else if (heard == "Elmich Care Staff") {
					$("#otherHeards").empty();
					$("#otherHeards").append('<div class="input text"><label>Elmi Care Staff</label><span class="red">*</span><input type="text" name="elmichCareStaff" id="elmichCareStaff" class="form-control" placeholder="Name of Elmich Care Staff" value="<?=$elmichCareStaff?>" /></div>');
				} else {
					$("#otherHeards").empty();
				}

			});
		});
	</script>
	
	<script>
		$(function(){
			$(".buttonNext").click(function(e){
				var step = $("#PrevStep").val();
				e.preventDefault();
				if(step == ""){
					var em_mon = em_tue = em_wed = em_thu = em_fri = em_sat = em_sun = 0;
					if($("#em_mon").prop("checked")){
						em_mon = 1;
					}if($("#em_tue").prop("checked")){
						em_tue = 1;
					}if($("#em_wed").prop("checked")){
						em_wed = 1;
					}if($("#em_thu").prop("checked")){
						em_thu = 1;
					}if($("#em_fri").prop("checked")){
						em_fri = 1;
					}if($("#em_sat").prop("checked")){
						em_sat = 1;
					}if($("#em_sun").prop("checked")){
						em_sun = 1;
					}
					var lm_mon = lm_tue = lm_wed = lm_thu = lm_fri = lm_sat = lm_sun = 0;
					if($("#lm_mon").prop("checked")){
						lm_mon = 1;
					}if($("#lm_tue").prop("checked")){
						lm_tue = 1;
					}if($("#lm_wed").prop("checked")){
						lm_wed = 1;
					}if($("#lm_thu").prop("checked")){
						lm_thu = 1;
					}if($("#lm_fri").prop("checked")){
						lm_fri = 1;
					}if($("#lm_sat").prop("checked")){
						lm_sat = 1;
					}if($("#lm_sun").prop("checked")){
						lm_sun = 1;
					}
					var ea_mon = ea_tue = ea_wed = ea_thu = ea_fri = ea_sat = ea_sun = 0;
					if($("#ea_mon").prop("checked")){
						ea_mon = 1;
					}if($("#ea_tue").prop("checked")){
						ea_tue = 1;
					}if($("#ea_wed").prop("checked")){
						ea_wed = 1;
					}if($("#ea_thu").prop("checked")){
						ea_thu = 1;
					}if($("#ea_fri").prop("checked")){
						ea_fri = 1;
					}if($("#ea_sat").prop("checked")){
						ea_sat = 1;
					}if($("#ea_sun").prop("checked")){
						ea_sun = 1;
					}
					var la_mon = la_tue = la_wed = la_thu = la_fri = la_sat = la_sun = 0;
					if($("#la_mon").prop("checked")){
						la_mon = 1;
					}if($("#la_tue").prop("checked")){
						la_tue = 1;
					}if($("#la_wed").prop("checked")){
						la_wed = 1;
					}if($("#la_thu").prop("checked")){
						la_thu = 1;
					}if($("#la_fri").prop("checked")){
						la_fri = 1;
					}if($("#la_sat").prop("checked")){
						la_sat = 1;
					}if($("#la_sun").prop("checked")){
						la_sun = 1;
					}
					var ev_mon = ev_tue = ev_wed = ev_thu = ev_fri = ev_sat = ev_sun = 0;
					if($("#ev_mon").prop("checked")){
						ev_mon = 1;
					}if($("#ev_tue").prop("checked")){
						ev_tue = 1;
					}if($("#ev_wed").prop("checked")){
						ev_wed = 1;
					}if($("#ev_thu").prop("checked")){
						ev_thu = 1;
					}if($("#ev_fri").prop("checked")){
						ev_fri = 1;
					}if($("#ev_sat").prop("checked")){
						ev_sat = 1;
					}if($("#ev_sun").prop("checked")){
						ev_sun = 1;
					}
					var wn_mon = wn_tue = wn_wed = wn_thu = wn_fri = wn_sat = wn_sun = 0;
					if($("#wn_mon").prop("checked")){
						wn_mon = 1;
					}if($("#wn_tue").prop("checked")){
						wn_tue = 1;
					}if($("#wn_wed").prop("checked")){
						wn_wed = 1;
					}if($("#wn_thu").prop("checked")){
						wn_thu = 1;
					}if($("#wn_fri").prop("checked")){
						wn_fri = 1;
					}if($("#wn_sat").prop("checked")){
						wn_sat = 1;
					}if($("#wn_sun").prop("checked")){
						wn_sun = 1;
					}
					var sn_mon = sn_tue = sn_wed = sn_thu = sn_fri = sn_sat = sn_sun = 0;
					if($("#sn_mon").prop("checked")){
						sn_mon = 1;
					}if($("#sn_tue").prop("checked")){
						sn_tue = 1;
					}if($("#sn_wed").prop("checked")){
						sn_wed = 1;
					}if($("#sn_thu").prop("checked")){
						sn_thu = 1;
					}if($("#sn_fri").prop("checked")){
						sn_fri = 1;
					}if($("#sn_sat").prop("checked")){
						sn_sat = 1;
					}if($("#sn_sun").prop("checked")){
						sn_sun = 1;
					}
					
					var will_hrs 		= $("#will_hrs").val().trim();
					var live_work_uk 	= $("#live_work_uk").val().trim();
					var f_name		 	= $("#f_nameX").val().trim();
					var relation		= $("#relation").val().trim();
					var contact			= $("#contact").val().trim();
					var heard			= $("#heard").val().trim();
					
					
					if(will_hrs == ""){
						$('#will_hrs').closest(".form-group").addClass("has-error");
					}else{
						$('#will_hrs').closest(".form-group").removeClass("has-error");
					}if(live_work_uk == ""){
						$('#live_work_uk').closest(".form-group").addClass("has-error");
					}else{
						$('#live_work_uk').closest(".form-group").removeClass("has-error");
					}if(f_name == ""){
						$('#f_nameX').closest(".form-group").addClass("has-error");
					}else{
						$('#f_nameX').closest(".form-group").removeClass("has-error");
					}if(relation == ""){
						$('#relation').closest(".form-group").addClass("has-error");
					}else{
						$('#relation').closest(".form-group").removeClass("has-error");
					}if(contact == ""){
						$('#contact').closest(".form-group").addClass("has-error");
					}else{
						$('#contact').closest(".form-group").removeClass("has-error");
					}if(heard == ""){
						$('#heard').closest(".form-group").addClass("has-error");
					}else{
						if (heard == "Other Search Engine") {
							var otherSearchEngine = $('#otherSearchEngine').val().trim();
							if (otherSearchEngine == "") {
								$("#otherSearchEngine").closest(".form-group").addClass("has-error");
							} else {
								$("#otherSearchEngine").closest(".form-group").removeClass("has-error");
							}
						} else if (heard == "Other job site") {
							var otherJobSite = $('#otherJobSite').val().trim();
							if (otherJobSite == "") {
								$("#otherJobSite").closest(".form-group").addClass("has-error");
							} else {
								$("#otherJobSite").closest(".form-group").removeClass("has-error");
							}
						} else if (heard == "Elmich Care Staff") {
							var elmichCareStaff = $('#elmichCareStaff').val().trim();
							if (elmichCareStaff == "") {
								$("#elmichCareStaff").closest(".form-group").addClass("has-error");
							} else {
								$("#elmichCareStaff").closest(".form-group").removeClass("has-error");
							}
						} else {
							$('#heard').closest(".form-group").removeClass("has-error");
						}
					}
					
					
					if(will_hrs=="" || live_work_uk=="" || f_name=="" || relation=="" || contact=="" || heard=="" || otherSearchEngine=="" || otherJobSite=="" || elmichCareStaff==""){
						alert("Some important fields were left blank!");
					}else{
						$(".overlay").show();
						$(".ajax-spinner").show();
						$.ajax({
							type: "POST",
							url: "includes/step1/",
							data: "em_mon="+em_mon+"&em_tue="+em_tue+"&em_wed="+em_wed+"&em_thu="+em_thu+"&em_fri="+em_fri+"&em_sat="+em_sat+"&em_sun="+em_sun+"&lm_mon="+lm_mon+"&lm_tue="+lm_tue+"&lm_wed="+lm_wed+"&lm_thu="+lm_thu+"&lm_fri="+lm_fri+"&lm_sat="+lm_sat+"&lm_sun="+lm_sun+"&ea_mon="+ea_mon+"&ea_tue="+ea_tue+"&ea_wed="+ea_wed+"&ea_thu="+ea_thu+"&ea_fri="+ea_fri+"&ea_sat="+ea_sat+"&ea_sun="+ea_sun+"&la_mon="+la_mon+"&la_tue="+la_tue+"&la_wed="+la_wed+"&la_thu="+la_thu+"&la_fri="+la_fri+"&la_sat="+la_sat+"&la_sun="+la_sun+"&ev_mon="+ev_mon+"&ev_tue="+ev_tue+"&ev_wed="+ev_wed+"&ev_thu="+ev_thu+"&ev_fri="+ev_fri+"&ev_sat="+ev_sat+"&ev_sun="+ev_sun+"&wn_mon="+wn_mon+"&wn_tue="+wn_tue+"&wn_wed="+wn_wed+"&wn_thu="+wn_thu+"&wn_fri="+wn_fri+"&wn_sat="+wn_sat+"&wn_sun="+wn_sun+"&sn_mon="+sn_mon+"&sn_tue="+sn_tue+"&sn_wed="+sn_wed+"&sn_thu="+sn_thu+"&sn_fri="+sn_fri+"&sn_sat="+sn_sat+"&sn_sun="+sn_sun+"&will_hrs="+will_hrs+"&live_work_uk="+live_work_uk+"&f_name="+f_name+"&relation="+relation+"&contact="+contact+"&heard="+heard+"&otherSearchEngine="+otherSearchEngine+"&otherJobSite="+otherJobSite+"&elmichCareStaff="+elmichCareStaff+"&posted=step1",
							cache: false,
							success: function(datum){
					
								var result = datum.trim();
								if(result == 'correct'){
									$(".overlay").hide();
									$(".ajax-spinner").hide();
									$("#step-1").hide();
									$("#step-2").show();
									$('#innerStep1').addClass("done");
									$('#innerStep1').removeClass("selected");
									$('#innerStep2').removeClass("disabled");
									$('#innerStep2').addClass("selected");
									$("#PrevStep").val("step-1");
									$('.buttonPrevious').removeClass("buttonDisabled");
									window.location = "./#circleBtn1";
								}else{
									alert(datum);
								}
							}
						});
					}
				}
			});
		});
	</script>