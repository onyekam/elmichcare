/*
 * SmartWizard 3.3.1 plugin
 * jQuery Wizard control Plugin
 * by Dipu
 *
 * Refactored and extended:
 * https://github.com/mstratman/jQuery-Smart-Wizard
 *
 * Original URLs:
 * http://www.techlaboratory.net
 * http://tech-laboratory.blogspot.com
 */

function SmartWizard(target, options) {
    this.target       = target;
    this.options      = options;
    this.curStepIdx   = options.selected;
    this.steps        = $(target).children("ul").children("li").children("a"); // Get all anchors
    this.contentWidth = 0;
    this.msgBox = $('<div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div>');
    this.elmStepContainer = $('<div></div>').addClass("stepContainer");
    this.loader = $('<div>Loading</div>').addClass("loader");
    this.buttons = {
        next : $('<a>'+options.labelNext+'</a>').attr("href","#").addClass("buttonNext"),
        previous : $('<a>'+options.labelPrevious+'</a>').attr("href","#").addClass("buttonPrevious"),
        finish  : $('<a>'+options.labelFinish+'</a>').attr("href","#").addClass("buttonFinish")
    };

    /*
     * Private functions
     */

    var _init = function($this) {
        var elmActionBar = $('<div></div>').addClass("actionBar");
        elmActionBar.append($this.msgBox);
        $('.close',$this.msgBox).click(function() {
            $this.msgBox.fadeOut("normal");
            return false;
        });

        var allDivs = $this.target.children('div');
        // CHeck if ul with steps has been added by user, if not add them
        if($this.target.children('ul').length == 0 ){
            var ul = $("<ul/>");
            target.prepend(ul)

            // for each div create a li
            allDivs.each(function(i,e){
                var title = $(e).first().children(".StepTitle").text();
                var s = $(e).attr("id")
                // if referenced div has no id, add one.
                if (s==undefined){
                    s = "step-"+(i+1)
                    $(e).attr("id",s);
                }
                var span = $("<span/>").addClass("stepDesc").text(title);
                var li = $("<li></li>").append($("<a></a>").attr("href", "#" + s).append($("<label></label>").addClass("stepNumber").text(i + 1)).append(span));
                ul.append(li);
            });
            // (re)initialise the steps property
            $this.steps = $(target).children("ul").children("li").children("a"); // Get all anchors
        }
        $this.target.children('ul').addClass("anchor");
        allDivs.addClass("content");

        // highlight steps with errors
        if($this.options.errorSteps && $this.options.errorSteps.length>0){
            $.each($this.options.errorSteps, function(i, n){
                $this.setError({ stepnum: n, iserror:true });
            });
        }

        $this.elmStepContainer.append(allDivs);
        elmActionBar.append($this.loader);
        $this.target.append($this.elmStepContainer);

        if ($this.options.includeFinishButton){
            elmActionBar.append($this.buttons.finish)
        }

        elmActionBar.append($this.buttons.next)
            .append($this.buttons.previous);
        $this.target.append(elmActionBar);
        $this.target.prepend(elmActionBar.clone());
        this.contentWidth = $this.elmStepContainer.width();

        $(".actionBar").on('click', '.buttonNext',function() {
            $this.goForward();
            return false;
        });
        $(".actionBar").on('click', '.buttonPrevious',function() {
            $this.goBackward();
            return false;
        });
        $(".actionBar").on('click', '.buttonFinish',function() {
        //$($this.buttons.finish).click(function() {
            if(!$(this).hasClass('buttonDisabled')){
                
                if($.isFunction($this.options.onFinish)) {
                    
                   var context = { fromStep: $this.curStepIdx + 1 };
                    if(!$this.options.onFinish.call(this,$($this.steps), context)){
                        return false;
                    }
                }else{
                   
                   
                    //var frm = $this.target.parents('form');
                    //if(frm && frm.length){
                      //  frm.submit();
                    //}
                    
                    
                    
                }
            }
            return false;
        });

        $($this.steps).bind("click", function(e){
			console.log("Step Navigation Click is disabled: ",$this.options.disabledStepNavigationClick);
			if($this.options.disabledStepNavigationClick){
				return false;
			}
            if($this.steps.index(this) == $this.curStepIdx){
                return false;
            }
            var nextStepIdx = $this.steps.index(this);
            var isDone = $this.steps.eq(nextStepIdx).attr("isDone") - 0;
            if(isDone == 1){
                _loadContent($this, nextStepIdx);
            }
            return false;
        });

        // Enable keyboard navigation
        if($this.options.keyNavigation){
            $(document).keyup(function(e){
                if(e.which==39){ // Right Arrow
                    $this.goForward();
                }else if(e.which==37){ // Left Arrow
                    $this.goBackward();
                }
            });
        }
        //  Prepare the steps
        _prepareSteps($this);
        // Show the first slected step
        _loadContent($this, $this.curStepIdx);
    };

    var _prepareSteps = function($this) {
        if(! $this.options.enableAllSteps){
            $($this.steps, $this.target).removeClass("selected").removeClass("done").addClass("disabled");
            $($this.steps, $this.target).attr("isDone",0);
        }else{
            $($this.steps, $this.target).removeClass("selected").removeClass("disabled").addClass("done");
            $($this.steps, $this.target).attr("isDone",1);
        }

        $($this.steps, $this.target).each(function(i){
            $($(this).attr("href").replace(/^.+#/, '#'), $this.target).hide();
            $(this).attr("rel",i+1);
        });
    };

    var _step = function ($this, selStep) { 
        return $($(selStep, $this.target).attr("href").replace(/^.+#/, '#'), $this.target );
    };

    var _loadContent = function($this, stepIdx) {
        var selStep = $this.steps.eq(stepIdx);
        var ajaxurl = $this.options.contentURL;
        console.log(ajaxurl);
        var ajaxurl_data = $this.options.contentURLData;
        var hasContent = selStep.data('hasContent');
        var stepNum = stepIdx+1;
        if (ajaxurl && ajaxurl.length>0) {
            if ($this.options.contentCache && hasContent) {
                _showStep($this, stepIdx);
            } else {
                var ajax_args = {
                    url: ajaxurl,
                    type: $this.options.ajaxType,
                    data: ({step_number : stepNum}),
                    dataType: "text",
                    beforeSend: function(){
                        $this.loader.show();
                    },
                    error: function(){
                        $this.loader.hide();
                    },
                    success: function(res){
                        $this.loader.hide();
                        if(res && res.length>0){
                            selStep.data('hasContent',true);
                            _step($this, selStep).html(res);
                            _showStep($this, stepIdx);
                        }
                    }
                };
                if (ajaxurl_data) {
                    ajax_args = $.extend(ajax_args, ajaxurl_data(stepNum));
                }
                $.ajax(ajax_args);
            }
        }else{
            _showStep($this,stepIdx);
        }
    };

    var _showStep = function($this, stepIdx) {
        var selStep = $this.steps.eq(stepIdx);
        var curStep = $this.steps.eq($this.curStepIdx);
        if(stepIdx != $this.curStepIdx){
            if($.isFunction($this.options.onLeaveStep)) {
                var context = { fromStep: $this.curStepIdx+1, toStep: stepIdx+1 };
                if (! $this.options.onLeaveStep.call($this,$(curStep), context)){
                    return false;
                }
            }
        }
        //$this.elmStepContainer.height(_step($this, selStep).outerHeight());
        var prevCurStepIdx = $this.curStepIdx;
        $this.curStepIdx =  stepIdx;
        if ($this.options.transitionEffect == 'slide'){
            _step($this, curStep).slideUp("fast",function(e){
                _step($this, selStep).slideDown("fast");
                _setupStep($this,curStep,selStep);
            });
        } else if ($this.options.transitionEffect == 'fade'){
            _step($this, curStep).fadeOut("fast",function(e){
                _step($this, selStep).fadeIn("fast");
                _setupStep($this,curStep,selStep);
            });
        } else if ($this.options.transitionEffect == 'slideleft'){
            var nextElmLeft = 0;
            var nextElmLeft1 = null;
            var nextElmLeft = null;
            var curElementLeft = 0;
            if(stepIdx > prevCurStepIdx){
                nextElmLeft1 = $this.elmStepContainer.width() + 10;
                nextElmLeft2 = 0;
                curElementLeft = 0 - _step($this, curStep).outerWidth();
            } else {
                nextElmLeft1 = 0 - _step($this, selStep).outerWidth() + 20;
                nextElmLeft2 = 0;
                curElementLeft = 10 + _step($this, curStep).outerWidth();
            }
            if (stepIdx == prevCurStepIdx) {
                nextElmLeft1 = $($(selStep, $this.target).attr("href"), $this.target).outerWidth() + 20;
                nextElmLeft2 = 0;
                curElementLeft = 0 - $($(curStep, $this.target).attr("href"), $this.target).outerWidth();
            } else {
                $($(curStep, $this.target).attr("href"), $this.target).animate({left:curElementLeft},"fast",function(e){
                    $($(curStep, $this.target).attr("href"), $this.target).hide();
                });
            }

            _step($this, selStep).css("left",nextElmLeft1).show().animate({left:nextElmLeft2},"fast",function(e){
                _setupStep($this,curStep,selStep);
            });
        } else {
            _step($this, curStep).hide();
            _step($this, selStep).show();
            _setupStep($this,curStep,selStep);
        }
        return true;
    };

    var _setupStep = function($this, curStep, selStep) {
        $(curStep, $this.target).removeClass("selected");
        $(curStep, $this.target).addClass("done");

        $(selStep, $this.target).removeClass("disabled");
        $(selStep, $this.target).removeClass("done");
        $(selStep, $this.target).addClass("selected");

        $(selStep, $this.target).attr("isDone",1);

        _adjustButton($this);

        if($.isFunction($this.options.onShowStep)) {
            var context = { fromStep: parseInt($(curStep).attr('rel')), toStep: parseInt($(selStep).attr('rel')) };
            if(! $this.options.onShowStep.call(this,$(selStep),context)){
                return false;
            }
        }
        if ($this.options.noForwardJumping) {
            // +2 == +1 (for index to step num) +1 (for next step)
            for (var i = $this.curStepIdx + 2; i <= $this.steps.length; i++) {
                $this.disableStep(i);
            }
        }
    };

    var _adjustButton = function($this) {
        //console.log($this.options.cycleSteps);
        if (! $this.options.cycleSteps){
            if (0 >= $this.curStepIdx) {
                $(".actionBar .buttonPrevious").addClass("buttonDisabled");
                if ($this.options.hideButtonsOnDisabled) {
                    $($this.buttons.previous).hide();
                }
            }else{
                $(".actionBar .buttonPrevious").removeClass("buttonDisabled");
                if ($this.options.hideButtonsOnDisabled) {
                    $($this.buttons.previous).show();
                }
            }
            if (($this.steps.length-1) <= $this.curStepIdx){
                $(".actionBar .buttonNext").addClass("buttonDisabled");
                if ($this.options.hideButtonsOnDisabled) {
                    $($this.buttons.next).hide();
                }
            }else{
                $(".actionBar .buttonNext").removeClass("buttonDisabled");
                if ($this.options.hideButtonsOnDisabled) {
                    $($this.buttons.next).show();
                }
            }
        }
        // Finish Button
        $this.enableFinish($this.options.enableFinishButton);
    };

    /*
     * Public methods
     */

    SmartWizard.prototype.goForward = function(){

       
        var _this = this;
       
        var curStep = parseInt(this.curStepIdx);
       
        var nextStepIdx = parseInt(this.curStepIdx) + 1;
        
        if (this.steps.length <= nextStepIdx){
           
            if (! this.options.cycleSteps){
               
                return false;
            }
            nextStepIdx = 0;
        }
        
        $(".currStep").attr('value', curStep);
     
        var curStep = $("#step-"+nextStepIdx),          
                    curInputs = curStep.find("input[type='text'],select,input[type='url'],input[type='email'],input[type='number'],input[type='password'],textarea, .req_check"),
                    isValid = true;


        $(".form-group").removeClass("has-error");
            /*
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
            */

            //var curStepBtn = $(this).data("step-id");


            /*
            * This pattern will check only one space 
            */
            var stringPatternOnlyOneSpace = /\s{1,}/g;
            /*
            * This pattern will check more than one space more than one place
            */
            var stringPatternMultipleSpace = /\s{2,}/g;
            /*
            * for valid password
            */
            var minNumberofPass         = 6;
            var maxNumberofPass         = 16;
            var regularExpressionPass   = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
            
            /*
            * for valid phone no.
            */
            var minNumberofPhone        = 6;
            var maxNumberofPhone        = 16;
            //var regularExpressionPhone  = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
            var regularExpressionPhone  = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
            /*
            * for valid email
            */
            var regularExpressionEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

             //var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            /*
            * Zipcode
            */
            var minNumberofZipcode          = 3;
            var maxNumberofZipcode          = 10;
            //var regularExpressionZipcode    = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR ?0AA)$/;
            var regularExpressionZipcode    = /^([a-pr-uwyzA-PR-UWYZ0-9][a-hk-yA-HK-Y0-9][aehmnprtvxyAEHMNPRTVXY0-9]?[abehmnprvwxyABEHMNPRVWXY0-9]? {0,2}[0-9][abd-hjln-uw-zABD-HJLN-UW-Z]{2}|GIR ?0AA)$/;
            /* 
            *  url or link validation http and www
            */
            var stringPatternLinkUrl = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;

            /*
            * insureance patteren
            */
            var insureanceNumberPattern = /^[A-CEGHJ-PR-TV-Y]{1}[A-CEGHJ-NPR-TV-Y]{1}[0-9]{6}[A-Z]{1}/;
            /*
            * insureance patteren
            */
            var numericPattern = /^\d+$/;
            /*
            * fix lenght = 9
            */
            var fixLength = 9;
            /* validation start here 
            *  frist name validation
            */
            var currentInnerStep = $('#myInnerWizard').find('li a.selected').attr('rel');

            /*
            * 
            */
            //alert(currentInnerStep);
            $('input').click(function(){

                $(this).removeClass('input-error');             
                $(this).closest(".form-group").removeClass("has-error");
                $(this).next('span').remove();

            });
            $('select').change(function(){

                $(this).removeClass('input-error');             
                $(this).closest(".form-group").removeClass("has-error");
                $(this).next('span').remove();

            });
            if (currentInnerStep == '1') {

                var UserProfileWillinghours        = $('#UserProfileWillinghours').val();
                var UserProfileWorkInUk            = $('#UserProfileWorkInUk').val();
                var UserProfileFullName            = $('#UserProfileFullName').val();
                var UserProfileRelationship        = $('#UserProfileRelationship').val();
                var UserProfileContactNumber       = $('#UserProfileContactNumber').val();
                var UserProfileHearAbout           = $('#UserProfileHearAbout').val();

                if (UserProfileWillinghours == '') {
                   
                    $('#UserProfileWillinghours').closest(".form-group").addClass("has-error");
                    isValid = false;
                   
                }

                if (UserProfileWillinghours!='' && !numericPattern.test(UserProfileWillinghours)) {

                    
                    $('#UserProfileWillinghours').closest(".form-group").addClass("has-error");
                    $('#UserProfileWillinghours').next('span').remove();
                    $('#UserProfileWillinghours').after('<span class="input-error">Invalid willing hours.</span>');
                    isValid = false;
                    return false;
                }
                /*
                * work in uk
                */
                if (UserProfileWorkInUk == '') {
                   
                    $('#UserProfileWorkInUk').closest(".form-group").addClass("has-error");
                    isValid = false;
                }
                /*
                *  full name
                */
                if (UserProfileFullName == '') {
                   
                    $('#UserProfileFullName').closest(".form-group").addClass("has-error");
                    isValid = false;
                   
                }
                if (UserProfileFullName!='' && UserProfileFullName.length == 1 && UserProfileFullName.match(stringPatternOnlyOneSpace)) {

                    $('#UserProfileFullName').closest(".form-group").addClass("has-error");
                    $('#UserProfileFullName').next('span').remove();
                    $('#UserProfileFullName').after('<span class="input-error">Please enter full name or remove this space.</span>');
                    isValid = false;
                    return false;
                }
                if (UserProfileFullName!='' && UserProfileFullName.length > 1 && UserProfileFullName.match(stringPatternMultipleSpace)) {
                    
                    $('#UserProfileFullName').closest(".form-group").addClass("has-error");
                    $('#UserProfileFullName').next('span').remove();
                    $('#UserProfileFullName').after('<span class="input-error">Only one space is allow in full name.</span>');
                    isValid = false;
                    return false;
                }
                /*
                * UserProfileRelationship
                */
                if (UserProfileRelationship == '') {
                   
                    $('#UserProfileRelationship').closest(".form-group").addClass("has-error");
                    isValid = false;
                   
                }
                if (UserProfileRelationship!='' && UserProfileRelationship.length == 1 && UserProfileRelationship.match(stringPatternOnlyOneSpace)) {

                    
                    $('#UserProfileRelationship').closest(".form-group").addClass("has-error");
                    $('#UserProfileRelationship').next('span').remove();
                    $('#UserProfileRelationship').after('<span class="input-error">Please enter relationship or remove this space.</span>');
                    isValid = false;
                    return false;
                }
                if (UserProfileRelationship!='' && UserProfileRelationship.length > 1 && UserProfileRelationship.match(stringPatternMultipleSpace)) {
                    
                  
                    $('#UserProfileRelationship').closest(".form-group").addClass("has-error");
                    $('#UserProfileRelationship').next('span').remove();
                    $('#UserProfileRelationship').after('<span class="input-error">Only one space is allow in relationship.</span>');
                    isValid = false;
                    return false;
                }
                /*
                * phone number validation
                */
                if (UserProfileContactNumber == '') {
                       
                    $('#UserProfileContactNumber').closest(".form-group").addClass("has-error");
                    isValid = false;
                }
                if (UserProfileContactNumber!='' && UserProfileContactNumber.length == 1 && UserProfileContactNumber.match(stringPatternOnlyOneSpace)) {

                   
                    $('#UserProfileContactNumber').closest(".form-group").addClass("has-error");
                    $('#UserProfileContactNumber').next('span').remove();
                    $('#UserProfileContactNumber').after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                    isValid = false;
                    return false;
                }
                if (UserProfileContactNumber!='' && UserProfileContactNumber.length > 1 && UserProfileContactNumber.match(stringPatternMultipleSpace)) {
                    
                    $('#UserProfileContactNumber').closest(".form-group").addClass("has-error");
                    $('#UserProfileContactNumber').next('span').remove();
                    $('#UserProfileContactNumber').after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                    isValid = false;
                    return false;
                }
                if((UserProfileContactNumber!='' && UserProfileContactNumber.length < minNumberofPhone) || (UserProfileContactNumber!='' && UserProfileContactNumber.length > maxNumberofPhone)){

                    $('#UserProfileContactNumber').closest(".form-group").addClass("has-error");
                    $('#UserProfileContactNumber').next('span').remove();
                    $('#UserProfileContactNumber').after('<span class="input-error">Contact number length should be 6 to 16.</span>');
                    isValid = false;
                    return false;
                }
                if(UserProfileContactNumber!='' && !regularExpressionPhone.test(UserProfileContactNumber)) {
                    
                    $('#UserProfileContactNumber').closest(".form-group").addClass("has-error");
                    $('#UserProfileContactNumber').next('span').remove();
                    $('#UserProfileContactNumber').after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                    isValid = false;
                    return false;
                }
                /*
                * how did you
                */
                 if (UserProfileHearAbout == '') {
                   
                    $('#UserProfileHearAbout').closest(".form-group").addClass("has-error");
                    isValid = false;
                }
            } 
            /*
            * validation for employment
            */
            
            if (currentInnerStep == '2') {				
					
                    var GetFromDate;
                    var GetToDate;
                    $('.emp_hist_block:visible').find('input[type=text], textarea').each(function(){
						
						var emp_hist_data_obj             = $(this);
						
						var emp_hist_data             = $(emp_hist_data_obj).val().trim();
						
						if (!emp_hist_data.length && !emp_hist_data_obj.hasClass('emp-notice') && !emp_hist_data_obj.hasClass('emp-job_desc')) {                               
                            emp_hist_data_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        } 
                        else
                        {
							if(emp_hist_data.length == 1 && emp_hist_data.match(stringPatternMultipleSpace))
							{
								emp_hist_data_obj.closest(".form-group").addClass("has-error");
								emp_hist_data_obj.next('span').remove();
								
								if(emp_hist_data_obj.hasClass('emp-company'))
								{
									emp_hist_data_obj.after('<span class="input-error">Only one space is allow in company name.</span>');
								}
								else if(emp_hist_data_obj.hasClass('emp-address'))
								{
									emp_hist_data_obj.after('<span class="input-error">Only one space is allow in address.</span>');
								}
								else if(emp_hist_data_obj.hasClass('emp-position'))
								{
									emp_hist_data_obj.after('<span class="input-error">Only one space is allow in position.</span>');
								}
								else if(emp_hist_data_obj.hasClass('emp-phone'))
								{
									emp_hist_data_obj.after('<span class="input-error">Please enter telephone number in numeric format only.</span>');
								}
								else
								{
									emp_hist_data_obj.after('<span class="input-error">Only one space is allow.</span>');
								}	
								isValid = false;
								
							}
							
							if(emp_hist_data_obj.hasClass('emp-phone'))
							{ 
								if(emp_hist_data.length < minNumberofPhone || emp_hist_data.length > maxNumberofPhone)
								{
									emp_hist_data_obj.closest(".form-group").addClass("has-error");
									emp_hist_data_obj.next('span').remove();
									emp_hist_data_obj.after('<span class="input-error">Telephone number length should be 6 to 16.</span>');
									
									isValid = false;
									
								}
								if(!regularExpressionPhone.test(emp_hist_data))
								{
									emp_hist_data_obj.closest(".form-group").addClass("has-error");
									emp_hist_data_obj.next('span').remove();
									emp_hist_data_obj.after('<span class="input-error">Please enter telephone number in numeric format only.</span>');
									
									isValid = false;
									
								}
							}
							
							if(emp_hist_data_obj.hasClass('date_from'))
							{
								GetFromDate = emp_hist_data;
							}
							
							if(emp_hist_data_obj.hasClass('date_to'))
							{
								GetToDate = emp_hist_data;
								
								
								/*var partsF = GetFromDate.split('/');
								var dObjF = new Date(partsF[2],partsF[0]-1,partsF[1]);
								
								var partsT = emp_hist_data.split('/');
								var dObjT = new Date(partsT[2],partsT[0]-1,partsT[1]);
								
								console.log(dObjF);
								console.log(dObjT);
																
								//	if(GetFromDate.length > 0 && new Date(GetFromDate).getTime() > new Date(emp_hist_data).getTime())
								
								if(GetFromDate.length > 0 && dObjF > dObjT)
								{
									emp_hist_data_obj.closest(".form-group").addClass("has-error");	
									emp_hist_data_obj.next('span').remove();
									emp_hist_data_obj.after('<span class="input-error">To date must be greater than "From" date.</span>');
								
									isValid = false;									
								}*/
								
							}						
							
							if(emp_hist_data_obj.hasClass('emp-notice') && emp_hist_data.length > 0)
							{
								console.log(typeof emp_hist_data);
								console.log(typeof GetToDate);
								
								var parts = emp_hist_data.split('/');
								var dObjL = new Date(parts[2],parts[0]-1,parts[1]);
								
								var partsT = GetToDate.split('/');
								var dObjT = new Date(partsT[2],partsT[0]-1,partsT[1]);
								
								console.log(dObjL);
								console.log(dObjT);
								
								if(GetFromDate.length > 0 && GetToDate.length > 0 && dObjL < dObjT)
								{
									emp_hist_data_obj.closest(".form-group").addClass("has-error");	
									emp_hist_data_obj.next('span').remove();
									emp_hist_data_obj.after('<span class="input-error">Leaving date or notice must be greater than or equals to "To" date.</span>');
								
									isValid = false;									
								}
							}
							
						}					
					});                               

					$('.reasonForGap:input').each(function()
					{
						var rval = $.trim($(this).val());
						
						if (rval.length > 0){
							// do nothing
						} else {
                            $(this).addClass("has-error");
                            isValid = false;
                        }
                    });
                    
					var invalid_dates = jQuery("#invalid_dates").val(); 
					if(invalid_dates=='1'){
						 isValid = false;
					}
            }
            
            
            /*
            * education validation
            */
            if (currentInnerStep == '3') {
                
                $(".education_title").not('.hide .education_title').each(function(){
                        
                        var education_title_obj         = $(this);

                        var qualification_obtain_obj    = $(this).parents('.row:first').find('.qualification_obt');
                        var year_awarded_obj            = $(this).parents('.row:first').find('.year_awarded');

                        var education_title_val         = education_title_obj.val();
                        var qualification_obtain_val    = qualification_obtain_obj.val();
                        var year_awarded_val            = year_awarded_obj.val();
                        
                       
                        /*
                        *  full title
                        */
                        if (education_title_val == '') {
                           
                            education_title_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                           
                        }
                        if (education_title_val!='' && education_title_val.length == 1 && education_title_val.match(stringPatternOnlyOneSpace)) {

                         
                            education_title_obj.closest(".form-group").addClass("has-error");
                            education_title_obj.next('span').remove();
                            education_title_obj.after('<span class="input-error">Please enter title or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (education_title_val!='' && education_title_val.length > 1 && education_title_val.match(stringPatternMultipleSpace)) {
                            
                           
                           education_title_obj.closest(".form-group").addClass("has-error");
                           education_title_obj.next('span').remove();
                            education_title_obj.after('<span class="input-error">Only one space is allow in title.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        *  full qualification
                        */
                        if (qualification_obtain_val == '') {
                           
                            qualification_obtain_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                           
                        }
                        if (qualification_obtain_val!='' && qualification_obtain_val.length == 1 && qualification_obtain_val.match(stringPatternOnlyOneSpace)) {

                           
                            qualification_obtain_obj.closest(".form-group").addClass("has-error");
                            qualification_obtain_obj.next('span').remove();
                            qualification_obtain_obj.after('<span class="input-error">Please enter qualification or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (qualification_obtain_val!='' && qualification_obtain_val.length > 1 && qualification_obtain_val.match(stringPatternMultipleSpace)) {
                            
                           
                            qualification_obtain_obj.closest(".form-group").addClass("has-error");
                            qualification_obtain_obj.next('span').remove();
                            qualification_obtain_obj.after('<span class="input-error">Only one space is allow in qualification.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        *  year awarded
                        */
                        if (year_awarded_val == '') {
                           
                            year_awarded_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                           
                        }                        
                        /*
                        * education digree validation end
                        */
                    });
                    /*
                    * validation on referees
                    */
                     var kk=0;
                    $('.u_r_b').not('.ref3').each(function(){

                        
                        var user_reference_name_obj             = $(this).find('.user_reference_name');

                        var user_reference_company_obj          = $(this).find('.user_reference_company');
                        var user_reference_job_title_obj        = $(this).find('.user_reference_job_title');
                        var user_reference_email_obj            = $(this).find('.user_reference_email');
                        var user_reference_address_obj          = $(this).find('.user_reference_address');
                        var user_reference_address2_obj         = $(this).find('.user_reference_address2');
                        var user_reference_town_obj             = $(this).find('.user_reference_town');
                        var user_reference_brough_obj           = $(this).find('.user_reference_brough');
                        var user_reference_contact_number_obj   = $(this).find('.user_reference_contact_number');
                        var user_reference_postcode_obj         = $(this).find('.user_reference_postcode');
                         var user_ok_contact_obj                = $(this).find('.ok_contact');

                        var user_reference_name_val             = user_reference_name_obj.val();
                        var user_reference_company_val            = user_reference_company_obj.val();
                        var user_reference_job_title_val        = user_reference_job_title_obj.val();
                        var user_reference_email_val            = user_reference_email_obj.val();
                        var user_reference_address_val          = user_reference_address_obj.val();
                        var user_reference_address2_val         = user_reference_address2_obj.val();
                        var user_reference_town_val             = user_reference_town_obj.val();
                        var user_reference_brough_val           = user_reference_brough_obj.val();
                        var user_reference_contact_number_val   = user_reference_contact_number_obj.val();
                        var user_reference_postcode_val         = user_reference_postcode_obj.val();
                        var user_ok_contact_val                 = user_ok_contact_obj.prop('checked');
                       // alert(user_ok_contact_val);

                        /*
                        * check first name
                        */
                        

                         $('.u_r_b').not('.ref3').not(this).each(function(){

                                if (user_reference_email_obj.val()!='' && $(this).find('.user_reference_email').val()!='' && user_reference_email_obj.val() == $(this).find('.user_reference_email').val()) {

                                    $(this).find('.user_reference_email').closest(".form-group").addClass("has-error");
                                    $(this).find('.user_reference_email').next().hide();
                                    $(this).next('span').remove();
                                    $(this).find('.user_reference_email').after('<span class="sameElementError">Email is same in both record please change it.</span>');
                                   isValid = false;
                                }

                                if (user_reference_contact_number_obj.val()!='' && $(this).find('.user_reference_contact_number').val() !='' && user_reference_contact_number_obj.val() == $(this).find('.user_reference_contact_number').val()) {

                                   $(this).find('.user_reference_contact_number').closest(".form-group").addClass("has-error");
                                   $(this).find('.user_reference_contact_number').next().hide();
                                   $(this).next('span').remove();
                                    $(this).find('.user_reference_contact_number').after('<span class="sameElementError">Contact number is same in both record please change it.</span>');
                                   isValid = false;
                                }else{
                                    $(this).find('.user_reference_contact_number').next('span').hide();
                                }

                                 //console.log($(this).find('.ok_contact:checked'));
                                 //console.log($(this).find('.ok_contact[value=Yes]').is(':checked'));
                                 //console.log($(this).find('.ok_contact[value=No]').is(':checked'));
                                 //console.log( $(this).find('.ok_contact:checked').val());

                                if ($(this).find('.ok_contact[value=No]').is(':checked')) {
                                    ++kk;
                                }

                                  
                                if( kk == $('.ok_contact[value=No]').not('.ref3 .ok_contact[value=No]').length){

                                     $(this).find('.ok_contact').closest(".form-group").addClass("has-error");  
                                    
                                    alert('You should choose Ok to contact as “Yes” for atleast one of the Reference to proceed.');                                    
                                       isValid = false;
                                }

                        });
						
						if(user_ok_contact_val == false)
						{
							if($(this).find('.ok_contact[value=No]').is(':checked'))
							{
								//alert('no');
							}
							else
							{
								isValid = false;
								$(this).find('.ok_contact').closest(".form-group").addClass("has-error");  
							}	
							
							
                                    
                                    //alert('Please confirm, Ok to contact?');    
							
						}
						
                        if (user_reference_name_val == '') {
                               
                            user_reference_name_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                           
                        }
                        if (user_reference_name_val!='' && user_reference_name_val.length == 1 && user_reference_name_val.match(stringPatternOnlyOneSpace)) {

                            user_reference_name_obj.closest(".form-group").addClass("has-error");
                            user_reference_name_obj.next('span').remove();
                            user_reference_name_obj.after('<span class="input-error">Please enter name or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_name_val!='' && user_reference_name_val.length > 1 && user_reference_name_val.match(stringPatternMultipleSpace)) {
                           
                            user_reference_name_obj.closest(".form-group").addClass("has-error");
                            user_reference_name_obj.next('span').remove();
                            user_reference_name_obj.after('<span class="input-error">Only one space is allow in name.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * qualification
                        */
                        if (user_reference_company_val == '') {
                               
                            user_reference_company_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        if (user_reference_company_val!='' && user_reference_company_val.length == 1 && user_reference_company_val.match(stringPatternOnlyOneSpace)) {

                          
                            user_reference_company_obj.closest(".form-group").addClass("has-error");
                            user_reference_company_obj.next('span').remove();
                            user_reference_company_obj.after('<span class="input-error">Please enter company name or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_company_val!='' && user_reference_company_val.length > 1 && user_reference_company_val.match(stringPatternMultipleSpace)) {
                            
                           
                            user_reference_company_obj.closest(".form-group").addClass("has-error");
                            user_reference_company_obj.next('span').remove();
                            user_reference_company_obj.after('<span class="input-error">Only one space is allow in company name.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        *  title
                        */
                        if (user_reference_job_title_val == '') {
                               
                            user_reference_job_title_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        if (user_reference_job_title_val!='' && user_reference_job_title_val.length == 1 && user_reference_job_title_val.match(stringPatternOnlyOneSpace)) {

                           
                            user_reference_job_title_obj.closest(".form-group").addClass("has-error");
                            user_reference_job_title_obj.next('span').remove();
                           user_reference_job_title_obj.after('<span class="input-error">Please enter job title or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_job_title_val!='' && user_reference_job_title_val.length > 1 && user_reference_job_title_val.match(stringPatternMultipleSpace)) {
                            
                            
                            user_reference_job_title_obj.closest(".form-group").addClass("has-error");
                            user_reference_job_title_obj.next('span').remove();
                            user_reference_job_title_obj.after('<span class="input-error">Only one space is allow in job title.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * email
                        */
                        if (user_reference_email_val == '') {
                               
                           user_reference_email_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        if (user_reference_email_val!='' && user_reference_email_val.length == 1 && user_reference_email_val.match(stringPatternOnlyOneSpace)) {
                            
                            user_reference_email_obj.closest(".form-group").addClass("has-error");
                            user_reference_email_obj.next('span').remove();
                            user_reference_email_obj.after('<span class="input-error">Please enter email or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_email_val!='' && user_reference_email_val.length > 1 && user_reference_email_val.match(stringPatternMultipleSpace)) {
                            
                            
                            user_reference_email_obj.closest(".form-group").addClass("has-error");
                            user_reference_email_obj.next('span').remove();
                            user_reference_email_obj.after('<span class="input-error">Only one space is allow in email.</span>');
                            isValid = false;
                            return false;
                        }
                        if(user_reference_email_val!='' && !regularExpressionEmail.test(user_reference_email_val)) {
                           
                            user_reference_email_obj.closest(".form-group").addClass("has-error");
                            user_reference_email_obj.next('span').remove();
                            user_reference_email_obj.after('<span class="input-error">Enter valid email.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * address
                        */
                        if (user_reference_address_val == '') {
                               
                           user_reference_address_obj.closest(".form-group").addClass("has-error");
                        }
                        if (user_reference_address_val!='' && user_reference_address_val.length == 1 && user_reference_address_val.match(stringPatternOnlyOneSpace)) {

                           
                            user_reference_address_obj.closest(".form-group").addClass("has-error");
                            user_reference_address_obj.next('span').remove();
                            user_reference_address_obj.after('<span class="input-error">Please enter address or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_address_val!='' && user_reference_address_val.length > 1 && user_reference_address_val.match(stringPatternMultipleSpace)) {
                            
                            
                            user_reference_address_obj.closest(".form-group").addClass("has-error");
                            user_reference_address_obj.next('span').remove();
                            user_reference_address_obj.after('<span class="input-error">Only one space is allow in address.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * addre3ss two
                        */
                        if (user_reference_address2_val == '') {
                               
                          user_reference_address2_obj.closest(".form-group").addClass("has-error");
                          isValid = false;
                        }
                        if (user_reference_address2_val!='' && user_reference_address2_val.length == 1 && user_reference_address2_val.match(stringPatternOnlyOneSpace)) {

                            user_reference_address2_obj.next('span').remove();
                            user_reference_address2_obj.closest(".form-group").addClass("has-error");
                            user_reference_address2_obj.after('<span class="input-error">Please enter second line address 2 or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_address2_val!='' && user_reference_address2_val.length > 1 && user_reference_address2_val.match(stringPatternMultipleSpace)) {
                            
                           
                            user_reference_address2_obj.closest(".form-group").addClass("has-error");
                            user_reference_address2_obj.next('span').remove();
                            user_reference_address2_obj.after('<span class="input-error">Only one space is allow in second line address 2</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * town
                        */

                        if (user_reference_town_val == '') {
                               
                           user_reference_town_obj.closest(".form-group").addClass("has-error");
                           isValid = false;
                        }
                        if (user_reference_town_val!='' && user_reference_town_val.length == 1 && user_reference_town_val.match(stringPatternOnlyOneSpace)) {
                            
                            user_reference_town_obj.closest(".form-group").addClass("has-error");
                            user_reference_town_obj.next('span').remove();
                            user_reference_town_obj.after('<span class="input-error">Please enter town or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_town_val!='' && user_reference_town_val.length > 1 && user_reference_town_val.match(stringPatternMultipleSpace)) {
                           
                            user_reference_town_obj.closest(".form-group").addClass("has-error");
                            user_reference_town_obj.next('span').remove();
                            user_reference_town_obj.after('<span class="input-error">Only one space is allow in town.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * brought
                        */
                        if (user_reference_brough_val == '') {
                               
                            user_reference_brough_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        /*
                        * phone number validation
                        */
                        if (user_reference_contact_number_val == '') {
                               
                            user_reference_contact_number_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        if (user_reference_contact_number_val!='' && user_reference_contact_number_val.length == 1 && user_reference_contact_number_val.match(stringPatternOnlyOneSpace)) {

                           
                            user_reference_contact_number_obj.closest(".form-group").addClass("has-error");
                            user_reference_contact_number_obj.next('span').remove();
                            user_reference_contact_number_obj.after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_contact_number_val!='' && user_reference_contact_number_val.length > 1 && user_reference_contact_number_val.match(stringPatternMultipleSpace)) {
                            
                           
                            user_reference_contact_number_obj.closest(".form-group").addClass("has-error");
                            user_reference_contact_number_obj.next('span').remove();
                            user_reference_contact_number_obj.after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                            isValid = false;
                            return false;
                        }
                        if((user_reference_contact_number_val!='' && user_reference_contact_number_val.length < minNumberofPhone) || (user_reference_contact_number_val!='' && user_reference_contact_number_val.length > maxNumberofPhone)){

                            user_reference_contact_number_obj.closest(".form-group").addClass("has-error");
                            user_reference_contact_number_obj.next('span').remove();
                            user_reference_contact_number_obj.after('<span class="input-error">Contact length should be 6 to 16.</span>');
                            isValid = false;
                            return false;
                        }
                        if(user_reference_contact_number_val!='' && !regularExpressionPhone.test(user_reference_contact_number_val)) {
                            
                            user_reference_contact_number_obj.closest(".form-group").addClass("has-error");
                            user_reference_contact_number_obj.next('span').remove();
                            user_reference_contact_number_obj.after('<span class="input-error">Please enter contact number in numeric format only.</span>');
                            isValid = false;
                            return false;
                        }
                        /*
                        * zipcode validation
                        */
                        if (user_reference_postcode_val == '') {
                               
                            user_reference_postcode_obj.closest(".form-group").addClass("has-error");
                            isValid = false;
                        }
                        if (user_reference_postcode_val!='' && user_reference_postcode_val.length == 1 && user_reference_postcode_val.match(stringPatternOnlyOneSpace)) {

                            
                            user_reference_postcode_obj.closest(".form-group").addClass("has-error");
                            user_reference_postcode_obj.next('span').remove();
                           user_reference_postcode_obj.after('<span class="input-error">Please enter postcode or remove this space.</span>');
                            isValid = false;
                            return false;
                        }
                        if (user_reference_postcode_val!='' && user_reference_postcode_val.length > 1 && user_reference_postcode_val.match(stringPatternMultipleSpace)) {
                            
                            
                            user_reference_postcode_obj.closest(".form-group").addClass("has-error");
                            user_reference_postcode_obj.next('span').remove();
                            user_reference_postcode_obj.after('<span class="input-error">Only one space is allow in postcode.</span>');
                            isValid = false;
                            return false;
                        }
                        if((user_reference_postcode_val!='' && user_reference_postcode_val.length < minNumberofZipcode) || (user_reference_postcode_val!='' && user_reference_postcode_val.length > maxNumberofZipcode)){

                          
                            user_reference_postcode_obj.closest(".form-group").addClass("has-error");
                            user_reference_postcode_obj.next('span').remove();
                            user_reference_postcode_obj.after('<span class="input-error">Postcode length should be 3 to 10.</span>');
                            isValid = false;
                            return false;
                        }
                        if(user_reference_postcode_val!='' && !regularExpressionZipcode.test(user_reference_postcode_val)) {
                    
                            user_reference_postcode_obj.closest(".form-group").addClass("has-error");
                            user_reference_postcode_obj.next('span').remove();
                            user_reference_postcode_obj.after('<span class="input-error">Please enter postcode in valid format.</span>');
                            isValid = false;
                            return false;
                        }

                    });

                
            }


            //isValid = false;
            if (isValid) {
                 $.ajax({
                        type: "POST",
                        //url: "add",
                         url: "",
                        data: $('#form-application').serialize(),
                        beforeSend: function () {
                           // _this.loader.show();
                            $(".overlay").show();
                            $('.loader').show();
                        },
                        success: function (response) {
                            
                             //_this.loader.hide();     
                             $('.loader').hide();
                            $(".overlay").hide();
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                           // alert(this);
                          _loadContent(_this, nextStepIdx);
                        },
                        error: function () {
                            alert('Application not submitted. Please try later');
                        }
                    });
                
            } 
     
        //   _loadContent(this, nextStepIdx);
       
    };

    SmartWizard.prototype.goBackward = function(){
        var nextStepIdx = this.curStepIdx-1;
        if (0 > nextStepIdx){
            if (! this.options.cycleSteps){
                return false;
            }
            nextStepIdx = this.steps.length - 1;
        }
        
       
        _loadContent(this, nextStepIdx);
    };

    SmartWizard.prototype.goToStep = function(stepNum){
        var stepIdx = stepNum - 1;
        if (stepIdx >= 0 && stepIdx < this.steps.length) {
            _loadContent(this, stepIdx);
        }
    };
    SmartWizard.prototype.enableStep = function(stepNum) {
        var stepIdx = stepNum - 1;
        if (stepIdx == this.curStepIdx || stepIdx < 0 || stepIdx >= this.steps.length) {
            return false;
        }
        var step = this.steps.eq(stepIdx);
        $(step, this.target).attr("isDone",1);
        $(step, this.target).removeClass("disabled").removeClass("selected").addClass("done");
    }
    SmartWizard.prototype.disableStep = function(stepNum) {
        var stepIdx = stepNum - 1;
        if (stepIdx == this.curStepIdx || stepIdx < 0 || stepIdx >= this.steps.length) {
            return false;
        }
        var step = this.steps.eq(stepIdx);
        $(step, this.target).attr("isDone",0);
        $(step, this.target).removeClass("done").removeClass("selected").addClass("disabled");
    }
    SmartWizard.prototype.currentStep = function() {
        return this.curStepIdx + 1;
    }

    SmartWizard.prototype.showMessage = function (msg) {
        $('.content', this.msgBox).html(msg);
        this.msgBox.show();
    }

    SmartWizard.prototype.enableFinish = function (enable) {
        // Controll status of finish button dynamically
        // just call this with status you want
        this.options.enableFinishButton = enable;
        if (this.options.includeFinishButton){ 
			console.log("Finish:",!this.steps.hasClass('disabled'),this.options.enableFinishButton);
            if (!this.steps.hasClass('disabled') || this.options.enableFinishButton){
				if ((this.steps.length-1) === this.curStepIdx){
					$(".actionBar .buttonFinish").removeClass("buttonDisabled");
				}else{
					$(".actionBar .buttonFinish").addClass("buttonDisabled");					
				}
                if (this.options.hideButtonsOnDisabled) {
                    $(".actionBar .buttonFinish").show();
                }
            }else{
                $(".actionBar .buttonFinish").addClass("buttonDisabled");
                if (this.options.hideButtonsOnDisabled) {
                    $(".actionBar .buttonFinish").hide();
                }
            }
        }
        return this.options.enableFinishButton;
    }




    SmartWizard.prototype.hideMessage = function () {
        this.msgBox.fadeOut("normal");
    }
    SmartWizard.prototype.showError = function(stepnum) {
        this.setError(stepnum, true);
    }
    SmartWizard.prototype.hideError = function(stepnum) {
        this.setError(stepnum, false);
    }
    SmartWizard.prototype.setError = function(stepnum,iserror) {
        if (typeof stepnum == "object") {
            iserror = stepnum.iserror;
            stepnum = stepnum.stepnum;
        }

        if (iserror){
            $(this.steps.eq(stepnum-1), this.target).addClass('error')
        }else{
            $(this.steps.eq(stepnum-1), this.target).removeClass("error");
        }
    }
    SmartWizard.prototype.convertToDate = function(dateString,format){
        if(typeof dateString === 'string'){
            var _this = this;
            if(!format) format = _this.format;
            ddIndex = format.indexOf('dd');
            mmIndex = format.indexOf('mm');
            yyIndex = format.indexOf('yyyy'); 
            y = dateString.substr(yyIndex,4);
            m = dateString.substr(mmIndex,2)-1;
            d = dateString.substr(ddIndex,2);
        }else{
            y = 0;
            m = 0;
            d = 0;
        }
        return new Date(y,m,d);
    }
    SmartWizard.prototype.fixHeight = function(){
        var height = 0;

        var selStep = this.steps.eq(this.curStepIdx);
        var stepContainer = _step(this, selStep);
        stepContainer.children().each(function() {
            if($(this).is(':visible')) {
                 height += $(this).outerHeight(true);
            }
        });

        // These values (5 and 20) are experimentally chosen.
        //stepContainer.height(height + 5);
        //this.elmStepContainer.height(height + 20);
    }

    _init(this);
};



(function($){

    $.fn.smartWizard = function(method) {
        var args = arguments;
        var rv = undefined;
        var allObjs = this.each(function() {
            var wiz = $(this).data('smartWizard');
            if (typeof method == 'object' || ! method || ! wiz) {
                var options = $.extend({}, $.fn.smartWizard.defaults, method || {});
                if (! wiz) {
                    wiz = new SmartWizard($(this), options);
                    $(this).data('smartWizard', wiz);
                }
            } else {
                if (typeof SmartWizard.prototype[method] == "function") {
                    rv = SmartWizard.prototype[method].apply(wiz, Array.prototype.slice.call(args, 1));
                    return rv;
                } else {
                    $.error('Method ' + method + ' does not exist on jQuery.smartWizard');
                }
            }
        });
        if (rv === undefined) {
            return allObjs;
        } else {
            return rv;
        }
    };

// Default Properties and Events
    $.fn.smartWizard.defaults = {
        selected: 0,  // Selected Step, 0 = first step
        keyNavigation: false, // Enable/Disable key navigation(left and right keys are used if enabled)
        enableAllSteps: false,
        disabledStepNavigationClick: true,
        transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
        contentURL:null, // content url, Enables Ajax content loading
        contentCache:false, // cache step contents, if false content is fetched always from ajax url
        cycleSteps: false, // cycle step navigation
        enableFinishButton: false, // make finish button enabled always
        hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead?
        errorSteps:[],    // Array Steps with errors
        labelNext:'Next',
        labelPrevious:'Previous',
        labelFinish:'Finish',
        noForwardJumping: false,
        ajaxType: "POST",
        onLeaveStep: null, // triggers when leaving a step
        onShowStep: null,  // triggers when showing a step
        onFinish: null,  // triggers when Finish button is clicked
        includeFinishButton : true   // Add the finish button
    };

})(jQuery);

