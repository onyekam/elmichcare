var dateDifferences = function(settings){
	var _this = this;
	var defaults = {
		element:'',
		outputElement:'',
		tmplElement:'',
		useDatepicker:true,
		startFrom:new Date(),
		range:[5,0,0],
		format:"dd/mm/yyyy",
		monthNames:["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
]
	};
	var o = _this.extend(defaults,settings);
	_this.el = o.element;
	_this.output = o.outputElement;
	_this.tmpl = o.tmplElement;
	_this.format = o.format;
	_this.useDatepicker = o.useDatepicker;
	_this.dates = new Array();	
	_this.differences = new Array();	
	_this.startFrom = o.startFrom;		
	_this.invalid = false;	
	_this.range = o.range;
	_this.lastDate = new Date(
			_this.startFrom.getFullYear()-this.range[0],
			_this.startFrom.getMonth()-this.range[1],
			_this.startFrom.getDate()-this.range[2]
	);
	_this.monthNames = o.monthNames;
	_this.init();
}
dateDifferences.prototype.init = function(){
	var _this = this; 
}
dateDifferences.prototype.convertToDate = function(dateString,format){
	if(typeof dateString === 'string'){
		var _this = this;
		if(!format) format = _this.format;
		ddIndex = format.indexOf('dd');
		mmIndex = format.indexOf('mm');
		yyIndex = format.indexOf('yyyy'); 
		y = dateString.substr(yyIndex,4);
		m = dateString.substr(mmIndex,2)-1;
		d = dateString.substr(ddIndex,2);
	}else{
		y = 0;
		m = 0;
		d = 0;
	}
	return new Date(y,m,d);
}
dateDifferences.prototype.getDateDiff = function(endDate,startDate){
	var _this = this;
	
	var end = _this.convertToDate(endDate);
	var start = _this.convertToDate(startDate);
	
	start.setDate(start.getDate()+1);

	var diff_year   = Math.floor((end - start)/31536000000);
	var diff_month   = Math.floor(((end - start)% 31536000000)/2628000000);
	var diff_day   = Math.floor((((end - start)% 31536000000) % 2628000000)/86400000);
	var days = (end - start) / (1000 * 60 * 60 * 24);
	var formatedStartDate = _this.formatDate(start);	
	end.setDate(end.getDate()-1);
	var formatedEndDate = _this.formatDate(end);
	switch(days){
		case 0:
			formatedStartDate='';
			formatedEndDate='';
		break;
		case 1:
			formatedStartDate=formatedEndDate;
			formatedEndDate=formatedEndDate;
		break;
		default:
		break;
	}
	var difference = {
		'start':formatedStartDate,
		'end':formatedEndDate,
		'years':diff_year,
		'months':diff_month,
		'days':diff_day,
		'totalMonths':(diff_year*12+diff_month),
		'totalDays':days,
		'startMonth':_this.monthNames[start.getMonth()],
		'startYear':start.getFullYear(),
		'endMonth':_this.monthNames[end.getMonth()],
		'endYear':end.getFullYear(),
	}
	if(days<0){
		//_this.invalid = true;
	}
	return difference;
}
dateDifferences.prototype.formatDate = function(date,format){
	var _this = this;
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        yr = d.getYear();
        year = d.getFullYear(),
		dt;
		
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
	
	if(!format) format = _this.format;
	var dt = format;
	
	dt = dt.replace('dd',day);
	dt = dt.replace('mm',month);
	dt = dt.replace('yyyy',year);
	dt = dt.replace('yy',yr);
	
    return dt;
}
dateDifferences.prototype.getResults = function(){
	var _this = this;
	return _this.differences;
}
dateDifferences.prototype.printResults = function(output){
	var _this = this;
	var message = '';
	for(i=0;i<_this.differences.length-1;i++){	
		console.log("Difference:",_this.differences[i],"\n");
		message += "Year: "+ _this.differences[i]['years'] + ", Month: "+ _this.differences[i]['months'] + ", days: "+ _this.differences[i]['days']+"\n";
	}
	
	if($(output).length){
		$(output).html(message);
	}
}
dateDifferences.prototype.update = function(){
	var _this = this;
	var diff_year, diff_month, diff_day;
	var lastdate = _this.lastDate;	
	var elements = $(_this.el); 
	
	_this.messages = new Array();
	_this.differences = new Array();
	_this.dates = new Array();
	_this.invalid = false;
	_this.dates.push(_this.formatDate(_this.startFrom));
	
	$(_this.output).empty();
	elements.each(function(index, element) {
		
		var end = $(".date_to",this).val();
		var start = $(".date_from",this).val();
		var last = _this.formatDate(_this.lastDate);
		var startDate = _this.convertToDate(start);		
		var endDate = _this.convertToDate(end);
		if(
			startDate 
			&& endDate 
		){
			if(endDate < startDate){
				start = end;
				_this.invalid = true;
			}
			if(startDate < _this.lastDate){
				start = last;			
			}
			if(endDate < _this.lastDate){
				end = last;
			}
		}
		_this.dates.push(end,start);
	});	
	_this.dates.push(_this.formatDate(_this.lastDate));
	var count = Number(Math.ceil(_this.dates.length));
	var counter = 0;
	var dt = false;
	if(!isNaN(count) && count>2){
		var dates = _this.dates;
		var differences = _this.getDifferences();
		console.log("------------------------------------");
		console.log("Dates",dates);
		console.log("Differences",differences);
		console.log("------------------------------------");
		//_this.dates,differences,_this.invalid);
		var diffCounter = 0;
		var diffCount = Number(Math.ceil(_this.differences.length));
		var invalid = _this.checkDates();
		if(_this.invalid){
			$('<span class="gap_error">Selected dates are invalid. Please check your employment dates.</span>').appendTo(_this.output);	$("#invalid_dates").val('1');
		}else{
			$("#invalid_dates").val('0');
			for(j=0;j<diffCount;j+=1){	
				difference = _this.differences[j];
				if(_this.validate(difference)){
					$(_this.tmpl).tmpl({
							'index':diffCounter,
							'start':difference['start'],
							'end':difference['end'],
							'startMonth':difference['startMonth'],
							'startYear':difference['startYear'],
							'endMonth':difference['endMonth'],
							'endYear':difference['endYear'],
						}).appendTo(_this.output);	
						diffCounter+=1;
				}
			}
		}
	}
}
dateDifferences.prototype.checkDates = function(){
	var _this = this;
	var dates = _this.dates;
	var invalid=false;
	var olddate=_this.convertToDate(dates[0]);
	for(i=1;i<dates.length;i++){ 
		if(olddate && (olddate<_this.convertToDate(dates[i]))){
			invalid = true;
			break;
		}
		olddate = _this.convertToDate(dates[i]);
	}
	if(!_this.invalid && invalid){
		_this.invalid = invalid; 
	}
	return invalid;
}
dateDifferences.prototype.getDifferences = function(){
	var _this = this;
	var dates = _this.dates;
	var differences = [];
	for(i=0;i<dates.length;i+=2){	
		var difference = _this.getDateDiff(dates[i],dates[i+1]);
		differences.push(difference);
		if(_this.validate(difference)){
			var dateStart = difference['start'];
			var dateEnd = difference['end'];  
		}
	} 
	_this.differences = differences;
	return differences;
}
dateDifferences.prototype.validate = function(difference){
	var _this = this;
	if (_this.onValidate && typeof(_this.onValidate) === "function") {
    	return _this.onValidate.call(this,difference);
	}
	return true;
}
dateDifferences.prototype.extend = function(obj, props) {
    for(var prop in props) {
        if(props.hasOwnProperty(prop)) {
            obj[prop] = props[prop];
        }
    }
	return obj;
};
