<?php 
	session_start();
	$userMail = "";
	if(isset($_SESSION['userMail']))
		$userMail = $_SESSION['userMail'];
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta name="title" content="Care Recruitment">
<meta name="keywords" content="care recruitment"/>
<meta name="description" content="Care People" />
<meta name="author" content="">

		<meta name="author" content="">
		<meta name="format-detection" content="telephone=no">
		<title>ELMICH Care</title>

		<link rel="shortcut icon" href="files/fav.png"/>
	<link rel="stylesheet" type="text/css" href="filesA/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="filesA/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="filesA/style.css"/>
	<link rel="stylesheet" type="text/css" href="filesA/responsive.css"/>
	<link rel="stylesheet" type="text/css" href="filesA/AdminLTE.min.css"/>
<!-- Font Awesome -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- iCheck -->    
<link rel="stylesheet" href="filesA/blue.css">    
 <!-- For application form css -->  
 
	<script type="text/javascript" src="filesA/jQuery-2.1.4.min.js"></script>
	<script type="text/javascript" src="filesA/bootstrap.min.js"></script>
   
</head> 
<body>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container"> 
					<div class="navbar-header page-scroll">
						<button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button"> 
							<span class="sr-only">Toggle navigation</span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand logo">
							<img src="files/logodaryel.png" alt="logo"/>
						</a>      
					</div>
					<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">	  
						<ul class="login-register pull-right" style="width:auto;">
							<li class="page-scroll" style="margin-right: 20px"> 
								<h4 style="color: #006a77;">Call 02084347010</h4> 
							</li>     
							<li  class=""><a href="#" style="background:#006a77">Home</a></li>
							<li id="login1" class="login"><a href="signin.php">Sign In</a></li>
							<li id="login2" style="display:none;" class="login">
								<div class="btn-group">
									<a href="#" class="btn btn-default">My Account</a>							  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">						
										<li><a href="signout.php">Sign Out</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
<!--main section here-->
<section class="main-section">
    
<section class="page-section main-content" >
    <div class="container">
	    <div class="blue_hadaing row">
            <div class="col-md-6">
                <h2><a href="/users/myaccount" class="myaccount_anchor">My Account</a></h2>

            </div>
			<div class=" uploadsbuttons col-md-6">
                

            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12"><div class="alert alert-success alert-dismissible fade in" id="successFlashMsg">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Success!</strong> Your application has been submitted successfully. Ensure to bring the original document if invited for an interview.</div></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <span class="welcome">Welcome </span><span class="person">Applicant</span>
            </div>
        </div><br><br><br><br><br><br><br><br><br><br><br>
        <div class="row hide">
            <div class=" text-center action-btns-account">                            
                <div class="col-md-3">
                <a href="/jobApplications/edit/Mzk4NzIxODU3LjE2" class="btn btn-primary "><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Application</a>            </div>

                <div class="col-md-3">
                <a href="/users/documents" class="btn btn-primary"><i class="fa fa-file" aria-hidden="true"></i>
 Documents List</a>  </div>

                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="1" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Passport/Work Permit</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="2" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>First proof of address</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="3" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Second proof of address</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="4" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Qualifications</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="5" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>DBS Certificate</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="6" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Photo ID</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-danger uploadDoc" data-id="7" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Time Sheets</button>
			                </div>
						
                                			<div class="col-md-3">
			                	<button class="btn btn-warning uploadDoc" data-id="8" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Daily Log</button>
							</div>
							<div class="col-md-3">
			                	<button class="btn btn-warning uploadDoc" data-id="8" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Payslips</button>
							</div>
							<div class="col-md-3">
			                	<button class="btn btn-warning uploadDoc" data-id="8" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Supervision</button>
							</div>
							<div class="col-md-3">
			                	<button class="btn btn-warning uploadDoc" data-id="8" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i>Appraisal form</button>
			                </div>
						
                                	
                	<!-- <div class="col-md-3">
			                	<button class="btn btn-success uploadDoc" data-id="1" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add passport/work permit</button>
			                </div>
                
                <div class="col-md-3">
                <button class="btn btn-success uploadDoc" data-id="2" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add first proof of address</button>
                </div>

                 <div class="col-md-3">   
                <button class="btn btn-success uploadDoc" data-id="3" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add second proof of address</button>

                </div>

                 <div class="col-md-3">   
                <button class="btn btn-success uploadDoc" data-id="4" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add qualifications</button>
                </div>

                <div class="col-md-3">
                <button class="btn btn-success uploadDoc" data-id="5" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add DBS certificate</button>
                </div>


                <div class="col-md-3">
                <button class="btn btn-success uploadDoc" data-id="6" data-toggle="modal" data-target="#myModalUploadDoc"><i class="fa fa-upload" aria-hidden="true"></i> Add Picture for Photo ID</button>
                </div> -->
            </div>
        </div>    
            
    </div>
 <!-- Modal -->
<div id="myModalUploadDoc" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <form action="/users/upload_documents" controller="users" enctype="multipart/form-data" id="UserUploadDocumentsForm" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Documents</h4>
        </div>
        <div class="modal-body">
            <div class=" edu_row">                                      
            <div class="upload_add_more_doc row">                
                <div class="form-gorup col-md-6">
                    <input type="hidden" name="data[UserDoc][user_id]" value="731" id="UserDocUserId"/><input type="hidden" name="data[UserDoc][is_active]" value="1" id="UserDocIsActive"/><input type="hidden" name="data[UserDoc][is_deleted]" value="0" id="UserDocIsDeleted"/><input type="hidden" name="data[UserDoc][doc_type_id]" value="1" id="UserDocDocTypeId"/><div class="input file"><label for="UserDocDocName">User Document</label><input type="file" name="data[UserDoc][doc_name][]" required="required" class="btn btn-default" id="UserDocDocName"/></div>                </div>
                <div class="form-gorup col-md-6 innerHtml">

                </div>
            </div>
            <div class="" id="afterThis">
                <div class="upload_add_more_button">                                  
                    <a class="btn btn-warning btn-flat" href="javascript:void(0);" id="edu_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Documents</a>
                </div>                                                                    
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> btn-primary-->
        <input type="submit" name="submit" id="submit" class="btn disabled" value="Submit"  />
      </div>
      </form>    </div>
  </div>
</div>

<div id="more_edu" class="hide ">
    <div class="edu_row addedMoreDiv" style="margin-bottom: 12px;">
        <hr/>                                
        <div class="upload_add_more_doc">           
            <div class="input file"><input type="file" name="data[UserDoc][doc_name][]" class="btn btn-default" required="required" id="UserDocDocName"/></div>   
             <img src="https://daryelcare.com/images/close.png" style="height: 24px; width: 24px; cursor: pointer;" class="edu_remove " title="Remove Qualification">                                                       
        </div>
    </div>
</div> 

<script>
	$(function(){
		var userMail = "<?=$userMail?>";
		if(userMail != ""){
			$('#login1').hide();
			$('#login2').show();
		}
	});
</script>

			
			
			<footer class="footer">
				<div class="footer-secoandry" style="background:#006a77">
				  <div class="container">
					<div class="logo-fotter pull-right"> 
						<a href="#"><img src="files/logodaryel.png" alt="logo"/></a>        </div>
					<div class="pull-left copyright"> © 2017 
					   <a href="#">elmichcare.com</a> |
					   <a href="#">Privacy</a> | 
					   <a href="#">Terms </a> </div>
				  </div>
				</div>
			  </footer>   
</section>
<!--footer here --> 

</body>
</html>