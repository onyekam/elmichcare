<script>
	$(document).ready(function(){
		var step = "<?= $step?>";
		$("#step-form-1").hide();
		$("#step-form-2").hide();
		$("#step-form-3").hide();
		if(step == "NONE"){
			$("#step-form-1").show();
		}else if(step == "stepForm2"){
			$('#circleBtn3').addClass("btn-warning");
			$('#circleBtn3').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn2').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn2').removeClass("btn-warning");
			$("#step-1").hide();
			$("#step-form-3").show();
		}else{
			$("#step-form-2").show();
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
			$("#step-5").hide();
			if(step == "step1"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep2').addClass("selected");
				$("#PrevStep").val("step-1");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-2").show();
			}else if(step == "step2"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep3').addClass("selected");
				$("#PrevStep").val("step-2");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-3").show();
			}else if(step == "step3"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').addClass("done");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep4').removeClass("disabled");
				$('#innerStep4').addClass("selected");
				$("#PrevStep").val("step-3");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-4").show();
			}else if(step == "step4"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').addClass("done");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep4').addClass("done");
				$('#innerStep4').removeClass("disabled");
				$('#innerStep5').removeClass("disabled");
				$('#innerStep5').addClass("selected");
				$("#PrevStep").val("step-4");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$('.buttonFinish').removeClass("buttonDisabled");
				$('.buttonNext').addClass("buttonDisabled");
				$("#step-5").show();
			}else{
				$("#PrevStep").val("NONE");
				$('.buttonPrevious').addClass("buttonDisabled");
				$('.buttonFinish').addClass("buttonDisabled");
				$("#step-1").show();
			}
			$('#circleBtn2').addClass("btn-warning");
			$('#circleBtn2').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn3').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn3').removeClass("btn-warning");
		}
		function datePicked(id){
			var gid = '#'+id;
			$(gid).datepicker({
                changeMonth: true,
                changeYear: true,
				dateFormat: 'dd-mm-yy',
				yearRange: '1945:2017'
			}).on('changeDate', function(e) {
				$(gid).val($(gid).datepicker('getFormattedDate'));
				$('#app_form_steps').formValidation('revalidateField', id);
				//$(gid).closest(".form-group").removeClass("has-error");
			});
		}
		datePicked("ex_start");
		datePicked("ex_end");
		datePicked("ex_notice");
		//datePicked("sigDate");
		$('#dob').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			yearRange: '1945:2017'
		}).on('changeDate', function(e) {
			$("#dob").val($("#dob").datepicker('getFormattedDate'));
			$('#app_form_steps').formValidation('revalidateField', 'dob');
			//$('#dob').closest(".form-group").removeClass("has-error");
		});
		$("#cal-btn").click(function(){
			$('#dob').focus();
		});
		$("#backBtn1").click(function(){
			window.location="includes/stepForm1/back.php";
		});
		$("#backBtn2").click(function(){
			window.location="includes/stepForm1/back.php";
		});
		$(".buttonPrevious").click(function(){
			$(".overlay").show();
			$(".ajax-spinner").show();
			var step = $("#PrevStep").val();
			if(step == "step-1"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-1").show();
				$("#step-2").hide();
				$('#innerStep1').addClass("selected");
				$('#innerStep1').removeClass("done");
				$('#innerStep2').removeClass("selected");
				$('#innerStep2').addClass("disabled");
				$("#PrevStep").val("");
				$('.buttonPrevious').addClass("buttonDisabled");
				window.location = "./#circleBtn1";
			}else if(step == "step-2"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-2").show();
				$("#step-3").hide();
				$('#innerStep2').addClass("selected");
				$('#innerStep2').removeClass("done");
				$('#innerStep3').removeClass("selected");
				$('#innerStep3').addClass("disabled");
				$("#PrevStep").val("step-1");
				window.location = "./#circleBtn1";
			}else if(step == "step-3"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-3").show();
				$("#step-4").hide();
				$('#innerStep3').addClass("selected");
				$('#innerStep3').removeClass("done");
				$('#innerStep4').removeClass("selected");
				$('#innerStep4').addClass("disabled");
				$("#PrevStep").val("step-2");
				window.location = "./#circleBtn1";
			}else if(step == "step-4"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-4").show();
				$("#step-5").hide();
				$('#innerStep4').addClass("selected");
				$('#innerStep4').removeClass("done");
				$('#innerStep5').removeClass("selected");
				$('#innerStep5').addClass("disabled");
				$('.buttonNext').removeClass("buttonDisabled");
				$('.buttonFinish').addClass("buttonDisabled");
				$("#PrevStep").val("step-3");
				window.location = "./#circleBtn1";
			}
		});
		$(".prevBtn").click(function(){
			$(".overlay").show();
			$(".ajax-spinner").show();
			$("#step-form-3").hide();
			$("#step-form-2").show();
			$('#circleBtn2').addClass("btn-warning");
			$('#circleBtn2').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn3').addClass("btn-default");
			$('#circleBtn3').removeClass("btn-warning");
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
			$("#step-5").show();
			$('#innerStep1').addClass("done");
			$('#innerStep1').removeClass("selected");
			$('#innerStep1').removeClass("disabled");
			$('#innerStep2').addClass("done");
			$('#innerStep2').removeClass("selected");
			$('#innerStep2').removeClass("disabled");
			$('#innerStep3').addClass("done");
			$('#innerStep3').removeClass("selected");
			$('#innerStep3').removeClass("disabled");
			$('#innerStep4').addClass("done");
			$('#innerStep4').removeClass("selected");
			$('#innerStep4').removeClass("disabled");
			$('#innerStep5').removeClass("disabled");
			$('#innerStep5').addClass("selected");
			$('.buttonPrevious').removeClass("buttonDisabled");
			$('.buttonFinish').removeClass("buttonDisabled");
			$('.buttonNext').addClass("buttonDisabled");
			$("#PrevStep").val("step-4");
			$(".overlay").hide();
			$(".ajax-spinner").hide();
		});
		//$("#borough").val("37").change();
		//$('#title').closest(".form-group").addClass("has-error");
	});
</script>