<?php
    session_start();
    
    if(isset($_SESSION['userMail'])){
        unset($_SESSION['userMail']);
    }
    session_destroy();
    header("location: ./");
?>