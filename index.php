<?php 
	session_start();
	$userMail = "";
	if(isset($_SESSION['userMail']))
		$userMail = $_SESSION['userMail'];
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
		<meta name="title" content="Care Recruitment">
		<meta name="keywords" content="care recruitment"/>
		<meta name="description" content="Care People" />

		<meta name="author" content="">
		<meta name="format-detection" content="telephone=no">
		<title>Elmich Care</title>

		<link rel="shortcut icon" href="files/fav.png"/>
		<link rel="stylesheet" type="text/css" href="files/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/style.css"/>
		<link rel="stylesheet" type="text/css" href="files/responsive.css"/>
		<link rel="stylesheet" type="text/css" href="files/AdminLTE.min.css"/>
		<link rel="stylesheet" type="text/css" href="files/jquery-ui.css"/>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="files/blue.css">  
	 
		<script type="text/javascript" src="files/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="files/bootstrap.min.js"></script>
		<script type="text/javascript" src="files/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="files/jquery-ui.js"></script>
		<script type="text/javascript" src="files/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="files/datedifferences.js"></script>
	   
	</head> 
	<body>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container"> 
					<div class="navbar-header page-scroll">
						<button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button"> 
							<span class="sr-only">Toggle navigation</span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand logo">
							<img src="files/logodaryel.png" alt="logo"/>
						</a>      
					</div>
					<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">	  
						<ul class="login-register pull-right" style="width:auto;">
							<li class="page-scroll" style="margin-right: 20px"> 
								<h4 style="color: #006a77;" class="nocapitalize">Phone: 02084347010 | Email: jobs@elmichcare.co.uk</h4> 
							</li>     
							<li  class=""><a href="#" style="background:#006a77">Home</a></li>
							<li id="login1" class="login"><a href="signin.php">Sign In</a></li>
							<li id="login2" style="display:none;" class="login">
								<div class="btn-group">
									<a href="#" class="btn btn-default">My Account</a>							  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">						
										<li><a href="signout.php">Sign Out</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<section class="main-section">
			<link href="files/smart_wizard.css" rel="stylesheet" type="text/css">
			<!--script type="text/javascript" src="files/jquery.smartWizard-2.0.js"></script-->
			<style>
				.inner-page-search .search_form{
				background: #3b72ab none repeat scroll 0 0;
				border-radius: 5px;
				color: #ffffff;
				padding: 15px;
				margin: 0px;
			}				
			</style>
			<section class="page-section main-content" >
				<div class="container">
					<div class="col-md-12">
						<div class="inner-page-search">
							<div class="search_form row" style="background:#006a77">
								<div class="col-md-10">
									<h2>Personal Information</h2>
								</div>
								 <div class="col-md-2">
								 </div> 				
							</div>
							<div class="row main-content-container">
								<div class="content-right col-md-12">
									<?php include_once("includes/strange-css.php");?>
						
									<input type="hidden" id="last_selected_value" value="">
									<div class="stepwizard">
										<div class="stepwizard-row setup-panel">
											<div class="stepwizard-step ptab text-pink">
												<a href="#step-form-1" type="button" class="btn btn-warning btn-warning-pink btn-circle" id="circleBtn1">1</a>
												<p>
												   Personal<br>Information
												</p>
											</div>
											<div class="stepwizard-step ptab text-pink">
												<a href="#step-form-2" type="button" class="btn btn-default btn-circle" id="circleBtn2">2</a>
												<p>
													Application<br>Form
												</p>
											</div>
											<div class="stepwizard-step ptab text-pink">
												<a href="#step-form-3" type="button" class="btn btn-default btn-circle" id="circleBtn3">3</a>
												<p>
													Terms<br>Policy
												</p>
											</div>
										</div>
									</div>
									<section id="iwizard" class="content">
										<?php include_once("includes/step-form-1.php");?>
										 
										<?php include_once("includes/step-form-2.php");?>
										   
										<?php include_once("includes/step-form-3.php");?>
									</section>

									<?php include_once("includes/prevEmp.php");?>

									<?php include_once("includes/more_gap.php");?>

									<?php include_once("includes/more_edu.php");?>

									<?php include_once("includes/lang_block.php");?>


								</div>
							</div>
						</div>
					</div>
				</div>
			</section> 
			<div id="dialog-message" title="Alert Notification" class="hide">
				<p><b>Your application has not submitted. Please try again.</b></p>
			</div>
			<?php //include_once("includes/last-script.php");?>
			<footer class="footer">
				<div class="footer-secoandry" style="background:#006a77">
				  <div class="container">
					<div class="logo-fotter pull-right"> 
						<a href="#"><img src="files/logodaryel.png" alt="logo"/></a>        </div>
					<div class="pull-left copyright"> © 2017 
					   <a href="#">elmichcare.com</a> |
					   <a href="#">Privacy</a> | 
					   <a href="#">Terms </a> </div>
				  </div>
				</div>
			  </footer>    
		</section>
	</body>
</html>
<script>
	$(document).ready(function(){
		var step = "<?= $step?>";
		$("#step-form-1").hide();
		$("#step-form-2").hide();
		$("#step-form-3").hide();
		if(step == "NONE"){
			$("#step-form-1").show();
		}else if(step == "stepForm2"){
			$('#circleBtn3').addClass("btn-warning");
			$('#circleBtn3').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn2').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn2').removeClass("btn-warning");
			$("#step-1").hide();
			$("#step-form-3").show();
		}else{
			$("#step-form-2").show();
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
			$("#step-5").hide();
			if(step == "step1"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep2').addClass("selected");
				$("#PrevStep").val("step-1");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-2").show();
			}else if(step == "step2"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep3').addClass("selected");
				$("#PrevStep").val("step-2");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-3").show();
			}else if(step == "step3"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').addClass("done");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep4').removeClass("disabled");
				$('#innerStep4').addClass("selected");
				$("#PrevStep").val("step-3");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$("#step-4").show();
			}else if(step == "step4"){
				$('#innerStep1').addClass("done");
				$('#innerStep1').removeClass("selected");
				$('#innerStep2').addClass("done");
				$('#innerStep2').removeClass("disabled");
				$('#innerStep3').addClass("done");
				$('#innerStep3').removeClass("disabled");
				$('#innerStep4').addClass("done");
				$('#innerStep4').removeClass("disabled");
				$('#innerStep5').removeClass("disabled");
				$('#innerStep5').addClass("selected");
				$("#PrevStep").val("step-4");
				$('.buttonPrevious').removeClass("buttonDisabled");
				$('.buttonFinish').removeClass("buttonDisabled");
				$('.buttonNext').addClass("buttonDisabled");
				$("#step-5").show();
			}else{
				$("#PrevStep").val("NONE");
				$('.buttonPrevious').addClass("buttonDisabled");
				$('.buttonFinish').addClass("buttonDisabled");
				$("#step-1").show();
			}
			$('#circleBtn2').addClass("btn-warning");
			$('#circleBtn2').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn3').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn3').removeClass("btn-warning");
		}
		function datePicked(id){
			var gid = '#'+id;
			$(gid).datepicker({
                changeMonth: true,
                changeYear: true,
				dateFormat: 'dd-mm-yy',
				yearRange: '1945:2017'
			}).on('changeDate', function(e) {
				$(gid).val($(gid).datepicker('getFormattedDate'));
				$('#app_form_steps').formValidation('revalidateField', id);
				//$(gid).closest(".form-group").removeClass("has-error");
			});
		}
		datePicked("ex_start");
		datePicked("ex_end");
		datePicked("ex_notice");
		//datePicked("sigDate");
		$('#dob').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			yearRange: '1945:2017'
		}).on('changeDate', function(e) {
			$("#dob").val($("#dob").datepicker('getFormattedDate'));
			$('#app_form_steps').formValidation('revalidateField', 'dob');
			//$('#dob').closest(".form-group").removeClass("has-error");
		});
		$("#cal-btn").click(function(){
			$('#dob').focus();
		});
		$("#backBtn1").click(function(){
			window.location="includes/stepForm1/back.php";
		});
		$("#backBtn2").click(function(){
			window.location="includes/stepForm1/back.php";
		});
		$(".buttonPrevious").click(function(){
			$(".overlay").show();
			$(".ajax-spinner").show();
			var step = $("#PrevStep").val();
			if(step == "step-1"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-1").show();
				$("#step-2").hide();
				$('#innerStep1').addClass("selected");
				$('#innerStep1').removeClass("done");
				$('#innerStep2').removeClass("selected");
				$('#innerStep2').addClass("disabled");
				$("#PrevStep").val("");
				$('.buttonPrevious').addClass("buttonDisabled");
				window.location = "./#circleBtn1";
			}else if(step == "step-2"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-2").show();
				$("#step-3").hide();
				$('#innerStep2').addClass("selected");
				$('#innerStep2').removeClass("done");
				$('#innerStep3').removeClass("selected");
				$('#innerStep3').addClass("disabled");
				$("#PrevStep").val("step-1");
				window.location = "./#circleBtn1";
			}else if(step == "step-3"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-3").show();
				$("#step-4").hide();
				$('#innerStep3').addClass("selected");
				$('#innerStep3').removeClass("done");
				$('#innerStep4').removeClass("selected");
				$('#innerStep4').addClass("disabled");
				$("#PrevStep").val("step-2");
				window.location = "./#circleBtn1";
			}else if(step == "step-4"){
				$(".overlay").hide();
				$(".ajax-spinner").hide();
				$("#step-4").show();
				$("#step-5").hide();
				$('#innerStep4').addClass("selected");
				$('#innerStep4').removeClass("done");
				$('#innerStep5').removeClass("selected");
				$('#innerStep5').addClass("disabled");
				$('.buttonNext').removeClass("buttonDisabled");
				$('.buttonFinish').addClass("buttonDisabled");
				$("#PrevStep").val("step-3");
				window.location = "./#circleBtn1";
			}
		});
		$(".prevBtn").click(function(){
			$(".overlay").show();
			$(".ajax-spinner").show();
			$("#step-form-3").hide();
			$("#step-form-2").show();
			$('#circleBtn2').addClass("btn-warning");
			$('#circleBtn2').removeClass("btn-default");
			$('#circleBtn1').addClass("btn-default");
			$('#circleBtn1').removeClass("btn-warning");
			$('#circleBtn3').addClass("btn-default");
			$('#circleBtn3').removeClass("btn-warning");
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
			$("#step-5").show();
			$('#innerStep1').addClass("done");
			$('#innerStep1').removeClass("selected");
			$('#innerStep1').removeClass("disabled");
			$('#innerStep2').addClass("done");
			$('#innerStep2').removeClass("selected");
			$('#innerStep2').removeClass("disabled");
			$('#innerStep3').addClass("done");
			$('#innerStep3').removeClass("selected");
			$('#innerStep3').removeClass("disabled");
			$('#innerStep4').addClass("done");
			$('#innerStep4').removeClass("selected");
			$('#innerStep4').removeClass("disabled");
			$('#innerStep5').removeClass("disabled");
			$('#innerStep5').addClass("selected");
			$('.buttonPrevious').removeClass("buttonDisabled");
			$('.buttonFinish').removeClass("buttonDisabled");
			$('.buttonNext').addClass("buttonDisabled");
			$("#PrevStep").val("step-4");
			$(".overlay").hide();
			$(".ajax-spinner").hide();
		});
		//$("#borough").val("37").change();
		//$('#title').closest(".form-group").addClass("has-error");
	});
</script>

<!-- validation to be added here -->