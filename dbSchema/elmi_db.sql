-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2017 at 10:33 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `elmi_db`
--
CREATE DATABASE IF NOT EXISTS `elmi_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `elmi_db`;

-- --------------------------------------------------------

--
-- Table structure for table `a_step1`
--

CREATE TABLE IF NOT EXISTS `a_step1` (
  `em_mon` int(3) NOT NULL,
  `em_tue` int(3) NOT NULL,
  `em_wed` int(3) NOT NULL,
  `em_thu` int(3) NOT NULL,
  `em_fri` int(3) NOT NULL,
  `em_sat` int(3) NOT NULL,
  `em_sun` int(3) NOT NULL,
  `lm_mon` int(3) NOT NULL,
  `lm_tue` int(3) NOT NULL,
  `lm_wed` int(3) NOT NULL,
  `lm_thu` int(3) NOT NULL,
  `lm_fri` int(3) NOT NULL,
  `lm_sat` int(3) NOT NULL,
  `lm_sun` int(3) NOT NULL,
  `ea_mon` int(3) NOT NULL,
  `ea_tue` int(3) NOT NULL,
  `ea_wed` int(3) NOT NULL,
  `ea_thu` int(3) NOT NULL,
  `ea_fri` int(3) NOT NULL,
  `ea_sat` int(3) NOT NULL,
  `ea_sun` int(3) NOT NULL,
  `la_mon` int(3) NOT NULL,
  `la_tue` int(3) NOT NULL,
  `la_wed` int(3) NOT NULL,
  `la_thu` int(3) NOT NULL,
  `la_fri` int(3) NOT NULL,
  `la_sat` int(3) NOT NULL,
  `la_sun` int(3) NOT NULL,
  `ev_mon` int(3) NOT NULL,
  `ev_tue` int(3) NOT NULL,
  `ev_wed` int(3) NOT NULL,
  `ev_thu` int(3) NOT NULL,
  `ev_fri` int(3) NOT NULL,
  `ev_sat` int(3) NOT NULL,
  `ev_sun` int(3) NOT NULL,
  `wn_mon` int(3) NOT NULL,
  `wn_tue` int(3) NOT NULL,
  `wn_wed` int(3) NOT NULL,
  `wn_thu` int(3) NOT NULL,
  `wn_fri` int(3) NOT NULL,
  `wn_sat` int(3) NOT NULL,
  `wn_sun` int(3) NOT NULL,
  `sn_mon` int(3) NOT NULL,
  `sn_tue` int(3) NOT NULL,
  `sn_wed` int(3) NOT NULL,
  `sn_thu` int(3) NOT NULL,
  `sn_fri` int(3) NOT NULL,
  `sn_sat` int(3) NOT NULL,
  `sn_sun` int(3) NOT NULL,
  `will_hrs` varchar(30) NOT NULL,
  `live_work_uk` varchar(30) NOT NULL,
  `f_name` varchar(30) NOT NULL,
  `relation` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `heard` varchar(30) NOT NULL,
  `otherSearchEngine` varchar(30),
  `otherJobSite` varchar(30),
  `elmichCareStaff` varchar(30),
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_step1`
--

INSERT INTO `a_step1` (`em_mon`, `em_tue`, `em_wed`, `em_thu`, `em_fri`, `em_sat`, `em_sun`, `lm_mon`, `lm_tue`, `lm_wed`, `lm_thu`, `lm_fri`, `lm_sat`, `lm_sun`, `ea_mon`, `ea_tue`, `ea_wed`, `ea_thu`, `ea_fri`, `ea_sat`, `ea_sun`, `la_mon`, `la_tue`, `la_wed`, `la_thu`, `la_fri`, `la_sat`, `la_sun`, `ev_mon`, `ev_tue`, `ev_wed`, `ev_thu`, `ev_fri`, `ev_sat`, `ev_sun`, `wn_mon`, `wn_tue`, `wn_wed`, `wn_thu`, `wn_fri`, `wn_sat`, `wn_sun`, `sn_mon`, `sn_tue`, `sn_wed`, `sn_thu`, `sn_fri`, `sn_sat`, `sn_sun`, `will_hrs`, `live_work_uk`, `f_name`, `relation`, `contact`, `heard`, `otherSearchEngine`, `otherJobSite`, `elmichCareStaff`, `email`) VALUES
(0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, '6', 'No', 'dsfjhnk', 'jsdbkfdsn', '268561256', 'Leaflet',' ', ' ',' ', 'degodtest@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `a_step2`
--

CREATE TABLE IF NOT EXISTS `a_step2` (
  `institution` varchar(100),
  `qual_sub` varchar(100),
  `qual_obt` varchar(100),
  `qual_yr` varchar(100),
  `ex_company` varchar(100) NOT NULL,
  `ex_address` varchar(300) NOT NULL,
  `ex_post` varchar(100) NOT NULL,
  `ex_phone` varchar(20) NOT NULL,
  `ex_start` varchar(20) NOT NULL,
  `ex_end` varchar(20) NOT NULL,
  `ex_notice` varchar(20) NOT NULL,
  `start_when` varchar(20),
  `ex_task` varchar(200) NOT NULL,
  `ex_reason` varchar(500) NOT NULL,
  `email` varchar(200) NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_step2`
--

INSERT INTO `a_step2` (`institution`,`qual_sub`,`qual_obt`,`qual_yr`,`ex_company`, `ex_address`, `ex_post`, `ex_phone`, `ex_start`, `ex_end`, `ex_notice`, `start_when`, `ex_task`, `ex_reason`, `email`) VALUES
('qeerq','qeerq','qeerq','qeerq', 'zfjdfkjfn', 'sfddkjflkdj', 'kfjenrkfenr', '55316851531', '04/17/2017', '04/18/2017', '04/13/2017', '05/20/2017', 's,fvjdfmlv;l', 'dfmlvdknvdfndlfv', 'degodtest@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `a_step3`
--

CREATE TABLE IF NOT EXISTS `a_step3` (
  `institution` varchar(200) NOT NULL,
  `qual_sub` varchar(200) NOT NULL,
  `qual_obt` varchar(200) NOT NULL,
  `qual_yr` varchar(200) NOT NULL,
  `institution2` varchar(200),
  `qual_sub2` varchar(200),
  `qual_obt2` varchar(200),
  `qual_yr2` varchar(200),
  `institution3` varchar(200),
  `qual_sub3` varchar(200),
  `qual_obt3` varchar(200),
  `qual_yr3` varchar(200),
  `institution4` varchar(200),
  `qual_sub4` varchar(200),
  `qual_obt4` varchar(200),
  `qual_yr4` varchar(200),
  `ref1_name` varchar(200) NOT NULL,
  `ref1_company` varchar(200) NOT NULL,
  `ref1_job` varchar(200) NOT NULL,
  `ref1_email` varchar(200) NOT NULL,
  `ref1_address1` varchar(200) NOT NULL,
  `ref1_address2` varchar(200) NOT NULL,
  `ref1_town` varchar(200) NOT NULL,
  `ref1_phone` varchar(200) NOT NULL,
  `ref1_postcode` varchar(200) NOT NULL,
  `ref1_yes` varchar(200) NOT NULL,
  `ref1_no` varchar(200) NOT NULL,
  `ref2_name` varchar(200) NOT NULL,
  `ref2_company` varchar(200) NOT NULL,
  `ref2_job` varchar(200) NOT NULL,
  `ref2_email` varchar(200) NOT NULL,
  `ref2_address1` varchar(200) NOT NULL,
  `ref2_address2` varchar(200) NOT NULL,
  `ref2_town` varchar(200) NOT NULL,
  `ref2_phone` varchar(200) NOT NULL,
  `ref2_postcode` varchar(200) NOT NULL,
  `ref2_yes` varchar(200) NOT NULL,
  `ref2_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_step3`
--

INSERT INTO `a_step3` (`institution`, `qual_sub`, `qual_obt`, `qual_yr`,`institution2`, `qual_sub2`, `qual_obt2`, `qual_yr2`, `institution3`, `qual_sub3`, `qual_obt3`, `qual_yr3`, `institution4`, `qual_sub4`, `qual_obt4`, `qual_yr4`, `ref1_name`, `ref1_company`, `ref1_job`, `ref1_email`, `ref1_address1`, `ref1_address2`, `ref1_town`, `ref1_phone`, `ref1_postcode`, `ref1_yes`, `ref1_no`, `ref2_name`, `ref2_company`, `ref2_job`, `ref2_email`, `ref2_address1`, `ref2_address2`, `ref2_town`, `ref2_phone`, `ref2_postcode`, `ref2_yes`, `ref2_no`, `email`) VALUES
('qrewrewr','skdjhfksjdh', 'kdfhgk', '1987', 'qrewrewr','skdjhfksjdh', 'kdfhgk', '1987','qrewrewr','skdjhfksjdh', 'kdfhgk', '1987','qrewrewr','skdjhfksjdh', 'kdfhgk', '1987', 'dsjhfkjh', 'jhsdkjhf', 'sdkjhfkj', 'khdsjhkj', 'hsdkfjhk', 'kjhfdkjh', 'hdkjhfk', 'dkjhfkj', 'dskjhfkj', '1', '0', 'djfhkjh', 'jhdkfjhk', 'jhkjhjh', 'kjhkjdhfkjh', 'kjhdkjhkj', 'hkjhdkjhk', 'jhkjsdhkjh', 'kjhkdjhk', 'hkjhdkfj', '1', '0', 'degodtest@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `a_step4`
--

CREATE TABLE IF NOT EXISTS `a_step4` (
  `a_g` varchar(10) NOT NULL,
  `b_g` varchar(10) NOT NULL,
  `c_g` varchar(10) NOT NULL,
  `d_g` varchar(10) NOT NULL,
  `e_g` varchar(10) NOT NULL,
  `f_g` varchar(10) NOT NULL,
  `g_g` varchar(10) NOT NULL,
  `h_g` varchar(10) NOT NULL,
  `i_g` varchar(10) NOT NULL,
  `j_g` varchar(10) NOT NULL,
  `k_g` varchar(10) NOT NULL,
  `l_g` varchar(10) NOT NULL,
  `m_g` varchar(10) NOT NULL,
  `n_g` varchar(10) NOT NULL,
  `o_g` varchar(10) NOT NULL,
  `p_g` varchar(10) NOT NULL,
  `q_g` varchar(10) NOT NULL,
  `r_g` varchar(10) NOT NULL,
  `s_g` varchar(10) NOT NULL,
  `t_g` varchar(10) NOT NULL,
  `u_g` varchar(10) NOT NULL,
  `v_g` varchar(10) NOT NULL,
  `w_g` varchar(10) NOT NULL,
  `x_g` varchar(10) NOT NULL,
  `a_b` varchar(10) NOT NULL,
  `b_b` varchar(10) NOT NULL,
  `c_b` varchar(10) NOT NULL,
  `d_b` varchar(10) NOT NULL,
  `e_b` varchar(10) NOT NULL,
  `f_b` varchar(10) NOT NULL,
  `g_b` varchar(10) NOT NULL,
  `h_b` varchar(10) NOT NULL,
  `i_b` varchar(10) NOT NULL,
  `j_b` varchar(10) NOT NULL,
  `k_b` varchar(10) NOT NULL,
  `l_b` varchar(10) NOT NULL,
  `m_b` varchar(10) NOT NULL,
  `n_b` varchar(10) NOT NULL,
  `o_b` varchar(10) NOT NULL,
  `p_b` varchar(10) NOT NULL,
  `q_b` varchar(10) NOT NULL,
  `r_b` varchar(10) NOT NULL,
  `s_b` varchar(10) NOT NULL,
  `t_b` varchar(10) NOT NULL,
  `u_b` varchar(10) NOT NULL,
  `v_b` varchar(10) NOT NULL,
  `w_b` varchar(10) NOT NULL,
  `x_b` varchar(10) NOT NULL,
  `a_n` varchar(10) NOT NULL,
  `b_n` varchar(10) NOT NULL,
  `c_n` varchar(10) NOT NULL,
  `d_n` varchar(10) NOT NULL,
  `e_n` varchar(10) NOT NULL,
  `f_n` varchar(10) NOT NULL,
  `g_n` varchar(10) NOT NULL,
  `h_n` varchar(10) NOT NULL,
  `i_n` varchar(10) NOT NULL,
  `j_n` varchar(10) NOT NULL,
  `k_n` varchar(10) NOT NULL,
  `l_n` varchar(10) NOT NULL,
  `m_n` varchar(10) NOT NULL,
  `n_n` varchar(10) NOT NULL,
  `o_n` varchar(10) NOT NULL,
  `p_n` varchar(10) NOT NULL,
  `q_n` varchar(10) NOT NULL,
  `r_n` varchar(10) NOT NULL,
  `s_n` varchar(10) NOT NULL,
  `t_n` varchar(10) NOT NULL,
  `u_n` varchar(10) NOT NULL,
  `v_n` varchar(10) NOT NULL,
  `w_n` varchar(10) NOT NULL,
  `x_n` varchar(10) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_step4`
--

INSERT INTO `a_step4` (`a_g`, `b_g`, `c_g`, `d_g`, `e_g`, `f_g`, `g_g`, `h_g`, `i_g`, `j_g`, `k_g`, `l_g`, `m_g`, `n_g`, `o_g`, `p_g`, `q_g`, `r_g`, `s_g`, `t_g`, `u_g`, `v_g`, `w_g`, `x_g`, `a_b`, `b_b`, `c_b`, `d_b`, `e_b`, `f_b`, `g_b`, `h_b`, `i_b`, `j_b`, `k_b`, `l_b`, `m_b`, `n_b`, `o_b`, `p_b`, `q_b`, `r_b`, `s_b`, `t_b`, `u_b`, `v_b`, `w_b`, `x_b`, `a_n`, `b_n`, `c_n`, `d_n`, `e_n`, `f_n`, `g_n`, `h_n`, `i_n`, `j_n`, `k_n`, `l_n`, `m_n`, `n_n`, `o_n`, `p_n`, `q_n`, `r_n`, `s_n`, `t_n`, `u_n`, `v_n`, `w_n`, `x_n`, `email`) VALUES
('0', '0', '0', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', 'degodtest@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `a_step5`
--

CREATE TABLE IF NOT EXISTS `a_step5` (
  `soc_serve` varchar(20) NOT NULL,
  `servExp` varchar(255),
  `arrest` varchar(20) NOT NULL,
  `arrestExp` varchar(255),
  `inve` varchar(20) NOT NULL,
  `inveExp` varchar(255),
  `caut` varchar(30) NOT NULL,
  `cautExp` varchar(255),
  `give_perm` varchar(20) NOT NULL,
  `con_cor` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_step5`
--

INSERT INTO `a_step5` (`soc_serve`, `servExp`, `arrest`,`arrestExp`, `inve`, `inveExp`, `caut`, `cautExp`, `give_perm`, `con_cor`, `email`) VALUES
('No',' ','Yes', ' ', 'Yes', ' ', 'Yes', ' ', '1', '1', 'degodtest@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `p_info`
--

CREATE TABLE IF NOT EXISTS `p_info` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `f_name` varchar(200) NOT NULL,
  `l_name` varchar(200) NOT NULL,
  `m_name` varchar(200),
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `address2` varchar(200) NOT NULL,
  `address_his1` varchar(200),
  `address_his2` varchar(200),
  `address_his3` varchar(200),
  `address_his4` varchar(200),
  `address_his5` varchar(200),
  `town` varchar(200) NOT NULL,
  `borough` varchar(200) NOT NULL,
  `otherBorough` varchar(200),
  `postcode` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `prof` varchar(200) NOT NULL,
  `lang` varchar(200) NOT NULL,
  `post` varchar(200) NOT NULL,
  `otherPosition` varchar(200),
  `expert` varchar(200) NOT NULL,
  `dbs` varchar(20) NOT NULL,
  `car_license` varchar(20) NOT NULL,
  `insure_no` varchar(50) NOT NULL,
  `complete` varchar(10) NOT NULL,
  `step` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `p_info`
--

INSERT INTO `p_info` (
  `id`, 
  `title`, 
  `f_name`, 
  `l_name`, 
  `m_name`, 
  `email`, 
  `mobile`, 
  `dob`, 
  `address1`, 
  `address2`,
  `address_his1`,
  `address_his2`,
  `address_his3`,
  `address_his4`,
  `address_his5`, 
  `town`, 
  `borough`, 
  `otherBorough`, 
  `postcode`, 
  `password`, 
  `prof`, 
  `lang`, 
  `post`, 
  `otherPosition`, 
  `expert`, 
  `dbs`, 
  `car_license`, 
  `insure_no`, 
  `complete`, 
  `step`) VALUES (
  1, 
  'Mrs', 
  'fsmadjfkj', 
  'skdjfkj', 
  'skdjfkj', 
  'degodtest@gmail.com', 
  '564164165', 
  '04/01/2017', 
  'kdsljdf', 
  'KFJKSH',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  'SDLKJSLDK', 
  '7',
  '',
  'sdj,flsdk', 
  '1212', 
  'Fluent', 
  '0', 
  '1',
  '', 
  'not_previously_employed', 
  'Yes', 
  'Yes', 
  '5346151384561', 
  'YES', 
  'stepForm2');

-- --------------------------------------------------------

--
-- Table structure for table `sig_table`
--

CREATE TABLE IF NOT EXISTS `sig_table` (
  `sig_agree` varchar(10) NOT NULL,
  `sig_name` varchar(50) NOT NULL,
  `sig_date` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sig_table`
--

INSERT INTO `sig_table` (`sig_agree`, `sig_name`, `sig_date`, `email`) VALUES
('1', 'sldkflksd', '10-05-2017', 'degodtest@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
